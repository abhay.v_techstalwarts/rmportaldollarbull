import { EnvironmentInterface } from "./environment.interface";

export const environment:EnvironmentInterface = {
  name: 'prod',
  production: true,
  logApi: true,
  enableProfiler: true,
  // serverUrl: 'http://3.141.105.187:4000',
  // userAPIUrl: 'http://3.141.105.187:4000/user/v1',
  // rmAPIUrl: "http://3.141.105.187:4000/user/v1/rm",
  // dataAPIUrl: 'http://3.141.105.187:4001/data/v1',
  faqAPIUrl:'https://website.dollarbull.com/',
  serverUrl: 'https://customer.dollarbull.com',
  rmAPIUrl: "https://customer.dollarbull.com/user/v1/rm",
  userAPIUrl: 'https://customer.dollarbull.com/user/v1',
  dataAPIUrl: 'https://stocks.dollarbull.com/data/v1',
  baseUrl:'http://localhost:4200'
};
