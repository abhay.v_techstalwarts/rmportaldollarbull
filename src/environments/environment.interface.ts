export interface EnvironmentInterface {
    name: 'dev' | 'stage' | 'prod';
    production: boolean;
    logApi: boolean;
    enableProfiler: boolean,
    serverUrl: string;
    userAPIUrl: string;
    dataAPIUrl: string;
    baseUrl:string;
    rmAPIUrl: string;
    faqAPIUrl:string;


}