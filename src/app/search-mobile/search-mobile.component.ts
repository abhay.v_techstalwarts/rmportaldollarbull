import { Component, EventEmitter, OnInit,Input, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavigationService } from 'src/app/@core/service/navigation.service';


@Component({
  selector: 'app-search-mobile',
  templateUrl: './search-mobile.component.html',
  styleUrls: ['./search-mobile.component.scss']
})
export class SearchMobileComponent implements OnInit {
  @Output()
  onSearchChange: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  onBackClick: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  disableDefaultBack: boolean = false;
  searchBlock=false;
  searchInput: string = '';
  strings: any = {
    searchPlaceholder: 'Search Stocks',
  };
  searchCategoryTabs : any[]=[
    {
      link: "/explore/stocks",
      label: "Stocks"
    },
    {
      link: "/explore/stocks",
      label: "Etf"
    },
  ]
  constructor(private navigation: NavigationService) { }

  ngOnInit(): void {
  }
  handleSearchChange(e:any) {
    this.searchInput = e.target.value;
    console.log(this.searchInput);
    this.searchBlock = true;
    this.onSearchChange.emit(this.searchInput);
    // setTimeout(() => {
    //   this.searchBlock = false;
    // }, 2000);
  }

  handleBackClick(e: any) {
    if (this.onBackClick) {
      this.onBackClick.emit(e);
    }

    if (this.disableDefaultBack) {
      return;
    }
    this.navigation.back();
  }

}
