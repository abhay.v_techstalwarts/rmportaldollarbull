import { Component, OnInit, NgZone } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { CommonService } from '../app/@core/service/common/common.service';
import { ActivatedRoute } from '@angular/router';
import { PlatformService } from './@core/service/platform.service';
import { AuthService } from './@core/service/auth/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  title = 'dollar-bull';
  loaderEnabled;
  counter = 0;
  platform:string="web";
  constructor(
    private commonService: CommonService,
    private zone: NgZone,
    private route: ActivatedRoute,
    private router: Router,
    private platformService: PlatformService,
    private authService: AuthService, 
  ) {
    
  
  }


  ngOnInit() {
    this.commonService.isLoaderEnabled$.subscribe((loaderEnabled) => {
      this.loaderEnabled = loaderEnabled;
    });
    

  }
}
