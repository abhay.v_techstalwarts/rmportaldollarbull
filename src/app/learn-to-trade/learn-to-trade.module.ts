import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../@shared/shared.module';
import { LearnToTradeRoutingModule } from './learn-to-trade-routing.module';
import { HomeComponent } from './home/home.component';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    LearnToTradeRoutingModule,
    SharedModule
  ]
})
export class LearnToTradeModule { }
