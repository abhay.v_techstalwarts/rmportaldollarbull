import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  videoURL:any;
  
  constructor(private sanitizer: DomSanitizer) { 
    this.videoURL = sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/bJHr6_skXWc');
  }

  ngOnInit(): void {
  }

}
