import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../@core/service/auth/auth.service';
import { CommonService } from '../@core/service/common/common.service';
import { StocksService } from '../@core/service/stocks/stocks.service';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
})
export class SplashComponent implements OnInit {
  constructor(private router: Router, private authService: AuthService, private commonService: CommonService, private stocksService: StocksService) { }

  ngOnInit(): void {
    setTimeout(() => this.handleAuthentication(), 4000);
  }  

  handleAuthentication():void {
    const isAuthenticated = this.authService.isAuthenticated();

    if (!isAuthenticated) {
      this.signUpNavigation();
    } else {
      this.handlePreloadApis();
    }
  }

  signUpNavigation() {
    this.router.navigate(['/login'], { replaceUrl: true });
  }

  handleInitialNavigation() {
    this.router.navigate(['/dashboard'], { replaceUrl: true });
  }

  async handlePreloadApis() {
    await this.commonService.getDollarbullStats().toPromise();
    await this.stocksService.getTopGainerLoserStocks({
      limit: 20,
      offset: 0
    }).toPromise();

    this.handleInitialNavigation();
  }
}
