import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { SignUpService } from '../@core/service/signup/signup.service';
import { Router } from '@angular/router';
import { JwtService } from '../@core/service/jwt.service';
import { AuthService } from 'src/app/@core/service/auth/auth.service';
import { PlatformService } from 'src/app/@core/service/platform.service';
import { CustomerService } from 'src/app/@core/service/customer/customer.service';
import { HttpClient } from '@angular/common/http';
import { CountdownComponent, CountdownEvent } from 'ngx-countdown';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from '../@core/service/common/common.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  @ViewChild('cd', { static: false }) private countdown: CountdownComponent;
  fbLogin: any;
  user = null;
  token = null;
  showLoader = false;
  enterOTPSection: boolean = false;
  otpForm: any;
  enterEmail: boolean = true;
  app_login_msg: string = "";
  otp_screen_msg: string = "";
  settings = {
    length: 6,
    numbersOnly: true,
    // timer: 60
  }
  platform = "";
  disableSendBtn: boolean = true;
  date_config = { leftTime: 120, format: 'm:s', demand: true }
  // rm login
  rmForm: any;
  rmForgotPasswordForm:any;

  constructor(private toastr: ToastrService, private commonService: CommonService, private modalService: NgbModal, config: NgbModalConfig ,private fb: FormBuilder, private signUpService: SignUpService, private router: Router, private jwtService: JwtService,
    
    private platformService: PlatformService,
    private authService: AuthService
    ,private http: HttpClient, private customerService: CustomerService) { }

  ngOnInit(): void {
    this.platform = this.platformService.getPlatform();
    // rm login form
    this.rmForm = this.fb.group({
      email_id: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required,]],
    });
    this.rmForgotPasswordForm = this.fb.group({
      email_id: ['', [Validators.required, Validators.email]],
    });
    // end rm login form
  }

  rm_login() {
    if (this.rmForm.valid) {
      let data = {
        email_id: this.rmForm.get('email_id').value,
        password: this.rmForm.get('password').value,
      }
      this.signUpService.rm_login(data).subscribe((res:any) => {
        let response: any = res;
        this.jwtService.saveToken(response.result.token);
        this.jwtService.savePartnercode(response.result.partner_code);
        this.jwtService.saveRM_AdvisorCode(response.result.rm_advisor_code);
        this.jwtService.saveRM_Email_ID(response.result.rm_email_id);
        this.jwtService.saveRM_Master_Id(response.result.rm_master_id);
        this.jwtService.saveRM_Mobile_number(response.result.rm_mobile_no);
        this.jwtService.saveRM_Referal_Code(response.result.rm_referal_code);
        this.showLoader = false;

        if(response.statusCode == '9078'){
        this.router.navigate(['/changepassword'], { replaceUrl: true });

        }
        else{
        this.router.navigate(['/dashboard'], { replaceUrl: true });

        }
        // console.log("rm_login", res);
        // console.log("rm_login", JSON.stringify(res));
      },
      (err) => {
        this.showLoader = false;
        console.log(err)
        this.otp_screen_msg = err.error.message;
      this.toastr.error(err.error.message, "", { timeOut: 3000, disableTimeOut: false, });

      }
      );


    }
  }

  openForgotPasswordModal(forgotPasswordModal) {
    this.modalService.open(forgotPasswordModal);
  }
  closemodal(){
    this.rmForgotPasswordForm.reset();
    this.modalService.dismissAll();
  }
  submitForgot(){
    localStorage.setItem('email_id', this.rmForgotPasswordForm.get('email_id').value);
    let data = {
      email_id : this.rmForgotPasswordForm.get('email_id').value,
    }
    this.commonService.forgotPasswordLink(data).subscribe((res:any) => {
      //@ts-ignore
      console.log(res);
      //@ts-ignore
        this.toastr.success(res.message, "", { timeOut: 3000, disableTimeOut: false, });
       this.closemodal();
        // this.rmForgotPasswordForm.reset();
    },
    (err) => {
      this.showLoader = false;
      this.toastr.error(err.error.message, "", { timeOut: 3000, disableTimeOut: false, });
    })
  }
}
