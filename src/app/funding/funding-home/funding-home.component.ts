import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-funding-home',
  templateUrl: './funding-home.component.html',
  providers: [NgbModalConfig, NgbModal]
})
export class FundingHomeComponent implements OnInit {

  constructor(private router: Router,config: NgbModalConfig, private modalService: NgbModal) {
    config.backdrop = 'static';
    config.keyboard = false;
   }

  ngOnInit(): void {
  }
  
  navigateToAppoitment(){
    this.router.navigate(['funding/appointment']);
  }
  open(offermodalonfunds) {
    this.modalService.open(offermodalonfunds);
  }

}
