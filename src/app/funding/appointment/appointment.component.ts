import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { LrsService } from 'src/app/@core/service/lrs/lrs.service';
import { CustomerService } from 'src/app/@core/service/customer/customer.service';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  providers: [NgbModalConfig, NgbModal]
})
export class AppointmentComponent implements OnInit {
  bankList: any = [];
  selectedBank: any;
  appointment: any;
  constructor(config: NgbModalConfig, private modalService: NgbModal, private lrsService: LrsService, private fb: FormBuilder
    , private customerService:CustomerService, private toastr: ToastrService,) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit(): void {
    this.getBanksList();
    this.initilise_form();
  }

  initilise_form(){
    this.appointment = this.fb.group({
      name: ['', [Validators.required]],
      preferred_bank: ['', [Validators.required]],
      mobile_no: ['', [Validators.required, Validators.maxLength(13)]],
      appointment_date: ['', [Validators.required]]
    });
  }

  getBanksList() {
    this.lrsService.getBanks(0).subscribe((res) => {
      this.bankList = res;
      console.log(['this.bankList[0].id', this.bankList[0].id]);
      this.appointment.controls['preferred_bank'].setValue(this.bankList[0].id);
    });
  }

  save_appointment() {
    if (this.appointment.valid) {
    this.appointment.controls['mobile_no'].setValue(this.appointment.controls['mobile_no'].value.toString());
    this.customerService.requestAppointment(this.appointment.value).subscribe(res=>{
      this.toastr.success("We have registered your request. Our support team will contact you.", "", { timeOut: 3000, disableTimeOut: false, });
      this.initilise_form();
      this.appointment.controls['preferred_bank'].setValue(this.bankList[0].id);
    });
  }
  }

}
