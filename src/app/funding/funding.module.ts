import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../@shared/shared.module';

import { FundingRoutingModule } from './funding-routing.module';
import { FundingHomeComponent } from './funding-home/funding-home.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'


@NgModule({
  declarations: [FundingHomeComponent, AppointmentComponent],
  imports: [
    CommonModule,
    FundingRoutingModule,
    SharedModule,
    FormsModule, 
    ReactiveFormsModule,
  ],
  exports: [FundingHomeComponent, AppointmentComponent]
})
export class FundingModule { }
