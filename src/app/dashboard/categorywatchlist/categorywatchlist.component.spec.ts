import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorywatchlistComponent } from './categorywatchlist.component';

describe('CategorywatchlistComponent', () => {
  let component: CategorywatchlistComponent;
  let fixture: ComponentFixture<CategorywatchlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategorywatchlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategorywatchlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
