import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavigationService } from 'src/app/@core/service/navigation.service';
import { Router } from '@angular/router';
import { NgbModalConfig, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TabsI } from '../../@shared/component/nav-tabs/nav-tabs.component';
import { WatchlistService } from 'src/app/@core/service/watchlist/watchlist.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { fail } from 'assert';
import { ToastrService } from 'ngx-toastr';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service'

@Component({
  selector: 'app-categorywatchlist',
  templateUrl: './categorywatchlist.component.html',
  providers: [NgbModalConfig, NgbModal]
})
export class CategorywatchlistComponent implements OnInit {
  @Output()
  onBackClick: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  disableDefaultBack: boolean = false;

  categoryTabs: TabsI[] = [];
  activeFinancialTab: string = "";
  activeWatchlist: any;
  myWatchListEmpty = true;
  myWatchListLoading = true;
  modalReference: any;
  isEdit = false;
  watchlistAddForm: FormGroup;
  watchlistCompany: any[] = [];
  watchlistCompanyIds =[]
  alphaArrowicon: boolean = true;
  changeArrowicon: boolean = false;
  pressTimer: any;
  show_name_arrow = false;
  show_percentage_arrow = false;
  componeysIds=[];
  alphabet_sort = "";
  percentage_change_sort = "";
  alphabet_sort_icon = "assets/icons/filter-arrow-up-icon.svg";
  percentage_change_sort_icon = "assets/icons/filter-arrow-up-icon.svg";
  longPressState:boolean = false;
  strings: any = {
    searchPlaceholder: 'Search Stocks & add to watchlist',
  };
  searchInput: string = '';
  searchBlock = false;
  searchedResults = []
  searchLoading = false;

  constructor(private router: Router, private modalService: NgbModal,
    private navigation: NavigationService,
    private watchlistService: WatchlistService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private stocksService: StocksService,
  ) {
    this.watchlistAddForm = this.fb.group({
      watchlistName: ['', [Validators.required,Validators.minLength(3), Validators.maxLength(15)]],
    });
  }

  ngOnInit(): void {
    this.getAllWatchlist(true);
  }
  watchListDiscloser(discloserModal) {
    this.modalService.open(discloserModal);
  }

  getAllWatchlist(forceCall = false) {
    this.myWatchListLoading = true;

    this.watchlistService.getWatchlist(forceCall).subscribe((res) => {
      this.categoryTabs = res.map((d: any) => ({
        label: d.watchlistName,
        link: d.watchlistId,
        preventDefault: true,
      }))
      if (this.categoryTabs.length) {
        this.activeWatchlist = this.categoryTabs[0];
        this.activeFinancialTab = this.categoryTabs[0] && this.categoryTabs[0].link ? this.categoryTabs[0].link : "";
        this.getCompanyByWatchlist();
      } else {
        this.myWatchListLoading = false;
      }
    },
      fail => {
        this.myWatchListLoading = false;
        this.watchlistCompany = [];
      })
  }

  getCompanyByWatchlist() {
    this.myWatchListLoading = true;

    this.watchlistService.getCompanyByWatchlist(this.activeFinancialTab).subscribe((success) => {
      console.log({ getCompanyByWatchlist: success });
      this.watchlistCompany = success.records;
      this.myWatchListEmpty = this.watchlistCompany.length < 1;
      this.myWatchListLoading = false;
      this.watchlistCompanyIds = this.watchlistCompany.map( company => company.companyId)
      this.componeysIds = [];
      this.longPressState = false;

    }, fail => {
      this.myWatchListLoading = false;
      this.watchlistCompany = [];
    })
  }

  handleBackClick(e: any) {
    if (this.onBackClick) {
      this.onBackClick.emit(e);
    }

    if (this.disableDefaultBack) {
      return;
    }
    this.navigation.back();
  }
  navigateToAllStocks() {
    this.router.navigate(['explore/stocks/all-stocks']);
  }
  deleteWatchlistOpen(deleteWatchlist: any) {
    this.modalReference = this.modalService.open(deleteWatchlist, { centered: true });
  }
  watchlistAddModalOpen(watchlistAddModal: any, isEdit = false) {
    this.isEdit = isEdit;
    if (isEdit) {
      this.watchlistAddForm.controls['watchlistName'].setValue(`${this.activeWatchlist.label}`)
    }
    this.modalReference = this.modalService.open(watchlistAddModal, { centered: true });
  }
  watchlistEditModalOpen(watchlistEditModal: any) {
    this.modalReference = this.modalService.open(watchlistEditModal, { centered: true });
  }

  handleOnClick(e: any) {
    this.show_percentage_arrow=false;
      this.show_name_arrow=false;
    this.activeFinancialTab = e.item.link;
    this.activeWatchlist = e.item;

    this.getCompanyByWatchlist()
  }

  onWatchlistAddFormSubmit() {
    if (this.watchlistAddForm.valid) {

      this.watchlistAddForm.value['watchlistName'] = this.watchlistAddForm.value['watchlistName'].toUpperCase()
      let submitData = this.watchlistAddForm.value;
      if (this.isEdit) {
        submitData["watchlistId"] = this.activeFinancialTab;
        this.watchlistService.updateWatchlist(submitData).subscribe(
          (success) => {
            this.getAllWatchlist(true);
            this.watchlistAddForm.reset();
            this.modalReference.close()
            let message = "Watchlist Updated";

            this.toastr.success(message, "", { timeOut: 3000, disableTimeOut: false, });

          },
          (error) => {
            let message = error.error && error.error.message ? error.error.message : "Failed to update watchlist";
            this.toastr.error(message, "", { timeOut: 3000, disableTimeOut: false, });

            console.error(error);
          }
        )
      } else {
        this.watchlistService.createWatchlist(submitData).subscribe(
          (success) => {
            this.getAllWatchlist(true);
            this.watchlistAddForm.reset();
            this.modalReference.close()
            let message = "Watchlist Added";
            this.toastr.success(message, "", { timeOut: 3000, disableTimeOut: false, });

          },
          (error) => {
            let message = error.error && error.error.message ? error.error.message : "Failed to create watchlist";
            this.toastr.error(message, "", { timeOut: 3000, disableTimeOut: false, });
            console.error(error);

          }
        )
      }

    } else {
      //alert('WatchList Name is Invalid.');
    }
  }

  handleDeleteWatchlist() {
    console.log('activeFinancialTab:', this.activeFinancialTab);

    this.watchlistService.deleteWatchlist(this.activeFinancialTab).subscribe(
      (success) => {
        this.getAllWatchlist(true);
        this.modalReference.close()
      },
      (error) => {
        console.error(error);
      }
    );
  }

  short(val) {

    console.log(val);
    switch (val) {
      case 'Alphabetical':
        this.alphaArrowicon = true;
        this.changeArrowicon = false;
        break;
      case 'Change':
        this.alphaArrowicon = false;
        this.changeArrowicon = true;
        break;
    }
  }

  longPress(start = true,companyId=null) {

    if (start) {
        console.log('start');
        // pressTimer
      this.pressTimer = setTimeout(() => {
        this.longPressState = true;
      if (companyId) {
        this.componeysIds.push(companyId)
      }
      }, 1000);

    }

    else {
      console.log('end');

      clearTimeout(this.pressTimer);
    }
  }

  sort_list(type: string) {
    if (type == 'alphabet_sort') {
      this.show_name_arrow=true;
      this.show_percentage_arrow=false;
      switch (this.alphabet_sort) {
        case "asc":
          this.alphabet_sort = "desc";
          this.alphabet_sort_icon = "assets/icons/down_arrow.svg";
          this.watchlistCompany.sort((a, b) => { return this.compare(a, b, "name", this.alphabet_sort) });
          break;
        case "desc":
          this.alphabet_sort = "asc";
          this.alphabet_sort_icon = "assets/icons/filter-arrow-up-icon.svg";
          this.watchlistCompany.sort((a, b) => { return this.compare(a, b, "name", this.alphabet_sort) });
          break;
        default:
          this.alphabet_sort = "asc";
          this.alphabet_sort_icon = "assets/icons/filter-arrow-up-icon.svg";
          this.watchlistCompany.sort((a, b) => { return this.compare(a, b, "name", this.alphabet_sort) });
          break;
      }
    }
    else {
      this.show_percentage_arrow=true;
      this.show_name_arrow=false;
      switch (this.percentage_change_sort) {
        case "asc":
          this.percentage_change_sort = "desc";
          this.percentage_change_sort_icon = "assets/icons/down_arrow.svg";
          this.watchlistCompany.sort((a, b) => { return this.compare(a, b, "percentage_change", this.percentage_change_sort) });
          break;
        case "desc":
          this.percentage_change_sort = "asc";
          this.percentage_change_sort_icon = "assets/icons/filter-arrow-up-icon.svg";
          this.watchlistCompany.sort((a, b) => { return this.compare(a, b, "percentage_change", this.percentage_change_sort) });
          break;
        default:
          this.percentage_change_sort = "asc";
          this.percentage_change_sort_icon = "assets/icons/filter-arrow-up-icon.svg";
          this.watchlistCompany.sort((a, b) => { return this.compare(a, b, "percentage_change", this.percentage_change_sort) });
          break;
      }
    }
  }

  compare(a, b, field, sort_order) {
    if (sort_order == 'desc') {
      if (a[field] > b[field]) {
        return -1;
      }
      if (a[field] < b[field]) {
        return 1;
      }
      return 0;
    }
    else {
      if (a[field] < b[field]) {
        return -1;
      }
      if (a[field] > b[field]) {
        return 1;
      }
      return 0;
    }

  }

  handleClick(cid){

    if(this.componeysIds.includes(cid)){
      const index = this.componeysIds.indexOf(cid);
        if (index > -1) {
          this.componeysIds = this.componeysIds.filter((value)=>( cid !== value));
        }
    }
    else{
     this.componeysIds.push(cid);
    }
    console.log(this.componeysIds);
  }
  handleDeleteCompany(){

    this.watchlistService.removeCompanyFromWatchlist(this.activeFinancialTab,this.componeysIds).subscribe(
      success =>{
        let message = "Company removed";

        this.toastr.success(message, "", { timeOut: 3000, disableTimeOut: false, });
        this.getCompanyByWatchlist();
      },
      failed =>{
        console.error(failed);
        let message = "Failed to removed";

        this.toastr.error(message, "", { timeOut: 3000, disableTimeOut: false, });
        
      }
    )  
    
  }

  handleSearchChange(e: any) {
    this.searchInput = e.target.value;
    this.searchInput = this.searchInput.trim()

    if (this.searchInput && this.searchInput.length > 1) {
      this.searchBlock = true;
      this.searchLoading = true;
      this.stocksService.requestStockSearch(this.searchInput)
        .subscribe(res => {
          this.searchLoading = false;
          this.searchedResults = res.result
        })
    }
  }

  
  handleOnBlurSearch() {
    this.searchBlock = false;
    // this.isMobileResolution = false;
  }

  handleAddToWatchlist(companyId:string){
    this.searchBlock =false;
    this.searchInput =""
      if (this.handleIsAdded(companyId)) {
        let message = "Already added in your watchlist.";
        this.toastr.info(message, "", { timeOut: 3000, disableTimeOut: false, });
        
      }else{

      this.watchlistService.addCompanyToWatchlist(this.activeFinancialTab,companyId).subscribe(
        success =>{
      // this.watchListAdded  = true;
        let message = "Company Added.";

        this.toastr.success(message, "", { timeOut: 3000, disableTimeOut: false, });
        console.log(success);
        // this.getAllWatchlist();
        this.getCompanyByWatchlist()
        },
        failed =>{
          console.error(failed);
          
        }
      )       
    }
  }
  handleIsAdded(companyId) :boolean{
    return this.watchlistCompanyIds.includes(companyId);
  }

  cancelDelete() {
    this.longPressState = false;
    this.componeysIds=[];
  }
}
