import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-indices',
  templateUrl: './top-indices.component.html',
  styleUrls: ['./top-indices.component.scss']
})
export class TopIndicesComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  handleViewAll(){
    this.router.navigate(['explore/stocks/all-stocks'])
  }
}
