import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopIndicesComponent } from './top-indices.component';

describe('TopIndicesComponent', () => {
  let component: TopIndicesComponent;
  let fixture: ComponentFixture<TopIndicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopIndicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopIndicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
