import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopEtfProvidersComponent } from './top-etf-providers.component';

describe('TopEtfProvidersComponent', () => {
  let component: TopEtfProvidersComponent;
  let fixture: ComponentFixture<TopEtfProvidersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopEtfProvidersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopEtfProvidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
