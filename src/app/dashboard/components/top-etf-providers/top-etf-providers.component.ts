import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';

@Component({
  selector: 'app-top-etf-providers',
  templateUrl: './top-etf-providers.component.html',
  styleUrls: ['./top-etf-providers.component.scss']
})
export class TopEtfProvidersComponent implements OnInit {

  constructor(private router: Router,private stockService: StocksService) { }
  topEtfs:any[] = [];
  limit: number = 10;
  offset: number = 1;

  ngOnInit(): void {
    this.stockService.getTopEtfs({
      limit: this.limit, offset: this.offset
    }).subscribe( res => {
      this.topEtfs = res.companies;console.log("res",res);
      if(this.topEtfs && this.topEtfs.length > 4){
        this.topEtfs = this.topEtfs.slice(0,4);
        console.log(["this.topEtfs", this.topEtfs]);
        
      }
    })
  }

  handleViewAll(){
    this.router.navigate(['explore/etfs'])
  }
}
