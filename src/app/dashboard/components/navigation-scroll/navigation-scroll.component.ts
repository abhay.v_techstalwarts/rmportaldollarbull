import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/@core/service/navigation.service';
import { AuthService } from 'src/app/@core/service/auth/auth.service';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/@core/service/common/common.service';



@Component({
  selector: 'app-navigation-scroll',
  templateUrl: './navigation-scroll.component.html',
})
export class NavigationScrollComponent implements OnInit {

  constructor(public navigationService: NavigationService,private commonService: CommonService,private authService: AuthService,private router: Router,) { }
  isAuthenticated: boolean = false;
  isGuestUser: boolean = false;
  dollarbullStats: any = {};
  hasInvested:boolean=false;
  ngOnInit(): void {
    this.commonService.getDollarbullStats().subscribe((res) => {
      this.dollarbullStats = res;
    });
    this.isAuthenticated = this.authService.isAuthenticated();
    this.hasInvested = this.authService.getHasInvested();
    this.isGuestUser = this.authService.isGuestUser();
  }

}
