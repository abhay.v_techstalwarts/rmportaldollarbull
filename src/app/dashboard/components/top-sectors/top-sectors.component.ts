import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/@core/service/navigation.service';
import {CommonService} from '../../../@core/service/common/common.service'
@Component({
  selector: 'app-top-sectors',
  templateUrl: './top-sectors.component.html',
})

export class TopSectorsComponent implements OnInit {
  topSectorData = [{ name: 'Information technology', img: 'assets/icons/it.svg' },
  { name: 'Health Care', img: 'assets/icons/health.svg' },
  { name: 'Consumer Staples', img: 'assets/icons/consumer-staples.svg' },
  { name: 'Communication Services', img: 'assets/icons/comunication.svg' },
  { name: 'Consumer Discretionary', img: 'assets/icons/consumer.svg' },
  { name: 'Materials', img: 'assets/icons/consumer.svg' },
  { name: 'Fiancials', img: 'assets/icons/financial.svg' },
  { name: 'Energy', img: 'assets/icons/consumer.svg' },]

  topSectorImg = ['assets/icons/it.svg', 'assets/icons/health.svg', 'assets/icons/consumer-staples.svg', 'assets/icons/comunication.svg','assets/icons/consumer.svg','assets/icons/financial.svg']
  constructor(private commonService: CommonService, private navigationService: NavigationService) { }

  ngOnInit(): void {
    this.commonService.getTopSectorsMaster().subscribe(
      (res) => {
        this.topSectorData = res;
      }
    )

  }

  handleNavigation(cardData: any) {
    this.navigationService.navigate(['/explore/stocks/all-stocks'], {
      queryParams: { sector: cardData.name },
    });
  }
}
