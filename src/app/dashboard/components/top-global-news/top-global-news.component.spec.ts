import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopGlobalNewsComponent } from './top-global-news.component';

describe('TopGlobalNewsComponent', () => {
  let component: TopGlobalNewsComponent;
  let fixture: ComponentFixture<TopGlobalNewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopGlobalNewsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopGlobalNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
