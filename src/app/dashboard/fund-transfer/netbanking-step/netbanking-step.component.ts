import { Component, OnInit } from '@angular/core';
import { LrsService } from 'src/app/@core/service/lrs/lrs.service';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-netbanking-step',
  templateUrl: './netbanking-step.component.html',
  styleUrls: ['./netbanking-step.component.scss']
})
export class NetbankingStepComponent implements OnInit {
  bankList: any = [];
  onlineSteps: any = [];
  net_banking_url: string = "";
  reference_link: any = []
  toastId = 0;
  selectedBank: any;
  pdfLink;
  constructor(private lrsService: LrsService,
    private toastr: ToastrService, private sanitizer: DomSanitizer
  ) { }

  ngOnInit(): void {
    this.getBanksList();
  }

  getBanksList() {
    this.lrsService.getBanks().subscribe((res) => {
      this.bankList = res;
      this.selectedBank = this.bankList[0];
      this.getBankSteps(this.selectedBank.id);
    });
  }



  bankListChange() {
    console.log(this.selectedBank);
    this.getBankSteps(this.selectedBank.id);
  }

  copiedToClipboard($event) {
    console.log($event);
    if (this.toastId) {
      this.toastr.remove(this.toastId);
    }
    let { toastId } = this.toastr.info("Copied to clipboard!", $event.content, {
      timeOut: 3000,
      disableTimeOut: false,

      // toastId:this.toastId,
      newestOnTop: true,

    });
    this.toastId = toastId;

  }
  getBankSteps(bank_id) {
    let body = {
      "bank_master_id": bank_id
    }
    this.lrsService.RemitOnline(body).subscribe((res) => {
      let response: any = res;
      this.onlineSteps = response.data;
      this.net_banking_url = response.net_banking_url;
      if (response.reference_link) {
        this.reference_link = response.reference_link;
      }
      else{
        this.reference_link = [];
      }
    });
  }

  emailAndDownloadLRSProcess() {
    this.lrsService.mailLrsProcess(this.selectedBank.id).subscribe((res) => {
      // this.pdfLink = "http://dollar-bull.s3.ap-south-1.amazonaws.com/1615902435.pdf";
      let response:any = res;
      this.pdfLink = response.s3;
      this.downloadPDF()
    });
  }

  downloadPDF() {
    window.open(this.pdfLink);
    // var a = document.createElement('a');
    // a.href = this.pdfLink;
    // a.target = '_blank';
    // a.download = 'A2Form.pdf';
    // document.body.appendChild(a);
    // a.click();
  }

  openInternetBankingLink() {
    window.open(this.net_banking_url);
  }

  openURL(url:string){
    window.open(url);
  }

  getSanitisedLink(url:string){
    let tmp_url = 'https://www.youtube.com/embed/';
    let tmp_array = url.split('/');
    if (tmp_array.length > 0) {
      tmp_url = tmp_url + tmp_array[tmp_array.length-1];
    }
    return this.sanitizer.bypassSecurityTrustResourceUrl(tmp_url);
  }

}
