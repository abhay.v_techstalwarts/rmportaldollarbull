import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NetbankingStepComponent } from './netbanking-step.component';

describe('NetbankingStepComponent', () => {
  let component: NetbankingStepComponent;
  let fixture: ComponentFixture<NetbankingStepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NetbankingStepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetbankingStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
