import { Component, OnInit } from '@angular/core';
import { LrsService } from 'src/app/@core/service/lrs/lrs.service';

@Component({
  selector: 'app-fund-transfer-bank',
  templateUrl: './fund-transfer-bank.component.html',
  styleUrls: ['./fund-transfer-bank.component.scss']
})
export class FundTransferBankComponent implements OnInit {

  constructor(private lrsService: LrsService) { }
  bankList:any=[];
  pdfPath:string;
  selectBank;
  amount;
  ngOnInit(): void {
    this.getBanksList();
  }

  getBanksList() {
      this.lrsService.getBanks().subscribe((res) => {
        this.bankList = res;
        this.selectBank = this.bankList[0].id;

        // this.getBankSteps(this.bankList[0].id);
      });
  }

  bankListChange(target: EventTarget) {
   this.selectBank = (target as HTMLInputElement).value;
  }

  isNumber(evt) {
    let code = evt.keyCode || evt.which;
    let regex = new RegExp("^\\d+$");
    if (evt.key.match(regex) || code == "9" || code == "8") {
      return true;
    }
    return false;
  }

  getBankSteps() {
    let body = {
      "bank_master_id": this.selectBank,
      "amount":this.amount
    }
    this.lrsService.RemitOffline(body).subscribe((res:any) => {
      console.log(['lrsService', res]);
      this.pdfPath = res.pdf;
      console.log(this.pdfPath);
      this.downloadPDF();
    });
  }

  downloadPDF() {
      window.open(this.pdfPath);
      //this.webviewService.open_URL(this.pdfPath);
      // var a         = document.createElement('a');
      // a.href        = this.pdfPath; 
      // a.target      = '_blank';
      // a.download    = 'Beneficiary details.pdf';
      // document.body.appendChild(a);
      // a.click();
  }

}