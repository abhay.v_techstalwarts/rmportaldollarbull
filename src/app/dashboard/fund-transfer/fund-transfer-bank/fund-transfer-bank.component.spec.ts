import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundTransferBankComponent } from './fund-transfer-bank.component';

describe('FundTransferBankComponent', () => {
  let component: FundTransferBankComponent;
  let fixture: ComponentFixture<FundTransferBankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FundTransferBankComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundTransferBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
