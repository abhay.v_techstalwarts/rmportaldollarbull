import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/@core/service/auth/auth.service';
import { NavigationService } from 'src/app/@core/service/navigation.service';

@Component({
  selector: 'app-fund-transfer',
  templateUrl: './fund-transfer.component.html',
  styleUrls: ['./fund-transfer.component.scss']
})
export class FundTransferComponent implements OnInit {
  // displayFundTransferCards: any[] = [];
  displayFundTransferCards: any[] = [
    {
      title: 'Remit funds online',
      info: 'You must fund your account from a bank account you own. You may not transfer funds from third party',
      iconPath: 'assets/icons/netbanking.svg',
      navigateTo: `netbanckingstep`,
    },
    {
      title: 'Remit funds through bank',
      info: 'You must fund your account from a bank account you own. You may not transfer funds from third party',
      iconPath: 'assets/icons/a2process.svg',
      navigateTo: `fundtransferbank`,
    },

  ];
  constructor(   private navigationService: NavigationService,
    private authService: AuthService) { }

  ngOnInit(): void {
  }

}
