import { JwtService } from 'src/app/@core/service/jwt.service';
import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/@core/service/common/common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {
  client_list:any;
  filterTerm:string;
  viewportfoliobtn:boolean;
  constructor(private router: Router,
    private commonService: CommonService, 
    private jwtService: JwtService) { }

  ngOnInit(): void {
    this.getRMClientList();
  }

  getRMClientList(){
    this.commonService.get_RM_Client_List().subscribe((res) => {
      let response: any = res;
      this.client_list = response.result.client;

    });
  }

}
