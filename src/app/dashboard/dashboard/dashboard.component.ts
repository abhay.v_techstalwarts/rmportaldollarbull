import { JwtService } from 'src/app/@core/service/jwt.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/@core/service/common/common.service';
import { AuthService } from 'src/app/@core/service/auth/auth.service';
import { NavigationService } from 'src/app/@core/service/navigation.service';
import { CustomerService } from 'src/app/@core/service/customer/customer.service';
import { AccountService } from 'src/app/@core/service/account/account.service';
// rm
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
  hasInvested: boolean = false;

  myWatchListData = false;
  myWatchListEmpty = true;
  watchListTabs: any[] = [];
  customer_invested_value = 0;
  customer_current_value = 0;
  profit_loss_value = 0;
  profit_loss_per = "";
  myInvestementData = [];
  portfolioTabs: any[] = [
    {
      label: 'Line Chart',
      link: '0',
      preventDefault: true,
      icon: 'assets/icons/line-chart-tab-icon.svg'
    },
    {
      label: 'Pie Chart',
      link: '1',
      preventDefault: true,
      icon: 'assets/icons/pie-chart-tab-icon.svg'
    },
  ];
  activePortfolioTab: string = '0';

  label = [];

  pieLabels = { 'etf': "ETFs", "stocks": "Stocks" };
  dataSets = [
    {
      data: [],
      label: 'Performance',
    },
  ];

  monthsInPerformanceCharts: any = [
    { text: '1M', value: '1M', selected: true },
    { text: '3M', value: '3M' },
    { text: '6M', value: '6M' },
    { text: '1Y', value: '1Y' },
  ];

  pieChartLabels = ['Stocks', 'ETFs', 'Bonds'];
  pieChartValues = [30, 50, 20];

  dollarbullStats: any = {};
  performanceData = [];
  dataLoaded = false;
  watchlistCompany = [];
  accountBalance: number = 0;
  // rm portal
  advisor_code:any;
  partner_code:any;
  client_list:any;
  referClientForm:any;
  pending_client_list:any;
  // end rm portal

  constructor(private router: Router,
    private commonService: CommonService, 
    private authService: AuthService, 
    public navigationService: NavigationService, 
    private customerService: CustomerService,
    private accountService: AccountService,
    private fb: FormBuilder,
    // rm
    private jwtService: JwtService ,
    private toastr: ToastrService,
    
    ) 
    {
    // this.commonService.getCustomerCurrentStage().subscribe((current_stage) => {
    //   if (current_stage > 0) {
    //     this.accountService.getCustomerAccount(true).subscribe((accounts)=>{
    //       if(!this.accountService.getSelectedAccount() && accounts.length){
    //         this.accountService.setSelectedAccount(accounts[0].customer_tradestation_account_id)
    //         // this.getTradeStationBalance();
    //       }
    //     })
    //   }
    // });
    // this.hasInvested = this.authService.getHasInvested();
    // this.initiliseAPP();
  }

  initiliseAPP() {
    this.setProfileData();
  }

  setProfileData() {
    let tmp = this.customerService.getCustomerProfile();
    if (tmp == "") {
      this.customerService.getProfile().subscribe((res) => {
        let data: any = res;
        this.customerService.saveCustomerProfile(data.result);
      });
    }
  }

  ngOnInit(): void {

    // rm
    this.getCodes();
    this.getRMClientList();
    this.referClientForm = this.fb.group({
      email_id: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
    });
    // end rm
    this.get_my_investment();
  }

  getCodes(){
    this.advisor_code = this.jwtService.getRM_AdvisorCode();
    this.partner_code= this.jwtService.getPartnercode();
    console.log(this.jwtService.getToken());
  }

  referClient(){
    // console.log(this.referClientForm.get('email_id'));
    // return;
    let referCF = this.referClientForm.get('email_id');
    console.log("testing email => ",referCF.value.trim().length);
    // if(referCF.value){

    // }
    if( (!referCF.value && referCF.value.trim().length < 3)  || (referCF.errors && referCF.errors.email) ){
      this.toastr.error("Please Enter Valid Email ID", "", { timeOut: 3000, disableTimeOut: false, });
      return;
    }
    
    let data={
      email_id: this.referClientForm.get('email_id').value,
    }

    this.commonService.refer_Client(data).subscribe((res) => {
      let response: any = res;

      if(response.status == 1){
        // alert(response.message);
        this.toastr.success(response.message, "", { timeOut: 3000, disableTimeOut: false, });

        this.referClientForm.reset();
        this.getRMClientList();
      }

    },
    fail =>{

      this.toastr.error(fail.error.message, "", { timeOut: 3000, disableTimeOut: false, });
    }
    );
  }

  getRMClientList(){
    this.commonService.get_RM_Client_List().subscribe((res:any) => {
      if(res.statusCode == 9085){
        this.toastr.error(res.message, "", { timeOut: 3000, disableTimeOut: false, });
        localStorage.clear();
        this.authService.logout().subscribe((res:any) => {
          this.navigationService.stackFirst(['/splash']);
        });
      }
      else{
      let response: any = res;
      this.client_list = response.result.client.splice(0,10);
      let pendingData = response.result.pending.reverse();
      this.pending_client_list = pendingData.splice(0,10)
      }


    });
  }

  get_my_investment() {
    var data = {
      perPage: 5,
      page: 1,
    }

  }


  handleViewAll() {
    this.router.navigate(['investment']);
  }

  handleCompleteProfile(e: any) {
    alert("Coming Soon !!!");
  }


  handleOnClick(e: any) {
    console.log('tabs', e.item.link)
    this.activePortfolioTab = e.item.link;
  }
}
