import { JwtService } from 'src/app/@core/service/jwt.service';
import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/@core/service/common/common.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-pending-invites',
  templateUrl: './pending-invites.component.html',
  styleUrls: ['./pending-invites.component.scss']
})
export class PendingInvitesComponent implements OnInit {
  pending_client_list:any;
  filterTerm: string;
  constructor(private router: Router,
    private commonService: CommonService, 
    private jwtService: JwtService) { }
  ngOnInit(): void {
    this.getRMClientList();
  }
  getRMClientList(){
    this.commonService.get_RM_Client_List().subscribe((res) => {
      let response: any = res;
      this.pending_client_list = response.result.pending;

    });
  }

}
