import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NewsArticlesService } from 'src/app/@core/service/news-articles/news-articles.service';
@Component({
  selector: 'app-news-articles',
  templateUrl: './news-articles.component.html',
  styleUrls: ['./news-articles.component.scss']
})
export class NewsArticlesComponent implements OnInit {
  page=1;
  perPage=5;
  newsList=[];
  loading=true;
  newSummarylength:number=150;
  constructor(private router: Router, 
    private newsArticlesService: NewsArticlesService
    ) { }

  ngOnInit(): void {

    this.newsArticlesService.getNews({perPage:this.perPage, page:this.page}).subscribe((res)=>{
      this.newsList = res.records;
      this.loading=false;
    })

  }

  handleClick() {
    this.router.navigate(['account/article']);
  }

  handleNewsClick(item:any) {
    //this.webviewService.open_URL(item.url);
    window.open(item.url);
    // this.router.navigate([`account/news-articles/${item.company_news_id}`]);
  }

  loadMore(){
      this.loading=true;
      this.page =this.page+1
    this.newsArticlesService.getNews({perPage:this.perPage, page:this.page}).subscribe((res)=>{
      this.newsList = [...this.newsList, ...res.records];
      this.loading=false;

    })
  }

}
