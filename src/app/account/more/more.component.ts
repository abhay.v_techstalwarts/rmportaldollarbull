import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/@core/service/customer/customer.service';

@Component({
  selector: 'app-more',
  templateUrl: './more.component.html',
  styleUrls: ['./more.component.scss']
})
export class MoreComponent implements OnInit {
  userData:any;
  accountCards = [
    { title: "Terms & Conditions",  iconPath: "assets/icons/tc.svg", navigateTo: `terms-condition`, ld: true },
    { title: "Privacy Policy",iconPath: "assets/icons/pc.svg", navigateTo: `privacy-policy`, ld: true },
    { title: "Security", iconPath: "assets/icons/lock.svg", navigateTo: "security", ld: true }
  ];
  constructor(private customerService:CustomerService) { }

  ngOnInit(): void {
    this.userData = this.customerService.getCustomerProfile();
    
  }

}
