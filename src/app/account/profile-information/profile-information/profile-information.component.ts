import { Component, OnInit } from '@angular/core';
import { JwtService } from 'src/app/@core/service/jwt.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CustomerService } from 'src/app/@core/service/customer/customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-profile-information',
  templateUrl: './profile-information.component.html',
  providers: [NgbModal]
})
export class ProfileInformationComponent implements OnInit {
userData:any;
updateMobileNoForm: FormGroup;
  constructor(
    private jwtService: JwtService, private modalService: NgbModal,private fb: FormBuilder, private customerService:CustomerService
    ,private toastr: ToastrService) { 
      this.initialise_popup();
    }

  initialise_popup(){
    this.updateMobileNoForm = this.fb.group({
      mobile_no: ['', [Validators.required, Validators.pattern('[1-9]{1}[0-9]{9}')]],
    });
  }

  ngOnInit(): void {
    console.log("Inside Profile info");
    
    this.userData = this.customerService.getCustomerProfile();

  }

  openUpdateMobilePopup(updateMobileNoModal){
    this.initialise_popup();
    this.modalService.open(updateMobileNoModal);
  }

  updateMobileNoFormSubmit(){
    this.updateMobileNoForm.controls['mobile_no'].setValue(this.updateMobileNoForm.controls['mobile_no'].value.toString());
    this.customerService.updateMobileNo(this.updateMobileNoForm.value).subscribe((success)=>{
      this.modalService.dismissAll();
      let message = "Mobile no updated.";
      this.toastr.success(message, "", { timeOut: 3000, disableTimeOut: false, });
      this.customerService.getProfile().subscribe((res)=>{
        let data : any = res;
        this.customerService.saveCustomerProfile(data.result);
        this.userData = this.customerService.getCustomerProfile();
      });
    },
    (error)=>{
      let message = "Failed to update mobile no.";
      this.toastr.error(message, "", { timeOut: 3000, disableTimeOut: false, });
    });
    
  }

  cancelPopup(){
    this.initialise_popup();
    this.modalService.dismissAll();
  }

}
