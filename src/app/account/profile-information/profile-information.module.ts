import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileInformationRoutingModule } from './profile-information-routing.module';
import { ProfileInformationComponent } from './profile-information/profile-information.component';
import { SharedModule } from 'src/app/@shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'


@NgModule({
  declarations: [ProfileInformationComponent],
  imports: [
    CommonModule,
    ProfileInformationRoutingModule,
    SharedModule,
    FormsModule, 
    ReactiveFormsModule,
  ]
})
export class ProfileInformationModule { }
