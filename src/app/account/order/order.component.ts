import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router,ActivatedRoute  } from '@angular/router';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';
import { ToastrService } from 'ngx-toastr';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  providers: [NgbModalConfig, NgbModal]
})
export class OrderComponent implements OnInit {
  @ViewChild('SuccessModal') templateRef: TemplateRef<any>;
  companyId:string ="";
  limit:string = '5';
  offset:string = '1';
  orderListData =[];
  pendingOrderListData =[];
  perPage= 5;
  pendingPage= 1;
  executedPage= 1;
  pendingTotalCount= 0;
  executedTotalCount= 0;
  loading= true;
  showLoadBtn = true;
  orderTabs: any[] = [
    {
      link: '/account/order/pending',
      label: 'Pending',
      preventDefault: true,
    },
    {
      link: '/account/order/executed',
      label: 'Executed',
      preventDefault: true,
    },
  ]
  companyName:any;
  buyPrice:any;
  quantity:any;
  orderId:any;
  activeFinancialTab: string = 'Pending';
  setectedOrder:any;
  updateOrderForm: FormGroup;
  tradeActionOption={
    BUY:"BUY",
    SELL:"SELL",
  };
  tradeAction:string;
  modalReference: any;
  stockPreOrderDetail;
  advanceOptionText:boolean=true;
  advanceOptionField:boolean=false;
  stopPriceSection:boolean=false;
  termsDefinationData =[
    {heading : 'Limit Order' , text: 'A limit order is an order to buy or sell a stock at a specific price or better. A buy limit order can only be executed at the limit price or lower, and a sell limit order can only be executed at the limit price or higher'},
    {heading : 'Market Order' , text: 'A market order is an order to buy or sell a security immediately. This type of order guarantees that the order will be executed, but does not guarantee the execution price. A market order generally will execute at or near the current bid (for a sell order) or ask (for a buy order) price.'},
    {heading : 'GTC' , text: 'Good till canceled'},
    {heading : 'FOK' , text: 'Fill or Kill; orders are filled entirely or canceled, partial fills are not accepted'},
    {heading : 'DAY' , text: 'Day, valid until the end of the regular trading session.'},
  ];
  orderDetail:{
    quantity: 0,
    price: 0,
    stopPrice: 0,
    orderType:0,
    orderId:0,
  }
  constructor( private router: Router,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private stockService: StocksService, config: NgbModalConfig, private modalService: NgbModal) {
      config.backdrop = 'static';
    config.keyboard = false;
     }

  ngOnInit(): void {


    this.route.params.subscribe(params => {
      let type = params['type'];
      if (type == "executed") {
        this.activeFinancialTab = 'Executed';
      }else{
        this.activeFinancialTab = 'Pending';
      }
    });
    this.updateOrderForm = this.fb.group({
      quantity: [0, Validators.required],
      price: [0, Validators.required,],
      stopPrice: [0],
      orderType: [ Validators.required],

    });
    
    this.updateOrderForm.controls['price'].disable();
    this.updateOrderForm.controls['stopPrice'].disable();

    this.updateOrderForm.get('orderType').valueChanges.subscribe(val => {
        if (val == "Market" || val == "StopMarket") {
          this.updateOrderForm.controls['price'].disable();
        }else{
          this.updateOrderForm.controls['price'].enable();
        }

        if (val == "StopLimit" || val == "StopMarket") {
          this.updateOrderForm.controls['stopPrice'].enable();
        }else{
          this.updateOrderForm.controls['stopPrice'].disable();
        }
        if(val == "StopLimit" || val == "StopMarket"){
          this.stopPriceSection = true;
          this.updateOrderForm.controls['stopPrice'].enable();
        }

        else{
          this.stopPriceSection = false;
          this.updateOrderForm.controls['stopPrice'].disable();
        }
    });
    let obj ={
      perPage : this.limit,
      page :this.offset,


    }
    this.stockService.executedOrder(obj).subscribe(res => {
      this.orderListData = res.records;
      this.executedTotalCount = res.totalCount;
      this.handleLoadBtn( this.orderListData,res.totalCount)
    this.loading=false;

    })
    
    this.stockService.pendingOrder(obj).subscribe(res=>{
      this.pendingOrderListData = res.records;
      this.pendingTotalCount = res.totalCount;
    this.loading=false;

    })
  }
  handleOnClick(e: any) {
    console.log('tabs',e.item.label)
    this.activeFinancialTab = e.item.label;
    this.router.navigate([e.item.link], {
      replaceUrl: true
    });
    if (this.activeFinancialTab == "Pending") {
      this.handleLoadBtn( this.pendingOrderListData,this.pendingTotalCount)
    }else{
      this.handleLoadBtn( this.orderListData,this.executedTotalCount)
    }
  }

  handleLoadBtn(list,totalCount){
    if(list.length < totalCount){
      this.showLoadBtn=true;
    }else{
      this.showLoadBtn=false;

    }
  }

  loadMore(){
    this.loading=true;
    if (this.activeFinancialTab == "Pending") {
      this.loadMorePending();
    }else{
      this.loadMoreExecuted();
    }
 }

 loadMorePending(){
  this.pendingPage =this.pendingPage+1
  this.stockService.pendingOrder({perPage:this.perPage, page:this.pendingPage}).subscribe((res)=>{
    this.pendingOrderListData = [...this.pendingOrderListData, ...res.records];
    this.handleLoadBtn(this.pendingOrderListData,res.totalCount);
      this.pendingTotalCount = res.totalCount;
      this.loading=false;
  })
  }
  loadMoreExecuted(){
    this.executedPage =this.executedPage+1
  this.stockService.executedOrder({perPage:this.perPage, page:this.executedPage}).subscribe((res)=>{
    this.loading=false;
    this.orderListData = [...this.orderListData, ...res.records];
      this.executedTotalCount = res.totalCount;
      this.handleLoadBtn(this.orderListData,res.totalCount);
  })
  }
  cancelOrderDetail($event,order ,cancleOrderDetail){
    $event.stopPropagation();
     this.setectedOrder = order;

  
    this.modalService.open(cancleOrderDetail);
    console.log(order);
  }
  cancleOrderModal(cancleOrders){
    // $event.stopPropagation();
    this.modalService.dismissAll();
    this.modalService.open(cancleOrders);
  
  }
  cancleOrder(){
    // $event.stopPropagation();
    this.stockService.cancelOrder(this.setectedOrder.orderId).subscribe((success)=>{
     let message ="Order Canceled";
      this.toastr.success(message,"",{timeOut: 3000,disableTimeOut:false,});

      this.stockService.pendingOrder({perPage:this.pendingOrderListData.length, page:1}).subscribe((res)=>{
        this.pendingOrderListData = res.records;
        this.handleLoadBtn(this.pendingOrderListData,res.totalCount);
          this.pendingTotalCount = res.totalCount;
          this.loading=false;
          this.modalService.dismissAll();
          this.companyName = '';
          this.buyPrice = '';
          this.quantity = '';
          this.companyName = '';
          this.orderId = '';
      })
    },
    fail=>{
      let message =  fail.error && fail.error.message ? fail.error.message :"Failed To Cancel order";
      this.toastr.error(message,"",{timeOut: 3000,disableTimeOut:false,});
          console.error(fail);
      
    })
  }

  advanceOption(){
    // this.advanceOptionText = !this.advanceOptionText;
    this.advanceOptionField = !this.advanceOptionField;
  }

  openBuyStocksModal(BuyStocksModal: any, tradeAction,companyId, orderId ) {
    this.advanceOptionField=false;
    this.stockPreOrderDetail=null;
    this.stockService.preOrderDetail(companyId, orderId).subscribe(res => {
      this.stockPreOrderDetail = res;
      this.orderDetail={
        quantity: this.stockPreOrderDetail.order.quantity,
        price:this.stockPreOrderDetail.order.price? this.stockPreOrderDetail.order.price:0,
        stopPrice: this.stockPreOrderDetail.order.stop_price? this.stockPreOrderDetail.order.stop_price:0,
        orderType: this.stockPreOrderDetail.order.order_type,
        orderId:orderId,
      }
    })

    this.tradeAction = tradeAction;
    this.modalService.dismissAll();
    this.modalReference = this.modalService.open(BuyStocksModal);
  }

  openModal(ModelId: any){
    this.modalReference = this.modalService.open(ModelId);
  }

  onOrderFormSubmit() {
    if (this.updateOrderForm.valid) {
      this.stockService.updateOrder(this.orderDetail).subscribe(
        (success) => {
          // this.modalReference.close()
          this.modalService.dismissAll();

          this.toastr.success("Order Updated","", { timeOut: 3000, disableTimeOut: false, });
          this.stockService.pendingOrder({perPage:this.pendingOrderListData.length, page:1}).subscribe((res)=>{
            this.pendingOrderListData = res.records;
            this.handleLoadBtn(this.pendingOrderListData,res.totalCount);
              this.pendingTotalCount = res.totalCount;
              this.loading=false;
              this.modalService.dismissAll();
          })
        },
        (error) => {
        // this.toastr.error('Hello world!', 'Toastr fun!');

          console.error(error);
          console.log(error);
          if (error && error.error && error.error.statusCode == 1001) {
            
            this.toastr.error("Please Login with Trade Station","", { timeOut: 3000, disableTimeOut: false, });

            //TODO Trade LOGIN Handle
          }else{
            let message =  error.error ? error.error.message :"";
        
            this.toastr.error("", message, { timeOut: 3000, disableTimeOut: false, });
          }
        }
      )

    } else {
      //alert('order data is Invalid.');
    }
  }

  openTermsModal(termsModal) {
    this.modalService.open(termsModal ,{ scrollable: true });
  }

  

}
