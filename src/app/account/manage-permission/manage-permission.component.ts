import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/@core/service/auth/auth.service';
import { PlatformService } from 'src/app/@core/service/platform.service';

@Component({
  selector: 'app-manage-permission',
  templateUrl: './manage-permission.component.html',
})
export class ManagePermissionComponent implements OnInit {
  isGuestUser: boolean = false;
  platform: string = '';
  smsPermission = false;
  screenLock = false;

  constructor(
    private platformService: PlatformService,
    private authService: AuthService,
  ) {}

  ngOnInit(): void {
    this.isGuestUser = this.authService.isGuestUser();
    this.platform = this.platformService.getPlatform();
  }

  handlePermissions(permission: string) {
    if (permission == 'sms') {
      this.smsPermission = !this.smsPermission;
    }

    if (permission == 'screen') {
      const result = !this.screenLock;
      this.screenLock = result;
    }
    //TODO: Just for check after api int wil remove
    // console.log({ screen: this.screenLock, sms: this.smsPermission });
  }
}
