import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-safe-secure',
  templateUrl: './safe-secure.component.html'
})
export class SafeSecureComponent implements OnInit {

  constructor() { }

  infoCards = [
    { title: "Sec-registered - compliance.", 
    info: "Dollarbull has partnered with FINRA and SEC registered broker TradeStation. View more details at https://brokercheck.finra.org/firm/summary/39473", 
    iconPath: "assets/icons/register-complaint.svg" },
    { title: "Insurance", 
    info: "Your Dollarbull accounts through Tradestation are protected with SIPC insurance. SIPC insures your equities accounts up to $500,000, including $250,000 for cash. Beyond this, accounts get an additional protection through Lloyd’s of London, insuring each account up to $24.5 million, subject to a $900,000 per account maximum for cash, with an aggregate firm limit of $300 million. For more information on SIPC coverage, we encourage you to visit the SIPC website.",
     iconPath: "assets/icons/insurance.svg" },
    { title: "Data Privacy & Encryption", 
    info: "Dollarbull uses 256-bit encryption and Secure Sockets Layer (SSL) to ensure the security of our platform and to protect all your information. We also employ state of the art log-in methods, automatic logouts, and ID verification to help prevent unauthorized access.",
     iconPath: "assets/icons/data-pract.svg" },
    // { title: "Reputed and experienced team.", info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum molestie nibh at vehicula.", iconPath: "assets/icons/icon-user.png" },

  ]

  ngOnInit(): void {
  }

}
