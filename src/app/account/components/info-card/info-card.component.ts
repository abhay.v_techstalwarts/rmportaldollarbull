import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-safe-info-card',
  templateUrl: './info-card.component.html'
})
export class InfoCardComponent implements OnInit {

  constructor() { }

  @Input() title: any;
  @Input() iconPath: any;
  @Input() info: any;

  ngOnInit(): void {
  }

}
