import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-account-card',
  templateUrl: './account-card.component.html'
})
export class AccountCardComponent implements OnInit {

  constructor() { }

  @Input() title: any;
  @Input() iconPath: any;
  @Input() info: any;
  @Input() navigateTo : any;

  ngOnInit(): void {

  }

}
