import { Component, Input, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-trade-station-account-card',
  templateUrl: './trade-station-account-card.html'
})
export class TradeStationAccountCardComponent implements OnInit {

  constructor() { }

  @Input() title: any;
  @Input() iconPath: string;
  @Input() subTitle: any;
  @Output()
  onEditClick: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit(): void {
  }
  handleEditClick(e: any){
    if (this.onEditClick) {
      this.onEditClick.emit(e);
    }
  }
}
