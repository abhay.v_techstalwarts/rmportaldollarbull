import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeStationAccountCardComponent } from './trade-station-account-card';

describe('TradeStationAccountCardComponent', () => {
  let component: TradeStationAccountCardComponent;
  let fixture: ComponentFixture<TradeStationAccountCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradeStationAccountCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeStationAccountCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
