import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/@core/service/auth/auth.service';
import { NavigationService } from 'src/app/@core/service/navigation.service';
import { JwtService } from 'src/app/@core/service/jwt.service';
import { CommonService } from '../../@core/service/common/common.service';
import { CustomerService } from '../../@core/service/customer/customer.service';
import { Plugins } from '@capacitor/core';
const { Browser } = Plugins

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
})
export class AccountComponent implements OnInit {
  WITHDRAW_FUND_URL :string= "https://clientcenter.tradestation.com/support/Transfer_Funds_Cashiering.aspx";
  isAuthenticated: boolean = false;
  isGuestUser: boolean = false;
  accountBalance:0;
  current_stage:number=0;
  accountCards: any[] = [
    // {
    //   title: 'Profile Information',
    //   info: 'Information Complete',
    //   iconPath: 'assets/icons/icon-profile.png',
    //   navigateTo: `profile-information`,
    //   allowedNoGuestAuth: false,
    // },
    {
      title: 'Tradestation Accounts',
      info: 'View your secondry accounts',
      iconPath: 'assets/icons/trade-station-icon-modal.png',
      navigateTo: `trade-station`,
      allowedNoGuestAuth: true,
    },
    {
      title: 'Talk to Us',
      info: 'Submit queries',
      iconPath: 'assets/icons/icon-help.png',
      navigateTo: `need-help`,
      allowedNoGuestAuth: true,
    },
    // {
    //   title: ' Funds',
    //   info: 'Manage your wallet and bank account',
    //   iconPath: 'assets/icons/icon-wallet.png',
    //   navigateTo: `wallet`,
    //   allowedNoGuestAuth: false,
    // },
    {
      title: 'Orders',
      info: 'See successful, failed & pending transactions',
      iconPath: 'assets/icons/icon-order.png',
      navigateTo: 'order',
      allowedNoGuestAuth: true,
    },
    {
      title: 'Notification',
      info: 'Read news and articles related to investment',
      iconPath: 'assets/icons/notification-icon-background.svg',
      navigateTo: '../notification',
      allowedNoGuestAuth: true,
    },
    {
      title: 'News and Articles',
      info: 'Read news and articles related to investment',
      iconPath: 'assets/icons/icon-article.png',
      navigateTo: `news-articles`,
      allowedNoGuestAuth: true,
    },
    // {
    //   title: 'US Stock account',
    //   info: 'Account details',
    //   iconPath: 'assets/icons/icon-user-add.png',
    //   navigateTo: 'us-stock-account',
    //   allowedNoGuestAuth: false,
    // },
    // {
    //   title: 'Risk Profile',
    //   info: 'Check your risk profile now.',
    //   iconPath: 'assets/icons/icon-meter.png',
    //   navigateTo: `risk-profile`,
    //   allowedNoGuestAuth: false,
    // },
    {
      title: 'Dollarbull safe and secure',
      info: 'Your data is safe with us',
      iconPath: 'assets/icons/icon-verified.png',
      navigateTo: `safe-secure`,
      allowedNoGuestAuth: true,
    },

    {
      title: 'Manage Permissions',
      info: 'Secure your account on Dollarbull app.',
      iconPath: 'assets/icons/icon-fingerprint.png',
      navigateTo: `manage-permissions`,
      allowedNoGuestAuth: false,
    },
    {
      title: 'More...',
      info: 'View terms & conditions , privacy policy, disclaimer',
      iconPath: 'assets/icons/icon-more.png',
      navigateTo: 'more',
      allowedNoGuestAuth: true,
    },
  ];
  displayAccountCards: any[] = [];
  userData:any;

  constructor(
    private navigationService: NavigationService,
    private authService: AuthService,
    private jwtService: JwtService,
    private commonService: CommonService,
    private router: Router,
    private customerService:CustomerService
  ) {
    
  }

  ngOnInit(): void {
    this.current_stage = this.commonService.getCurrentStage();
    this.isGuestUser = this.authService.isGuestUser();
    this.isAuthenticated = this.authService.isAuthenticated();
    this.userData = this.customerService.getCustomerProfile();console.log(this.userData);
    this.displayAccountCards = this.accountCards.filter((card: any) => {
      // display all cards for auth but non-guest user
      if (!this.isGuestUser) {
        return true;
      }

      // display allowed cards to non-guest user
      if (card.allowedNoGuestAuth) {
        return true;
      }

      return false;
    });
    if (this.current_stage > 0) {
      this.getTradeStationBalance();
    }
  }

  getTradeStationBalance(){
    this.commonService.getaccountBalance().subscribe((accountBalance) => {
      this.accountBalance = accountBalance;
    },
      (err) => {
        console.log(err)
        this.accountBalance = 0;
      }
    );
  }

  handleLogout() {
    localStorage.clear();
    this.authService.logout().subscribe((res) => {
      this.navigationService.stackFirst(['/splash']);
    });
  }

  handleLogin() {
    this.authService.authenticate('1').subscribe((res) => {
      this.navigationService.stackFirst(['/splash']);
    });
  }

  openWithdrawLink(){
    Browser.open({ url: this.WITHDRAW_FUND_URL})
  }

  navigateToProfile(){
    this.router.navigate(['/dashboard'], { replaceUrl: true });
  } 
}
