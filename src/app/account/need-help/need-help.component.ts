import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CustomerService } from 'src/app/@core/service/customer/customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-need-help',
  templateUrl: './need-help.component.html'
})
export class NeedHelpComponent implements OnInit {

  helpForm:FormGroup;
   
    constructor( private fb: FormBuilder, private customerService :CustomerService, private toastr: ToastrService) {
   }
  
  ngOnInit(): void {
    this.helpForm = this.fb.group({
      query: ['', [Validators.required]],
      description: ['',[Validators.required]],
    });
  }

  handleSubmit(){
    if (this.helpForm.valid) {
 
        this.customerService.submitQuery( this.helpForm.value).subscribe(
          (success) => {
            this.helpForm.reset()
            let message = "Query Submited Successfully";
            this.toastr.success(message, "", { timeOut: 3000, disableTimeOut: false, });

          },
          (error) => {
            let message = error.error && error.error.message ? error.error.message : "Failed to create watchlist";
            this.toastr.error(message, "", { timeOut: 3000, disableTimeOut: false, });
            console.error(error);

          }
        )
      

    } else {
      let message = 'Query & Description are not allowed to be empty' ;
        this.toastr.error(message, "", { timeOut: 3000, disableTimeOut: false, });
    }
}

}
