import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleApiDisclosureComponent } from './google-api-disclosure.component';

describe('GoogleApiDisclosureComponent', () => {
  let component: GoogleApiDisclosureComponent;
  let fixture: ComponentFixture<GoogleApiDisclosureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoogleApiDisclosureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleApiDisclosureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
