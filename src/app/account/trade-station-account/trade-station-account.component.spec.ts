import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeStationAccountComponent } from './trade-station-account.component';

describe('TradeStationAccountComponent', () => {
  let component: TradeStationAccountComponent;
  let fixture: ComponentFixture<TradeStationAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradeStationAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeStationAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
