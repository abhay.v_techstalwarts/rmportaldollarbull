import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/@core/service/account/account.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgbModalConfig, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-trade-station-account',
  templateUrl: './trade-station-account.component.html',
  providers: [NgbModalConfig, NgbModal]
})
export class TradeStationAccountComponent implements OnInit {
  editAccountTagForm: FormGroup;
  accounts=[];
  constructor(
    private accountService:AccountService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private modalService: NgbModal,
    ) { }

  ngOnInit(): void {
    this.editAccountTagForm = this.fb.group({
      customer_tradestation_account_id: ['', [Validators.required]],
      tag: ['', [Validators.required, Validators.maxLength(15)]],
    });
    this.getAccounts(true);
  }

  getAccounts(isForceCall =false){
    this.accountService.getCustomerAccount(isForceCall).subscribe((accounts)=>{
      this.accounts = accounts;
    })
  }


  handleSyncClick(){
    this.accountService.syncAccounts().subscribe(success=>{
      this.getAccounts(true);
    },
    error=>{
      console.error(error);
      let message = "Failed to Sync Account";
      this.toastr.error(message, "", { timeOut: 3000, disableTimeOut: false, });
    })
  }

  handleEditClick(EditAccountTagModal,account){
    this.editAccountTagForm.patchValue({
      tag: account.tag ,
      customer_tradestation_account_id:account.customer_tradestation_account_id
    });
    this.modalService.open(EditAccountTagModal, { centered: true });
  }

  onEditAccountTagFormSubmit(){
    if (this.editAccountTagForm.valid) {
      this.accountService.updateAccountTag(this.editAccountTagForm.value).subscribe(success=>{
        let message = "Tag Name Updated";
        this.toastr.success(message, "", { timeOut: 3000, disableTimeOut: false, });
        this.getAccounts(true);
        this.modalService.dismissAll();
      },
      error=>{
        let message = error.error && error.error.message ? error.error.message : "Failed to update Tag Name";
        this.toastr.error(message, "", { timeOut: 3000, disableTimeOut: false, });
      console.error(error);
    })
      

    }else{
      let message = "Invalid Tag name";
      this.toastr.error(message, "", { timeOut: 3000, disableTimeOut: false, });
    }
  }
}
