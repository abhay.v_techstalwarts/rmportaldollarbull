import { Component, OnInit } from '@angular/core';
import { StocksService } from '../../../@core/service/stocks/stocks.service'
@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
})
export class CategoryListComponent implements OnInit {

  constructor(private stockService: StocksService) { }
  offset: number = 0
  limit: number = 20
  topEtfs: Array<any> = []

  ngOnInit(): void {
    this.getEtfs()
  }

  getEtfs = () => {
    this.stockService.getTopEtfs(
      { offset: this.offset, limit: this.limit }
    ).subscribe(res => {
      if (res && res.companies && res.companies.length) {
        this.topEtfs = res.companies
        console.log(this.topEtfs)
      }
    })
  }
}
