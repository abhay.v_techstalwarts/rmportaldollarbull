import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoryListComponent } from './category-list/category-list.component';
import {TopEtfsComponent} from './top-etfs/top-etfs.component'

const routes: Routes = [
  { path: '', pathMatch: 'full', component:  CategoryListComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EtfsRoutingModule { }
