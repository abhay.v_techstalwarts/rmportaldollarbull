import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'stocks',
    loadChildren: () =>
      import('./stocks/stocks.module').then((m) => m.StocksModule),
  },
  {
    path: 'etfs',
    loadChildren: () =>
      import('./etfs/etfs.module').then((m) => m.EtfsModule),
  },
  {
    path: '',
    redirectTo: 'stocks',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: 'not-found',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExploreRoutingModule {}
