import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { StocksService } from '../../../@core/service/stocks/stocks.service';
import { CommonService } from '../../../@core/service/common/common.service';
import { PlatformService } from "../../../@core/service/platform.service";
@Component({
  selector: 'app-all-stocks',
  templateUrl: './all-stocks.component.html',
  providers: [NgbModalConfig, NgbModal],
})
export class AllStocksComponent implements OnInit {
  isMobileFilterOpen: boolean = false;

  constructor(
    config: NgbModalConfig,
    private modalService: NgbModal,
    private router: Router,
    private stockService: StocksService,
    private commonService: CommonService,
    private route: ActivatedRoute,
    private platformService: PlatformService,
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
  }
  isDesktop:boolean;
  filter: any;
  activeFilter: any;
  activeFilterIndex: number = 0;
  tempSelectedFilterData: any = {};
  selectedFilterData: any = {};
  resetFilterData: any = {};
  tempKeyMap = {
    'Sort By': 'sort',
    Category: 'category',
    'Day Returns': 'dayreturns',
    Sector: 'sector',
  };

  isMobileResolution: boolean;
  searchInput: string = '';
  searchBlock = false;
  searchedResults = [];
  strings: any = {
    searchPlaceholder: 'Search Stocks',
  };

  allStocks: any = [];
  allStocksCount: number = 0;
  offset: number = 0;
  limit: number = 10;
  loading: number = 1;

  ngOnInit(): void {
    this.isDesktop = this.platformService.isDesktop();
    //GET FILTER MASTER
    this.commonService.getFilterMaster().subscribe((res) => {
      this.filter = res;
      if (!this.isDesktop || !this.activeFilter) {
        this.activeFilter = res[0];
        this.activeFilterIndex = 0;
      }
      res.forEach((item) => {
        this.tempSelectedFilterData[item.name] = [];
        this.selectedFilterData[item.name] = [];
        this.resetFilterData[item.name] = [];
      });

      this.route.queryParams.subscribe(params => {
        if (params.hasOwnProperty('sector')) {
          this.tempSelectedFilterData['Sector'] = [...this.tempSelectedFilterData['Sector'], params.sector];
          this.selectedFilterData['Sector'] = [...this.selectedFilterData['Sector'], params.sector];
          this.resetFilterData['Sector'] = [...this.resetFilterData['Sector'], params.sector];
        }
      })

      this.fetchStocks(true);
    });
  }

  fetchStocks(forceCall = false, loadmore = false) {
    if (forceCall && !loadmore) {
      this.offset =0;
      this.limit =10;
    }
    let payload = {
      offset: this.offset,
      limit: this.limit,
    };


    Object.keys(this.selectedFilterData).forEach((key) => {
      if (this.selectedFilterData[key].length) {
        const keyName = this.tempKeyMap[key];
        payload[keyName] = this.selectedFilterData[key].join('#');
      }
    });



    this.stockService.getAllStocks(payload, (forceCall|| loadmore)).subscribe((res) => {
      let companyList = res.companyList;
      this.loading = 0;
      if (companyList) {
        if (forceCall) {
          console.log(['companyList', companyList]);
          
          this.allStocks = companyList;
        } else {
          this.allStocks = [...this.allStocks, ...companyList];
        }
      }
      this.allStocksCount = res.stocksCount;
    });
  }

  handleClick(e?: any) {
    this.router.navigate(['explore/stocks/1']);
  }

  handleLoadMore(e?: any) {
    this.offset += this.limit;
    if (this.loading == 0) {
      this.loading = 1;
      this.fetchStocks(false, true);
    }
  }

  handleMobileFilterClick(e?: any) {
    this.tempSelectedFilterData = JSON.parse(
      JSON.stringify(this.selectedFilterData)
    );
    console.log(this.tempSelectedFilterData, this.selectedFilterData);
    this.isMobileFilterOpen = true;
  }

  handleMobileFilterClose(e?: any) {
    this.isMobileFilterOpen = false;
    this.activeFilter = this.filter[0];
    this.activeFilterIndex = 0;
  }

  handleMobileFilterReset(e?: any) {
    this.selectedFilterData = JSON.parse(JSON.stringify(this.resetFilterData));
    this.tempSelectedFilterData = JSON.parse(
      JSON.stringify(this.resetFilterData)
    );
    this.fetchStocks(true);
    this.handleMobileFilterClose();

  }

  handleMobileFilterApply(e?: any) {
    //GET ALL STOCKS
    this.selectedFilterData = JSON.parse(
      JSON.stringify(this.tempSelectedFilterData)
    );
    this.fetchStocks(true);
    if (!this.isDesktop) {
      this.handleMobileFilterClose();
    }
  }

  handleMobileFilterSelect(e: any, filter: any, i: number) {
    this.activeFilterIndex = i;
    this.activeFilter = filter;
  }

  handleMobileFilterChange(e: any, value: any) {
    const isSingleSelection =
      this.activeFilter.type === 'single' ? true : false;

    if (isSingleSelection) {
      this.tempSelectedFilterData[this.activeFilter.name][0] = value;
    } else {
      if (this.tempSelectedFilterData[this.activeFilter.name].includes(value)) {
        this.tempSelectedFilterData[
          this.activeFilter.name
        ] = this.tempSelectedFilterData[this.activeFilter.name].filter(
          (item) => item !== value
        );
      } else {
        this.tempSelectedFilterData[this.activeFilter.name].push(value);
      }
    }
  }

  handleStockClick(companyId) {
    if (!companyId) {
      return;
    }

    this.router.navigate(['explore/stocks/' + companyId]);
  }

  handleSearchChange(e: any) {
    this.searchInput = e.target.value;
    this.searchInput = this.searchInput.trim();

    if (this.searchInput && this.searchInput.length > 1) {
      this.searchBlock = true;
      this.stockService
        .requestStockSearch(this.searchInput)
        .subscribe((res) => {
          this.searchedResults = res.result;
        });
    }
  }

  handleSearchClick(e: any) {
    console.log('Called');
    if (window.innerWidth < 768) {
      this.isMobileResolution = true;
    } else {
      this.isMobileResolution = false;
    }
  }

  handleOnBlurSearch(event) {
    this.searchBlock = false;
    this.isMobileResolution = false;
  }
}
