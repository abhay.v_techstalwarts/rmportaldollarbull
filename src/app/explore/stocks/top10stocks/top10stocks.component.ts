import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';
@Component({
  selector: 'app-top10stocks',
  templateUrl: './top10stocks.component.html',
  styleUrls: ['./top10stocks.component.scss']
})
export class Top10stocksComponent implements OnInit {

  constructor(private router: Router,private stockService: StocksService) { }
  Top10StocksList:any[] = [];
  limit: number = 10;
  offset: number = 0;


  ngOnInit() {
    this.stockService.getTop10Stocks({
      limit: 10, offset: this.offset
    }, true).subscribe(res => {
      this.Top10StocksList = res.companies
    })
  }

}
