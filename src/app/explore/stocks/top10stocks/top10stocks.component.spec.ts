import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Top10stocksComponent } from './top10stocks.component';

describe('Top10stocksComponent', () => {
  let component: Top10stocksComponent;
  let fixture: ComponentFixture<Top10stocksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Top10stocksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Top10stocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
