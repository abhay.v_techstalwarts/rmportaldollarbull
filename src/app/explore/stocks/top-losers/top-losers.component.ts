import { Component, OnInit } from '@angular/core';

import { StocksService } from '../../../@core/service/stocks/stocks.service'
@Component({
  selector: 'app-top-losers',
  templateUrl: './top-losers.component.html',
})
export class TopLosersComponent implements OnInit {
  constructor(private stockService: StocksService) { }
  topGainerLosers = [];
  ngOnInit() {
    this.stockService.getTopGainerLoserStocks({
      limit: 20, offset: 1
    }).subscribe( res => {
      this.topGainerLosers = res
    })
  }
}
