import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ByCategoryLayoutComponent } from './by-category-layout.component';

describe('ByCategoryLayoutComponent', () => {
  let component: ByCategoryLayoutComponent;
  let fixture: ComponentFixture<ByCategoryLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ByCategoryLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ByCategoryLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
