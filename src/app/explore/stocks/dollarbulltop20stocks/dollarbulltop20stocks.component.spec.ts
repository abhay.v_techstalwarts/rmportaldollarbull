import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Dollarbulltop20stocksComponent } from './dollarbulltop20stocks.component';

describe('Dollarbulltop20stocksComponent', () => {
  let component: Dollarbulltop20stocksComponent;
  let fixture: ComponentFixture<Dollarbulltop20stocksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Dollarbulltop20stocksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Dollarbulltop20stocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
