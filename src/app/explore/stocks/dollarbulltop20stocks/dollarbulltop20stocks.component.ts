import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';
@Component({
  selector: 'app-dollarbulltop20stocks',
  templateUrl: './dollarbulltop20stocks.component.html',
  styleUrls: ['./dollarbulltop20stocks.component.scss']
})
export class Dollarbulltop20stocksComponent implements OnInit {
  dollarbullTop20List:any[] = [];
  limit: number = 20;
  offset: number = 0;
  constructor(private router: Router,private stockService: StocksService) { }
  

  ngOnInit() {
    this.stockService.getDollarbullTop20({
      limit: 20, offset: 0
    }).subscribe( res => {
      this.dollarbullTop20List = res.companies
    })
  }

}
