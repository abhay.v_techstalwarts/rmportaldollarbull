import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostTradedComponent } from './most-traded.component';

describe('MostTradedComponent', () => {
  let component: MostTradedComponent;
  let fixture: ComponentFixture<MostTradedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostTradedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostTradedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
