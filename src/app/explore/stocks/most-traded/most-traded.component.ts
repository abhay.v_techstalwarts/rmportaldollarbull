import { Component, OnInit } from '@angular/core';
import {StocksService} from '../../../@core/service/stocks/stocks.service'
@Component({
  selector: 'app-most-traded',
  templateUrl: './most-traded.component.html',
  styleUrls: ['./most-traded.component.scss']
})
export class MostTradedComponent implements OnInit {
  limit: number = 10;
  loading: number = 1;
  offset: number = 0;
  constructor(private stocksService: StocksService) { }
  mostTradedStocks = []
  ngOnInit(): void {
    this.fetchStocks(true);
    // this.stocksService.getMostTradedStocks({
    //   limit: 20, offset: 1
    // })
    // .subscribe(res => {
    //   console.log(res)
    //   this.mostTradedStocks = res
    // })
    
  }

  fetchStocks(forceCall = false, loadmore = false) {
    if (forceCall && !loadmore) {
      this.offset =0;
      this.limit =10;
    }
    let payload = {
      offset: this.offset,
      limit: this.limit,
    };



    this.stocksService.getMostTradedStocks(payload, (forceCall|| loadmore)).subscribe((res) => {
      let companyList = res;
      this.loading = 0;
      if (companyList) {
        if (forceCall) {
          console.log(['companyList', companyList]);
          
          this.mostTradedStocks = companyList;
        } else {
          this.mostTradedStocks = [...this.mostTradedStocks, ...companyList];
        }
      }
    });
  }

  handleLoadMore(e?: any) {
    this.offset += this.limit;
    if (this.loading == 0) {
      this.loading = 1;
      this.fetchStocks(false, true);
    }
  }

}
