import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StocksByIdComponent } from './stocks-by-id.component';

describe('StocksByIdComponent', () => {
  let component: StocksByIdComponent;
  let fixture: ComponentFixture<StocksByIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StocksByIdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
