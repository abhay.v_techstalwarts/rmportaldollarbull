import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router,ActivatedRoute  } from '@angular/router';
import { NgbModalConfig, NgbModal, NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';
import { WatchlistService } from 'src/app/@core/service/watchlist/watchlist.service';
import { CommonService } from '../../../@core/service/common/common.service';
import { FormBuilder, Validators, FormGroup,NumberValueAccessor } from '@angular/forms';
// import { Modals } from '@capacitor/core';
import { ToastrService } from 'ngx-toastr';
import { PlatformService } from "../../../@core/service/platform.service";
@Component({
  selector: 'app-stocks-by-id',
  templateUrl: './stocks-by-id.component.html',
  providers: [NgbModalConfig, NgbModal, NgbDropdownConfig],
})


export class StocksByIdComponent implements OnInit {
  @ViewChild('SuccessModal') templateRef: TemplateRef<any>;
  @ViewChild("newsScroll") newsScroll : ElementRef;
  isScroll2 = true;
  stockDetailsLoading = false;
  showShortDesciption = true;
  isvaluation:boolean = false;
  istechnicals:boolean = false;
  isbalance_sheet:boolean = false;
  iscash_flow:boolean = false;
  isincome_statement:boolean = false;
  issharedstats:boolean = false;
  watchListAdded:boolean=false;
  devidant = [];
  closeResult = '';
  scrollRight100 = true;
  scrollLeft100 = false ;
  title = '';
  pointerName = '';
  watchlistactive = true;

    financialTabs: any[] = [
    {
      link: 'valuation',
      preventDefault: true,
      label: 'Valuation',

    },
    {
      link: 'technicals',
      preventDefault: true,
      label: 'Technical',
    },
    {
      link: 'balanceSheet',
      preventDefault: true,
      label: 'Balance Sheet',
    },
    {
      link: 'cashflow',
      preventDefault: true,
      label: 'Cash Flow',

    },
    {
      link: 'income_statement',
      preventDefault: true,
      label: 'Income Stmt',

    },
    {
      link: 'sharedstats',
      preventDefault: true,
      label: 'Share Stats',

    }
  ];
  activeFinancialTab: string = 'valuation';

  label = [];
  data = [];
  dataSets : any;

  dataLoaded = false;
  stockPreOrderDetail;
  monthsInPerformanceCharts: any = [
    { text: '1W', value: '1W' },
    { text: '1M', value: '1M', selected: true },
    { text: '3M', value: '3M' },
    { text: '6M', value: '6M' },
    { text: '1Y', value: '1Y' },
    { text: '5Y', value: '5Y' },

  ];
  watchlists:any[] =[];
  watchlistLoading:boolean =true;
  companyId:string ="";
  modalReference: any;

  orderForm: FormGroup;
  orderData={
    quantity: 0,
    price: 0,
    stopPrice: 0,
    orderType: "Market",
    duration: "GTC",

  }
  isSubmitClicked = false;
  tradeActionOption={
    BUY:"BUY",
    SELL:"SELL",
  };
  isPositive=0;
  stats="";
  tradeAction:string;
  companyNews:any =[];
  advanceOptionText:boolean=true;
  advanceOptionField:boolean=false;
  stopPriceSection:boolean=false;
  currentStage=0;
  isDesktop;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    config: NgbModalConfig,
    dropdownConfig:NgbDropdownConfig,
    private watchlistService: WatchlistService,
    private modalService: NgbModal,
    private stockService: StocksService,
    private fb: FormBuilder,
    private commonService: CommonService,
    private toastr: ToastrService,
    private platformService: PlatformService,
    ) {
    this.isDesktop = this.platformService.isDesktop();
    config.backdrop = 'static';
    config.keyboard = false;
    dropdownConfig.placement = 'top-left';
    dropdownConfig.autoClose = false;

    this.commonService.getCustomerCurrentStage(false).subscribe(currentStage=>{
      this.currentStage = currentStage;
    })
  }
  stockStats:any=[];
  termsDefinationData =[
    {heading : 'Limit Order' , text: 'A limit order is an order to buy or sell a stock at a specific price or better. A buy limit order can only be executed at the limit price or lower, and a sell limit order can only be executed at the limit price or higher'},
    {heading : 'Market Order' , text: 'A market order is an order to buy or sell a security immediately. This type of order guarantees that the order will be executed, but does not guarantee the execution price. A market order generally will execute at or near the current bid (for a sell order) or ask (for a buy order) price.'},
    {heading : 'GTC' , text: 'Good till canceled'},
    {heading : 'DAY' , text: 'Day, valid until the end of the regular trading session.'},
  ];

  ngOnInit(): void {
    this.orderForm = this.fb.group({
      quantity: [0,Validators.compose([Validators.required ,Validators.min(1)])],
      price: [0, Validators.required,],
      stopPrice: [0],
      orderType: ["Market", Validators.required],
      duration: ["GTC"],

    });
    
    this.orderForm.controls['price'].disable();
    this.orderForm.controls['stopPrice'].disable();

    this.orderForm.get('orderType').valueChanges.subscribe(val => {
        if (val == "Market" || val == "StopMarket") {
          this.orderForm.controls['price'].disable();
        }else{
          this.orderForm.controls['price'].enable();
        }

        if (val == "StopLimit" || val == "StopMarket") {
          this.orderForm.controls['stopPrice'].enable();
        }else{
          this.orderForm.controls['stopPrice'].disable();
        }
        if(val == "StopLimit" || val == "StopMarket"){
          this.stopPriceSection = true;
          this.orderForm.controls['stopPrice'].enable();
        }

        else{
          this.stopPriceSection = false;
          this.orderForm.controls['stopPrice'].disable();
        }
    });
    this.route.params.subscribe(params => {
      this.stockDetailsLoading=true;
      this.companyId = params['id'];
      this.stockService.getStockStatsById(this.companyId, true).subscribe(res => {
        this.stockStats = res;
        console.log(this.stockStats.cash_flow);
        this.isvaluation = Object.values(this.stockStats.valuation).every(x => x == null || x == '' ||  x == 0);
        this.istechnicals = Object.values(this.stockStats.technicals).every(x => x == null || x == '' ||  x == 0);
        this.isbalance_sheet = Object.values(this.stockStats.balance_sheet).every(x => x == null || x == '' ||  x == 0);
        this.iscash_flow = Object.values(this.stockStats.cash_flow).every(x => x == null || x == '' ||  x == 0);
        this.isincome_statement = Object.values(this.stockStats.income_statement).every(x => x == null || x == '' ||  x == 0);
        this.issharedstats = Object.values(this.stockStats.sharedstats).every(x => x == null || x == '' ||  x == 0);



        console.log(this.stockStats.result.top_10_holdings);
        if(this.stockStats.company.current_price){
          let price =this.stockStats.company.current_price ?this.stockStats.company.current_price:0;
          let yesterday_price =this.stockStats.company.yesterday_price ?this.stockStats.company.yesterday_price:0;
          let price_diff = price - yesterday_price;
          if (price_diff > 0) {
            this.isPositive = 1
          }else if (price_diff < 0) {
            this.isPositive = 2
          } else {
            this.isPositive = 0        
          }
        
          let percentage_val = 0;
          if (yesterday_price > 0) {
            percentage_val = (price - yesterday_price)/ yesterday_price*100;
          }
          this.stats = "$" + price_diff.toFixed(2) + "(" + percentage_val.toFixed(2) + "%)";
        }
        
      })
      this.stockService.getCompanyNewsById(this.companyId).subscribe(res=>{
        this.companyNews = res;
        // console.log(["this.companyNews",this.companyNews]);
      })
      this.generateGraph(this.companyId, "6M");
      this.getAllWatchlist();
    });
  }
  checkAge(checkNull) {
    return checkNull == null;
  }

  ngAfterViewInit(): void {
  
    setTimeout(() => {
      const scrollContainer = document.getElementById("newsScroll");
      scrollContainer.addEventListener("wheel", (evt:any) => {
        scrollContainer.scrollLeft += evt.deltaY;
        evt.preventDefault();
        });
  
        window.addEventListener("resize", (evt:any) => {
          this.watchScroll();
          evt.preventDefault();
        });
        this.watchScroll();
     
    }, 1000);
  



}
watchScroll(pos = null){

  let time = (pos == null) ? 1000 : 0;
  setTimeout(()=>{
    let target = this.newsScroll.nativeElement;
    let clientWidth = target.clientWidth;
    let scrollWidth = target.scrollWidth;
    let scrollCurr = target.scrollLeft;
    let scrollBy = 50;
   
    if(scrollWidth == clientWidth){
      this.isScroll2 = false;
       return;
    }else{
      this.isScroll2 = true;
    }

    if(pos == 'R'){
      scrollBy =  scrollCurr + scrollBy;
    }else if(pos == 'L'){
      scrollBy =  scrollCurr - scrollBy;
    }else{
      return;
    }

    if(scrollBy < 0 ){
      target.scrollLeft = 0;
      this.scrollLeft100 = false ;
    }else if(scrollCurr >= (scrollWidth - clientWidth) && pos == 'R'){
      target.scrollLeft = scrollWidth;
      this.scrollRight100 = false;
    }else if(scrollCurr <= (scrollWidth - clientWidth)){
      target.scrollLeft = scrollBy;
      this.scrollRight100 = true;
      this.scrollLeft100 = true ;
      
    }


  },time);

} //watchScroll Ends

 generateGraph(company_id, count){
  this.commonService.getStockPriceChart({
    "company_id": company_id,
    "count": count
  }).subscribe(res => {
    this.stockDetailsLoading=false;
    this.label = [];    
    this.dataSets = res;
    this.dataLoaded = true;
    var elmnt = document.getElementById("content");
    elmnt?.scrollIntoView();
    this.title = this.stockStats.company.name + ' price movement';
    this.pointerName = this.stockStats.company.symbol;
  },
  fail =>{
    this.stockDetailsLoading=false;
  });
 }

 alterDescriptionText() {
    this.showShortDesciption = !this.showShortDesciption
 }
  openBuyStocksModal(BuyStocksModal: any, tradeAction:any ) {
    if (this.currentStage > 1) {
      this.advanceOptionField=false;
      this.stockPreOrderDetail=null;
      this.stockService.preOrderDetail(this.companyId).subscribe(res => {
        this.stockPreOrderDetail = res;
      })
  
      this.tradeAction = tradeAction;
      this.modalReference = this.modalService.open(BuyStocksModal);
    }else{
      this.router.navigate(['/kyc'])
    }
  }
  openTermsModal(termsModal) {
    this.modalService.open(termsModal ,{ scrollable: true });
  }
  openConfermModal(confermModal) {
    this.modalService.open(confermModal ,{ scrollable: true });
  }

  openModal(ModelId: any){
    this.modalReference = this.modalService.open(ModelId);
  }
  closeModal(){
    this.orderForm.reset({
      quantity: 0,
      price: 0,
      stopPrice: 0,
      orderType: "Market",
      duration: "GTC",

    });
    this.modalService.dismissAll();
  }

  handleClickViewNewsAll() {
    this.router.navigate(['account/news-articles']);
  }

  changeGraph(month: any) {
    // console.log([month]);
    this.generateGraph(this.companyId, month);
  }

  handleOnClick(e: any) {
    console.log('tabs',e.item.link)
    this.activeFinancialTab = e.item.link;
  }

  handleWatchlistClick(selectWatchList:any) {
    this.modalService.open(selectWatchList);
  }

  getAllWatchlist(watchlistLoading = true){
    this.watchlistLoading = watchlistLoading;
    this.watchlistService.getWatchlistByCompany(this.companyId).subscribe((res) => {
      this.watchlists=[];
      res.forEach((d:any) => {
        this.watchlists.push({
          label: d.watchlistName,
          link: d.watchlistId,
          isAdded: d.isAdded,
          preventDefault: true,
        });
        if (d.isAdded) {
          this.watchListAdded = true;
        }
      });
    this.watchlistLoading = false;
    },
    error=>{
      console.error(error);
      this.watchlistLoading = false;
    }
    )
  }

  handleAddRemoveToWatchlist(watchlistId:string, isAdded :boolean){
    if (isAdded) {
      this.watchlistService.removeCompanyFromWatchlist(watchlistId,this.companyId).subscribe(
        success =>{
         this.watchListAdded  = false;

           this.watchlists = this.watchlists.map((watchlist:any) => {
            if (watchlist.link == watchlistId ) {
              watchlist.isAdded = !watchlist.isAdded;
            }
            return watchlist;
          });
          this.getAllWatchlist(false);
          this.watchlistService.getCompanyByWatchlist(watchlistId,true)
        },
        failed =>{
          console.error(failed);
        }
      )  
    } else {
      this.watchlistService.addCompanyToWatchlist(watchlistId,this.companyId).subscribe(
        success =>{
          console.log(success);
      this.watchListAdded  = true;
           this.watchlists = this.watchlists.map((watchlist:any) => {
              if (watchlist.link == watchlistId ) {
                watchlist.isAdded = !watchlist.isAdded;
              }
              return watchlist;
            });
          this.getAllWatchlist(false);
          this.watchlistService.getCompanyByWatchlist(watchlistId,true)
        },
        failed =>{
          console.error(failed);
          
        }
      )      
    }

    
  }

  addStocksIntoWatchListCategory(){
    
  }

  onOrderFormSubmit(confirmOrders) {
    if (this.orderForm.valid) {
      this.orderData = this.orderForm.value;
      this.modalService.dismissAll();
      this.openConfermModal(confirmOrders);

    } else {
      this.orderForm.markAllAsTouched();
      this.toastr.error("Order data is Invalid","", { timeOut: 3000, disableTimeOut: false, });

    }
  }

  onGoBackConfirm(BuyStocksModal){
    this.modalService.dismissAll();
    this.openConfermModal(BuyStocksModal);
  }

  onOrderConfirm(){
    this.isSubmitClicked = true;
    let orderData = this.orderForm.value;
    orderData.companyId = this.companyId;
    orderData.tradeAction = this.tradeAction;
    this.stockService.orderStock(orderData).subscribe(
      (success) => {
        this.orderForm.reset({
          quantity: 0,
          price: 0,
          stopPrice: 0,
          orderType: "Market",
          duration: "GTC",
    
        });
        this.modalService.dismissAll();
        this.openModal(this.templateRef)
         this.isSubmitClicked = false;

      },
      (error) => {
      // this.toastr.error('Hello world!', 'Toastr fun!');
      this.isSubmitClicked = false;

        console.error(error);
        console.log(error);
        if (error && error.error && error.error.statusCode == 1001) {
          
          this.toastr.error("Please Login with Trade Station","", { timeOut: 3000, disableTimeOut: false, });

          //TODO Trade LOGIN Handle
        }else{
          let message =  error.error ? error.error.message :"";
      
          this.toastr.error("", message, { timeOut: 3000, disableTimeOut: false, });
        }
      }
    )
  }

  handleNavigateOrderStatus(){
    this.modalReference.close()
    this.router.navigate(['account/order'])
  }

  comingSoon(){
    alert("coming soon");
  }

  navigateTransationHistory(){
    if(!this.companyId){ return; }

    this.router.navigate(['explore/stocks/'+this.companyId+'/transation-history'])
  }

  advanceOption(){
    // this.advanceOptionText = !this.advanceOptionText;
    this.advanceOptionField = !this.advanceOptionField;
  }

  getActionText(tradeAction){
    return tradeAction == 'BUY' ? 'INVEST' : 'SELL';
  }

  getPercentageDiff(current_price, pre_close){
    return ((current_price - pre_close)/ pre_close * 100).toFixed(2);;
  }

  getMaxBuyQty(){
    if (this.stockPreOrderDetail.price > 0) {
      return (this.stockPreOrderDetail.accountBalance/this.stockPreOrderDetail.price).toFixed(0);
    }
    else{
      return 0;
    }
  }

  clickOnMaxBuy(){
    this.orderForm.controls['quantity'].setValue((this.stockPreOrderDetail.accountBalance/this.stockPreOrderDetail.price).toFixed(0));
  }
}
