import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';
@Component({
  selector: 'app-top10-etfs',
  templateUrl: './top10-etfs.component.html',
  styleUrls: ['./top10-etfs.component.scss']
})
export class Top10EtfsComponent implements OnInit {
  Top10EtfsList:any[] = [];
  limit: number = 10;
  offset: number = 0;
  constructor(private router: Router,private stockService: StocksService) { }

  ngOnInit() {
    this.stockService.getTop10Ftfs({
      limit: 10, offset: 0
    }).subscribe( res => {
      this.Top10EtfsList = res.companies
    })
  }

}
