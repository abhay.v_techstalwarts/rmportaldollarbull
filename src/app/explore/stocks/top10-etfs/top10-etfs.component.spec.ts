import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Top10EtfsComponent } from './top10-etfs.component';

describe('Top10EtfsComponent', () => {
  let component: Top10EtfsComponent;
  let fixture: ComponentFixture<Top10EtfsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Top10EtfsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Top10EtfsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
