import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// app modules
import { StocksRoutingModule } from './stocks-routing.module';
import { SharedModule } from 'src/app/@shared/shared.module';
// app components
import { StocksByIdComponent } from './stocks-by-id/stocks-by-id.component';
import { StocksCategoryListComponent } from './stocks-category-list/stocks-category-list.component';
import { AllStocksComponent } from './all-stocks/all-stocks.component';
import { AllEtfsComponent } from './all-etfs/all-etfs.component';
import { TopGainersComponent } from './top-gainers/top-gainers.component';
import { TopLosersComponent } from './top-losers/top-losers.component';
import { ByCategoryLayoutComponent } from './components/by-category-layout/by-category-layout.component';
import { SearchComponent } from './search/search.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { MostTradedComponent } from './most-traded/most-traded.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TransactionHistoryComponent } from './transaction-history/transaction-history.component';
import { Dollarbulltop20stocksComponent } from './dollarbulltop20stocks/dollarbulltop20stocks.component';
import { Top10EtfsComponent } from './top10-etfs/top10-etfs.component';
import { Top10stocksComponent } from './top10stocks/top10stocks.component'

@NgModule({
  declarations: [
    StocksByIdComponent,
    StocksCategoryListComponent,
    AllStocksComponent,
    AllEtfsComponent,
    TopGainersComponent,
    TopLosersComponent,
    ByCategoryLayoutComponent,
    SearchComponent,
    MostTradedComponent,
    TransactionHistoryComponent,
    Dollarbulltop20stocksComponent,
    Top10EtfsComponent,
    Top10stocksComponent,
  ],
  imports: [CommonModule, 
    StocksRoutingModule, 
    SharedModule, 
    NgbModule,
    FormsModule, 
    ReactiveFormsModule,
  ],
  exports: [SearchComponent, MostTradedComponent, Dollarbulltop20stocksComponent, Top10EtfsComponent, Top10stocksComponent],
})
export class StocksModule {}
