import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';
import { StocksService } from '../../../@core/service/stocks/stocks.service'
import { getTopStocks, getAllStocks } from '../../../@core/store/stocks/stocks.action'
import { topGainers } from '../../../@core/store/stocks/stocks.selector'
@Component({
  selector: 'app-stocks-category-list',
  templateUrl: './stocks-category-list.component.html',
})
export class StocksCategoryListComponent implements OnInit {
  constructor(private store: Store, private router: Router, private stockService: StocksService) { }

  topGainerLoser: any = {
    'topgainers': [],
    'toplosers': []
  };

  tGainer = []
  tLoser = []
  mostTraded : any = [];

  limit: number = 20;
  offset: number = 0;

  ngOnInit() {
    this.router.navigate(['explore/stocks/all-stocks']);
    this.stockService.getTopGainerLoserStocks({
      limit: this.limit, offset: this.offset
    }).subscribe(res => {
      const tG = JSON.parse(JSON.stringify(res))
      this.tGainer = tG['topgainers'].slice(0,3)
      this.tLoser = tG['toplosers'].slice(0,3)
      // alert(JSON.stringify(this.tGainer));
    })

    this.stockService.getMostTradedStocks({
      limit: 20, offset: this.offset
    }, true).subscribe(res => {
      this.mostTraded = res.slice(0,3)
    })
  }
  navigateToAllStocks() {
    this.router.navigate(['explore/stocks/all-stocks'])
  }

}
