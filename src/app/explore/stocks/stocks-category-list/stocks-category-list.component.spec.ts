import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StocksCategoryListComponent } from './stocks-category-list.component';

describe('StocksCategoryListComponent', () => {
  let component: StocksCategoryListComponent;
  let fixture: ComponentFixture<StocksCategoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StocksCategoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksCategoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
