import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/@core/service/common/common.service';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';
import { Router, ActivatedRoute, Params  } from '@angular/router';
import { NewsArticlesService } from 'src/app/@core/service/news-articles/news-articles.service';


@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
})
export class PortfolioComponent implements OnInit {

  customer_invested_value:any;
  customer_current_value:any;
  profit_loss_value:any;
  profit_loss_per:any;
  dollarbullStats: any = {};
  myInvestementData = [];
  sortBy ="name";
  sortDirection =0;
  portfolioTabs: any[] = [
    {
      link: 'Line_Chart',
      label: 'Line Chart',
      icon:'assets/icons/line-chart-tab-icon.svg',

      preventDefault: true,
    },
    {
      link: 'Pie_Chart',
      label: 'Pie Chart',
      icon:'assets/icons/pie-chart-tab-icon.svg',
      preventDefault: true,
    },
  ];
  activePortfolioTab: string = 'Line_Chart';
  label = ['Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  dataSets = [
    {
      data: [],
      label: 'performance',
    },
  ];
  monthsInPerformanceCharts: any = [
    { text: '1M', value: '1M', selected: true },
    { text: '3M', value: '3M' },
    { text: '6M', value: '6M' },
    { text: '1Y', value: '1Y' },
  ];
  performanceData = [];
  dataLoaded = false;
  pieChartLabels = ['Stocks', 'ETFs', 'Bonds'];
  pieChartValues = [30, 50, 20];
  page:number = 0;
  showLoadMore:boolean=true;
  isMobileSortOpen: boolean = false;
  accountBalance:number= 0;
  newsList: any[] = [];
  newSummarylength: number = 150;
  constructor(private activatedRoute: ActivatedRoute, private commonService: CommonService,private stocksService: StocksService, private router: Router,
    private newsArticlesService: NewsArticlesService) { }

  ngOnInit(): void {
    this.newsArticlesService
    .getNewsArticles({
      limit: 5,
      offset: 1,
      company_id: ""
    })
    .subscribe((res) => {
      this.newsList = res;
    });
    this.getOverview();
    this.get_my_investment();
  }
  handleNewsClick(item:any) {
    window.open(item.url);
    // this.router.navigate([`account/news-articles/${item.company_news_id}`]);
  }

  getOverview(){
    this.commonService.getOverview().subscribe((res) => {
      console.log(['getOverview', res]);
      
      this.customer_invested_value = res.customer_invested_value;
      this.customer_current_value = res.customer_current_value;
      this.profit_loss_value = res.profit_loss_value;
      var pl_per = res.profit_loss_per;
      this.profit_loss_per = pl_per >= 0 ? "+" + pl_per + "%" : "-" + pl_per + "%";
    });

    this.commonService.getaccountBalance().subscribe((accountBalance) => {
      this.accountBalance = accountBalance;
    },
      (err) => {
        console.log(err)
        this.accountBalance = 0;
      }
    );
    this.commonService.getDollarbullStats().subscribe((res) => {
      this.dollarbullStats = res;
    })

    this.commonService.getPerformance(
      {
        customer_id: 1,
        type: "1M"
      }, true
    ).subscribe(res => {
      this.performanceData = res
      this.label = this.performanceData['label']
      this.dataSets[0]['data'] = this.performanceData['data']
      this.dataLoaded = true
    })
    this.commonService.getCustomerInvestmentChart(
      {
        customer_id : 1,
      }, true
    ).subscribe(res => {
      this.pieChartLabels = res.labels;
      this.pieChartValues = res.data;
      console.log({chart :res})
    })
  }

  handleMonthPerformance(month: any) {
    this.commonService.getPerformance(
      {
        customer_id: 1,
        type: month
      }, true
    ).subscribe(res => {
      this.performanceData = res
      this.label = this.performanceData['label']
      this.dataSets[0]['data'] = this.performanceData['data']
      this.dataLoaded = true

    })
  }

  handleOnClick(e: any) {
    console.log('tabs',e.item.link)
    this.activePortfolioTab = e.item.link;
  }

  get_my_investment(){
    let data =  {

    }
    this.stocksService.getOrderList(data).subscribe((res) => {
      this.myInvestementData = [...this.myInvestementData,...res.records ];
      console.log(this.myInvestementData);
      this.showLoadMore = (this.myInvestementData.length == res.totalCount) ? false:true;
    });
  }

  handleMobileSortOpen() {
    this.isMobileSortOpen = true;
  }

  handleSortClick(sortBy="name"){
    if (this.sortBy !== sortBy ) {
      this.sortBy= sortBy;
      this.sortDirection =0;
    }else{
      this.sortDirection = this.sortDirection ? 0: 1;
    }
    this.isMobileSortOpen = true;
    this.page=1;
    let data =  {
      perPage :10,
      page :this.page,
      sort: this.sortBy,
      direction: this.sortDirection
    }
    this.stocksService.getOrderList(data).subscribe((res) => {
      this.myInvestementData =res.records;
      this.showLoadMore = (this.myInvestementData.length == res.totalCount) ? false:true;
      this.handleMobileSortClose();
    });

  }

  handleMobileSortClose() {
    this.isMobileSortOpen = false;
  }
}
