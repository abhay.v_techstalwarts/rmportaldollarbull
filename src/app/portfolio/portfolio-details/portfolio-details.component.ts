import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router,ActivatedRoute  } from '@angular/router';
import { NgbModalConfig, NgbModal, NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';
import { FormBuilder, Validators, FormGroup,  } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-portfolio-details',
  templateUrl: './portfolio-details.component.html',
  providers: [NgbModalConfig, NgbModal, NgbDropdownConfig]
})
export class PortfolioDetailsComponent implements OnInit {
  
  @ViewChild('SuccessModal') templateRef: TemplateRef<any>;
  transactionHistory:any;
  perPage= 5;
  page= 1;
  loading= true;
  showLoadBtn = true;

  companyId:string ="";
  investmentDetails:any;
  modalReference: any;
  tradeAction:string;
  orderData={
    quantity: 0,
    price: 0,
    stopPrice: 0,
    orderType: "Market",
    duration: "GTC",

  }
  isSubmitClicked = false;
  tradeActionOption={
    BUY:"BUY",
    SELL:"SELL",
  };
  orderForm: FormGroup;
  stockPreOrderDetail;
  advanceOptionText:boolean=true;
  advanceOptionField:boolean=false;
  stopPriceSection:boolean=false;
  termsDefinationData =[
    {heading : 'Limit Order' , text: 'A limit order is an order to buy or sell a stock at a specific price or better. A buy limit order can only be executed at the limit price or lower, and a sell limit order can only be executed at the limit price or higher'},
    {heading : 'Market Order' , text: 'A market order is an order to buy or sell a security immediately. This type of order guarantees that the order will be executed, but does not guarantee the execution price. A market order generally will execute at or near the current bid (for a sell order) or ask (for a buy order) price.'},
    {heading : 'GTC' , text: 'Good till canceled'},
    {heading : 'DAY' , text: 'Day, valid until the end of the regular trading session.'},
  ];
  isPositive=0;
  stats="";
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    config: NgbModalConfig,
    dropdownConfig:NgbDropdownConfig,
    private stockService: StocksService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private toastr: ToastrService,
    ) { 
    
    config.backdrop = 'static';
    config.keyboard = false;
    dropdownConfig.placement = 'top-left';
    dropdownConfig.autoClose = false;

  }

  ngOnInit(): void {
    
    this.orderForm = this.fb.group({
      quantity: [0,Validators.compose([Validators.required ,Validators.min(1)])],
      price: [0, Validators.required,],
      stopPrice: [0],
      orderType: ["Market", Validators.required],
      duration: ["GTC"],

    });

    this.orderForm.controls['price'].disable();
    this.orderForm.controls['stopPrice'].disable();

    this.orderForm.get('orderType').valueChanges.subscribe(val => {
        if (val == "Market" || val == "StopMarket") {
          this.orderForm.controls['price'].disable();
        }else{
          this.orderForm.controls['price'].enable();
        }

        if (val == "StopLimit" || val == "StopMarket") {
          this.orderForm.controls['stopPrice'].enable();
        }else{
          this.orderForm.controls['stopPrice'].disable();
        }

        if(val == "StopLimit" || val == "StopMarket"){
          this.stopPriceSection = true;
          this.orderForm.controls['stopPrice'].enable();
        }

        else{
          this.stopPriceSection = false;
          this.orderForm.controls['stopPrice'].disable();
        }
    });

    this.route.params.subscribe(params => {
      this.companyId = params['id'];
      // console.log("this.route.params.subscribe", this.companyId);
      let data = {
        "perPage": 5,
        "page": 1,
        "company_id": this.companyId
      }
      this.stockService.getInvestmentByCompany(data).subscribe(res => {
        this.investmentDetails = res.result.records[0];
        if(this.investmentDetails.diffAmount){
          // console.log(["this.price", this.price, "this.previous_price", this.previous_price]);
          let price_diff = this.investmentDetails.diffAmount
          if (price_diff > 0) {
            this.isPositive = 1
          }else if (price_diff < 0) {
            this.isPositive = 2
          } else {
            this.isPositive = 0        
          }
          let amt = (price_diff * this.investmentDetails.quantity)
          this.stats = "$" + amt.toFixed(2);
        }
      })
      this.stockService.getTransactionHistoy(data).subscribe(res => {
        this.transactionHistory = res.result.records;
        this.handleLoadBtn(res.result.totalCount);
        this.loading= false;

      })
    });
  }

  handleLoadBtn(totalCount){
    if(this.transactionHistory.length < totalCount){
      this.showLoadBtn=true;
    }else{
      this.showLoadBtn=false;

    }
  }


  loadMore(){
    this.loading=true;
    this.page =this.page+1
  this.stockService.getTransactionHistoy({perPage:this.perPage, page:this.page,company_id: this.companyId}).subscribe((res)=>{
    this.transactionHistory = [...this.transactionHistory, ...res.result.records];
    this.handleLoadBtn(res.result.totalCount);
    this.loading=false;

  })
}

  openBuyStocksModal(BuyStocksModal: any, tradeAction ) {
    this.advanceOptionField=false;
    this.stockPreOrderDetail=null;
    this.stockService.preOrderDetail(this.companyId).subscribe(res => {
      this.stockPreOrderDetail = res;
    })

    this.tradeAction = tradeAction;
    this.modalReference = this.modalService.open(BuyStocksModal);
  }

  openModal(ModelId: any){
    this.modalReference = this.modalService.open(ModelId);
  }

  closeModal(){
    this.orderForm.reset({
      quantity: 0,
      price: 0,
      stopPrice: 0,
      orderType: "Market",
      duration: "GTC",

    });
    this.modalService.dismissAll();
  }

  openTermsModal(termsModal) {
    this.modalService.open(termsModal ,{ scrollable: true });
  }

  handleNavigateOrderStatus(){
    this.modalReference.close()
    this.router.navigate(['account/order'])
  }

  onOrderFormSubmit(confirmOrders) {
    if (this.orderForm.valid) {
      this.orderData = this.orderForm.value;
      this.modalService.dismissAll();
      this.openModal(confirmOrders);

    } else {
      this.orderForm.markAllAsTouched();
      this.toastr.error("Order data is Invalid","", { timeOut: 3000, disableTimeOut: false, });
    }
  }

  onGoBackConfirm(BuyStocksModal){
    this.modalService.dismissAll();
    this.openModal(BuyStocksModal);
  }

  onOrderConfirm(){
    this.isSubmitClicked = true;
    let orderData = this.orderForm.value;
    orderData.companyId = this.companyId;
    orderData.tradeAction = this.tradeAction;
    this.stockService.orderStock(orderData).subscribe(
      (success) => {
        this.orderForm.reset({
          quantity: 0,
          price: 0,
          stopPrice: 0,
          orderType: "Market",
          duration: "GTC",
    
        });
        this.modalService.dismissAll();
        this.openModal(this.templateRef)
         this.isSubmitClicked = false;
         let data = {
          "perPage": 5,
          "page": 1,
          "company_id": this.companyId
        }
        this.stockService.getTransactionHistoy(data).subscribe(res => {
          this.transactionHistory = res.result.records;
          this.handleLoadBtn(res.result.totalCount);
          this.loading= false;
  
        })

      },
      (error) => {
      // this.toastr.error('Hello world!', 'Toastr fun!');
      this.isSubmitClicked = false;

        console.error(error);
        console.log(error);
        if (error && error.error && error.error.statusCode == 1001) {
          
          this.toastr.error("Please Login with Trade Station","", { timeOut: 3000, disableTimeOut: false, });

          //TODO Trade LOGIN Handle
        }else{
          let message =  error.error ? error.error.message :"";
      
          this.toastr.error("", message, { timeOut: 3000, disableTimeOut: false, });
        }
      }
    )
  }
  navigateTransationHistory(){
    if(!this.companyId){ return; }
    this.router.navigate(["portfolio/transaction-history/"+this.companyId]);
  }

  comingSoon(){
    alert("coming soon");
  }

  advanceOption(){
    // this.advanceOptionText = !this.advanceOptionText;
    this.advanceOptionField = !this.advanceOptionField;
  }

  getActionText(tradeAction){
    return tradeAction == 'BUY' ? 'INVEST' : 'SELL';
  }

}
