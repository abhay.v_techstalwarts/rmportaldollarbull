import { DashboardModule } from './../dashboard/dashboard.module';
import { InvestmentModule } from './../investment/investment.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../@shared/shared.module';
import { PortfolioRoutingModule } from './portfolio-routing.module';
import { PortfolioComponent } from './portfolio.component';
import { PortfolioDetailsComponent } from './portfolio-details/portfolio-details.component';
import {TransactionHistoryComponent } from './transaction-history/transaction-history.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [PortfolioComponent, PortfolioDetailsComponent, TransactionHistoryComponent],
  imports: [
    CommonModule,
    PortfolioRoutingModule,
    SharedModule,
    NgbModule,
    InvestmentModule,
    ReactiveFormsModule,
    DashboardModule
  ]
})
export class PortfolioModule { }
