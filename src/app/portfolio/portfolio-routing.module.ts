import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PortfolioComponent } from './portfolio.component';
import { PortfolioDetailsComponent } from './portfolio-details/portfolio-details.component';
import { TransactionHistoryComponent } from './transaction-history/transaction-history.component';

const routes: Routes = [
                          { path: '', component: PortfolioComponent },
                          { path: 'details/:id', component: PortfolioDetailsComponent },
                          { path: 'transaction-history/:id', component: TransactionHistoryComponent }
                        ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortfolioRoutingModule { }
