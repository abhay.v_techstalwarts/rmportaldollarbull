import { CommonService } from './../../@core/service/common/common.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from 'src/app/@core/service/account/account.service';
@Component({
  selector: 'app-investment-list',
  templateUrl: './investment-list.component.html',
  styleUrls: ['./investment-list.component.scss']
})
export class InvestmentListComponent implements OnInit {
  customerId :any;
  investmentData:any;
  constructor(private accountService:AccountService, private router: Router, private route: ActivatedRoute, private commonService: CommonService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe( params => {
      this.customerId = params.get('id');
  
    });
    this.getInvetmentList();
  }

  getInvetmentList(){
    let data={
      customer_id:  this.customerId,
      customer_tradestation_account_id : this.accountService.getSelectedAccount(),
    }

    this.commonService.get_Investment_List_By_Customer_ID(data).subscribe((res) => {
      // @ts-ignore
      this.investmentData = res.result.records;
      console.log(this.investmentData);
    });
  }

}
