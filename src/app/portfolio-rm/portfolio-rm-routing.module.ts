import { InvestmentListComponent } from './investment-list/investment-list.component';
import { PortfolioHomeComponent } from './portfolio-home/portfolio-home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: ':id', component: PortfolioHomeComponent},
  { path: 'investment/:id', component: InvestmentListComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortfolioRmRoutingModule { }
