import { CommonService } from 'src/app/@core/service/common/common.service';
import { AccountService } from 'src/app/@core/service/account/account.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-portfolio-home',
  templateUrl: './portfolio-home.component.html',
  styleUrls: ['./portfolio-home.component.scss']
})
export class PortfolioHomeComponent implements OnInit {
  customerId :any;
  customerName:any;
  customerEmail:any;
  investmentData=[];
  customer_current_value:any;
  customer_invested_value:any;
  profit_loss_value:any;
  profit_loss_per:any;
  account_balance:any;
  unrealised_pl:any;
  pieChartLabels = ['Stocks', 'ETFs'];
  pieChartValues = [0, 0,];
  // 
  monthsInPerformanceCharts: any = [
    { text: '1M', value: '1M', selected: true },
    { text: '3M', value: '3M' },
    { text: '6M', value: '6M' },
    { text: '1Y', value: '1Y' },
  ];
  dataSets = [
    {
      data: [],
      label: 'performance',
      lineTension: 0,
      fill: false,
      pointBorderWidth: 0,
      backgroundColor: '#3d76cd',
      pointRadius: 0,
      borderColor: '#3d76cd',
      pointStyle: 'rect',
    },
  ];
  dataLoaded = false;
  label = ['Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  performanceData = [];

  constructor(private router: Router, private route: ActivatedRoute, private commonService: CommonService, private accountService:AccountService) {
 
  }

  ngOnInit(): void {
    this.route.queryParams
    .subscribe(params => {
      this.customerName = params.name;
      this.customerEmail = params.email;
    }
  );

    this.route.paramMap.subscribe( params => {
      this.customerId = params.get('id');
    });

    setTimeout(() => {
      this.accountService.SelectedAccount().subscribe((selectedAccount:any)=>{
        this.getInvetmentList(selectedAccount);
      this.overView(selectedAccount);
  
      });
    }, 2000);

  }
  getInvetmentList(selectedAccount : string){
    let data={
      perPage:5,
      page:0,
      customer_id:  this.customerId,
      customer_tradestation_account_id : Number(selectedAccount)
    }

    this.commonService.get_Investment_List_By_Customer_ID(data).subscribe((res) => {
       // @ts-ignore
      this.investmentData = res.result.records;

      console.log(this.investmentData + "hellow")
    });
  }

  overView(selectedAccount : string){
    let data = {
      customer_tradestation_account_id : Number(selectedAccount),
      customer_id:  this.customerId,
    }
     this.commonService.get_Portfolio_Overview(data).subscribe((res) => {
      // @ts-ignore
      this.customer_invested_value = res.result.customer_invested_value;
       // @ts-ignore
      this.customer_current_value = res.result.customer_current_value;
       // @ts-ignore
      this.profit_loss_value = res.result.profit_loss_value;
       // @ts-ignore
      this.unrealised_pl = res.result.unrealized_pl; 
       // @ts-ignore
      console.log(['this.unrealised_pl', this.unrealised_pl]);
       // @ts-ignore 
      var pl_per = res.result.profit_loss_per;
      this.profit_loss_per = pl_per + "%";
    });

    this.commonService.getPerformance(
      {
        customer_id: this.customerId,
        type: "1M"
      }, true
    ).subscribe(res => {
      this.performanceData = res
      this.label = this.performanceData['label']
      this.dataSets[0]['data'] = this.performanceData['data']
      this.dataLoaded = true
    })

    this.commonService.getCustomerInvestmentChart(
      {
        customer_id : this.customerId
      }, true
    ).subscribe(res => {
      this.pieChartLabels = res.labels;
      this.pieChartValues = res.data;
      console.log({chart :res})

    })

  }

  
  handleMonthPerformance(month: any) {
    this.commonService.getPerformance(
      {
        customer_id: this.customerId,
        type: month
      }, true
    ).subscribe(res => {
      this.performanceData = res
      this.label = this.performanceData['label']
      this.dataSets[0]['data'] = this.performanceData['data']
      this.dataLoaded = true

    })
  }

  navigateToAllInvestmentList(){
    this.router.navigate(["investment/"+this.customerId]);
  }

}
