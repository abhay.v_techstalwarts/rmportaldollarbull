import { SharedModule } from './../@shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PortfolioRmRoutingModule } from './portfolio-rm-routing.module';
import { InvestmentListComponent } from './investment-list/investment-list.component';
import { PortfolioHomeComponent } from './portfolio-home/portfolio-home.component';


@NgModule({
  declarations: [InvestmentListComponent, PortfolioHomeComponent],
  imports: [
    CommonModule,
    PortfolioRmRoutingModule,
    SharedModule
  ],
  exports: [InvestmentListComponent, PortfolioHomeComponent]
})
export class PortfolioRmModule { }
