import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../@shared/shared.module';
import { InvestmentRoutingModule } from './investment-routing.module';
import { InvestmentComponent } from './investment.component';
import { InvestmentInfoCardComponent } from './components/investment-info-card/investment-info-card.component';


@NgModule({
  declarations: [InvestmentComponent, InvestmentInfoCardComponent],
  imports: [
    CommonModule,
    InvestmentRoutingModule,
    SharedModule
  ],
  exports: [InvestmentInfoCardComponent,InvestmentComponent]
})
export class InvestmentModule { }
