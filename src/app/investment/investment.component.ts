import { Component, OnInit } from '@angular/core';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';

@Component({
  selector: 'app-investment',
  templateUrl: './investment.component.html',
  styleUrls: ['./investment.component.scss']
})
export class InvestmentComponent implements OnInit {

  myInvestementData = [];
  constructor(private stocksService: StocksService) { }

  ngOnInit(): void {
    this.get_my_investment();
  }

  get_my_investment(){
    var data = {
      "perPage" : 20,
      "page" : 1
    }
    this.stocksService.getOrderList(data).subscribe((res) => {
      this.myInvestementData = res.records;
    });
  }

}
