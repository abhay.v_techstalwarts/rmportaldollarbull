import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-investment-info-card',
  templateUrl: './investment-info-card.component.html',
  styleUrls: ['./investment-info-card.component.scss']
})
export class InvestmentInfoCardComponent implements OnInit {

  @Input() quantity:string="";
  @Input() company_name:string="";
  @Input() current_price:string="";
  @Input() buy_price:string="";
  @Input() companyId:string="";
  @Input() redirectUrl:string="";
  @Input() symbol:string="";
  absolute_val: number=0;
  percentage_val: number=0;
  current_price_value: number=0;
  buy_price_value: number=0;
  stats: string = "";
  is_positive : number = 0

  constructor() { 

  }

  ngOnInit(): void {
    this.current_price_value = parseFloat(this.current_price);
    this.buy_price_value = parseFloat(this.buy_price);
    this.absolute_val = this.current_price_value - this.buy_price_value;
    if (this.absolute_val > 0) {
      this.is_positive = 1;
    }else if (this.absolute_val < 0) {
      this.is_positive = 2;
    }else{
      this.is_positive = 0;
    }
    if (this.buy_price_value > 0) {
      this.percentage_val = (this.current_price_value - this.buy_price_value)/ this.buy_price_value*100;
    }
    this.stats = "$" + this.absolute_val.toFixed(2) + "(" + this.percentage_val.toFixed(2) + "%)";
  }
}
 