import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './@core/guard/auth.guard';
import { NoAuthGuard } from './@core/guard/no-auth.guard';
import { SearchMobileComponent } from './search-mobile/search-mobile.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'splash',
    pathMatch: 'full',
  },
  
  {
    path: 'search',
    component: SearchMobileComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'resepassword',
    component: ResetpasswordComponent,
  },
  {
    path: 'changepassword',
    component: ChangepasswordComponent,
  },
  
  {
    path: 'splash',
    loadChildren: () =>
      import('./splash/splash.module').then((m) => m.SplashModule),
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
    canActivate: [NoAuthGuard],
  },
  // {
  //   path: 'notification',
  //   loadChildren: () =>
  //     import('./notification/notification.module').then((m) => m.NotificationModule),
  //   canActivate: [AuthGuard],
  // },
  {
    path: 'watchlist',
    loadChildren: () =>
      import('./watchlist/watchlist.module').then((m) => m.WatchlistModule),
    },
  {
    path: 'explore',
    loadChildren: () =>
      import('./explore/explore.module').then((m) => m.ExploreModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
    canActivate: [AuthGuard],
  },
  // {
  //   path: 'account',
  //   loadChildren: () =>
  //     import('./account/account.module').then((m) => m.AccountModule),
  //   canActivate: [AuthGuard],
  // },
  // {
  //   path: 'investment',
  //   loadChildren: () =>
  //     import('./investment/investment.module').then((m) => m.InvestmentModule),
  //   canActivate: [AuthGuard],
  // },

  {
    path: 'portfolio-rm',
    loadChildren: () =>
      import('./portfolio-rm/portfolio-rm.module').then((m) => m.PortfolioRmModule),
      canActivate: [AuthGuard],
  },
  
  // {
  //   path: 'signup',
  //   loadChildren: () =>
  //     import('./sign-up/signup/signup.module').then((m) => m.SignupModule),
  //     canActivate: [NoAuthGuard],
  //   },
  // {
  //   path: 'kyc',
  //   loadChildren: () => import('./kyc/kyc.module').then((m) => m.KycModule),
  //   canActivate: [AuthGuard],
  // },
  // {
  //   path: 'funding',
  //   loadChildren: () => import('./funding/funding.module').then((m) => m.FundingModule),
  // },
  {
    path: 'learn-to-trade',
    loadChildren: () => import('./learn-to-trade/learn-to-trade.module').then((m) => m.LearnToTradeModule),
  },
  {
    path: 'portfolio',
    loadChildren: () =>
      import('./portfolio/portfolio.module').then((m) => m.PortfolioModule),
      canActivate: [AuthGuard],
    },
    
  {
    path: 'faq',
    loadChildren: () =>
      import('./faq-support/faq-support.module').then((m) => m.FaqSupportModule),
      canActivate: [AuthGuard],
  },

  {
    path: 'login',
    loadChildren: () =>
      import('./login/login.module').then((m) => m.LoginModule),
    canActivate: [NoAuthGuard],

  },
  { path: 'watchlist', loadChildren: () => import('./watchlist/watchlist.module').then(m => m.WatchlistModule) },
  // {
  //   path: 'refer',
  //   loadChildren: () =>
  //     import('./refer/refer.module').then((m) => m.ReferModule),
  //     canActivate: [AuthGuard],
  //   },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
