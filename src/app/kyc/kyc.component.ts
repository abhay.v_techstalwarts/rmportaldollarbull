import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-kyc',
  templateUrl: './kyc.component.html',
  providers: [NgbModalConfig, NgbModal]
})
export class KycComponent implements OnInit {
  videoURL:any;
  constructor(private router: Router,config: NgbModalConfig, private modalService: NgbModal, private sanitizer: DomSanitizer) {
    config.backdrop = 'static';
    config.keyboard = false;
    this.videoURL = sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/bJHr6_skXWc');
   }

  ngOnInit(): void {
  }

  navigateToAppoitment(){
    this.router.navigate(['kyc/appointment']);
  }
  open(offermodalonfunds) {
    this.modalService.open(offermodalonfunds);
  }

}
