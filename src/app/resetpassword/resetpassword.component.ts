import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { CommonService } from '../@core/service/common/common.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/@core/service/auth/auth.service';
import { NavigationService } from 'src/app/@core/service/navigation.service';
@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {
  password:number;
    hide: boolean = true;
  confirmPassword:number;
  errorConfirmPassword:boolean;
  errorPassword:boolean;
  resetButtonDisabled:boolean=true;
  resetPasswordForm:any;
  reset_code:any;
  
  constructor(private navigation: NavigationService,private authService: AuthService,private activatedRoute: ActivatedRoute , private toastr: ToastrService, private commonService: CommonService,private fb: FormBuilder, ) { }

  ngOnInit(): void {

    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.reset_code = params.reset_code;
    });

    
    this.resetPasswordForm = this.fb.group({
      password: ['', [Validators.required ,Validators.min(6)]],
      confirmpassword: ['', [Validators.required ,Validators.min(6)]],
    });
  }

    myFunction() {
    this.hide = !this.hide;
  }

  passwordPress(event:any) {
    this.password = event.target.value;
    console.log(this.password);
  }
  confirmPasswordPress(event:any){
    this.confirmPassword = event.target.value;
    if( this.confirmPassword != this.password){
      this.resetButtonDisabled = true;
      this.errorConfirmPassword = true;
    }
    else{
      this.resetButtonDisabled = false;
      this.errorConfirmPassword = false;
    }
  }

  submitResetPassword(){    
  //  let email_id = localStorage.getItem('email_id');
    let data = {
      reset_code : this.reset_code,
      password : this.resetPasswordForm.get('confirmpassword').value,
    }
    this.commonService.resetPassword(data).subscribe((res:any) => {
      if(res.status == '1'){
      this.toastr.success(res.message, "", { timeOut: 3000, disableTimeOut: false, });
      localStorage.clear();
        this.authService.logout().subscribe((res) => {
          this.navigation.stackFirst(['/splash']);
        });
      }
    },
    (err) => {
      this.toastr.error(err.error.message, "", { timeOut: 3000, disableTimeOut: false, });
    })
  }



}
