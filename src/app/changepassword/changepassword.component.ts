import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { CommonService } from '../@core/service/common/common.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/@core/service/auth/auth.service';
import { NavigationService } from 'src/app/@core/service/navigation.service';
import { JwtService } from '../@core/service/jwt.service';
@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent implements OnInit {
  password:number;
  confirmPassword:number;
  errorConfirmPassword:boolean;
  errorPassword:boolean;
  resetButtonDisabled:boolean=true;
  resetPasswordForm:any;
  reset_code:any;
  constructor(private jwtService: JwtService, private navigation: NavigationService,private authService: AuthService ,private activatedRoute: ActivatedRoute , private toastr: ToastrService, private commonService: CommonService,private fb: FormBuilder, ) { }

  ngOnInit(): void {
    
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.reset_code = params.reset_code;
    });

    
    this.resetPasswordForm = this.fb.group({
      password: ['', [Validators.required ,Validators.min(6)]],
      confirmpassword: ['', [Validators.required ,Validators.min(6)]],
    });

  }

  passwordPress(event:any) {
    this.password = event.target.value;
  }

  
  submitResetPassword(){    
    let email_id = this.jwtService.getRM_Email_ID();
     let data = {
       password : this.resetPasswordForm.get('password').value,
       email_id : email_id,
     }
     this.commonService.changePassword(data).subscribe((res:any) => {
       //@ts-ignore
       if(res.status == '1'){
        this.toastr.success(res.message, "", { timeOut: 3000, disableTimeOut: false, });
        localStorage.clear();
        this.authService.logout().subscribe((res) => {
          this.navigation.stackFirst(['/splash']);
        });
        }



     },
     (err) => {
       this.toastr.error(err.error.message, "", { timeOut: 3000, disableTimeOut: false, });
     })
   }
}
