import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Plugins } from "@capacitor/core";
const { Browser } = Plugins;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  browserInstance: any;
  
  constructor() {
    Browser.addListener('browserFinished', (info:any) => {
      console.log('finished', {info});
    })

    Browser.addListener('browserPageLoaded', (info:any) => {
      console.log('pageLoaded', {info});
    })

    Browser.prefetch({urls: ['https://www.google.co.in']})
  }

  async ngOnInit(): Promise<void> {
    this.browserInstance = await Browser.open({
      url: 'https://www.google.co.in'
    })
  }
}
