import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

// shared components
import { LoaderComponent } from './component/loader/loader.component';
import { ExploreLayoutComponent } from './component/explore-layout/explore-layout.component';
import { SearchFieldComponent } from './component/search-field/search-field.component';
import { NavTabsComponent } from './component/nav-tabs/nav-tabs.component';
import { CardsListComponent } from './component/cards-list/cards-list.component';
import { InfoCardComponent } from './component/info-card/info-card.component';
import { BaseLayoutComponent } from './component/base-layout/base-layout.component';
import { CardPriceComponent } from './component/card-price/card-price.component';
import { CardStatsComponent } from './component/card-stats/card-stats.component';
import { NewsCardComponent } from './component/news-card/news-card.component';
import { NewsCardVerticalComponent } from './component/news-card-vertical/news-card-vertical.component';
import { ToggleComponent } from './component/toggle/toggle/toggle.component';
import { LineChartComponent } from './component/line-chart/line-chart.component';
import { PieChartComponent } from './component/pie-chart/pie-chart.component';
import { InfoCardAllStockComponent } from './component/info-card-all-stock/info-card-all-stock.component';
import { BecomeDollarbullCardComponent } from './component/become-dollarbull-card/become-dollarbull-card.component';
import { MarketOpenCardComponent } from './component/market-open-card/market-open-card.component';
import { MyInvestmentCardComponent } from './component/my-investment-card/my-investment-card.component';
// shared pipe
import { BaseTitlePipe } from './pipe/base-title.pipe';
import { SignCurrencyPipe } from './pipe/sign-currency.pipe';
import { MoneyFormatPipe } from "./pipe/money-format.pipe";
import { PortfolioChartComponent } from './component/portfolio-chart/portfolio-chart.component';
import { NeedHelpCardComponent } from './component/need-help-card/need-help-card.component';
import { CountdownModule } from 'ngx-countdown';
import { UserInfoDropdownComponent } from './component/user-info-dropdown/user-info-dropdown.component';
import { ClientListCardComponent } from './component/client-list-card/client-list-card.component';
import { AreaChartComponent } from './component/area-chart/area-chart.component';
import { NgApexchartsModule } from 'ng-apexcharts';


@NgModule({
  declarations: [
    LoaderComponent,
    ExploreLayoutComponent,
    SearchFieldComponent,
    NavTabsComponent,
    CardsListComponent,
    InfoCardComponent,
    BaseLayoutComponent,
    CardPriceComponent,
    CardStatsComponent,
    BaseTitlePipe,
    NewsCardComponent,
    ToggleComponent,
    LineChartComponent,
    PieChartComponent,
    InfoCardAllStockComponent,
    NewsCardVerticalComponent,
    BecomeDollarbullCardComponent,
    MarketOpenCardComponent,
    MyInvestmentCardComponent,
    SignCurrencyPipe,
    MoneyFormatPipe,
    PortfolioChartComponent,
    NeedHelpCardComponent,
    UserInfoDropdownComponent,
    ClientListCardComponent,
    AreaChartComponent,
  ],
  imports: [CommonModule, RouterModule, ChartsModule, CountdownModule, NgbModule, NgApexchartsModule],
  exports: [
    LoaderComponent,
    ExploreLayoutComponent,
    SearchFieldComponent,
    NavTabsComponent,
    CardsListComponent,
    InfoCardComponent,
    BaseLayoutComponent,
    CardPriceComponent,
    CardStatsComponent,
    BaseTitlePipe,
    NewsCardComponent,
    ToggleComponent,
    LineChartComponent,
    PieChartComponent,
    InfoCardAllStockComponent,
    NewsCardVerticalComponent,
    BecomeDollarbullCardComponent,
    MarketOpenCardComponent,
    MyInvestmentCardComponent,
    SignCurrencyPipe,
    MoneyFormatPipe,
    PortfolioChartComponent,
    NeedHelpCardComponent,
    UserInfoDropdownComponent,
    ClientListCardComponent,
    AreaChartComponent,
  ],
})
export class SharedModule {}
