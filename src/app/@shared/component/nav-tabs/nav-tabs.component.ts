import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges ,ViewChild, ElementRef  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nav-tabs',
  templateUrl: './nav-tabs.component.html',
})
export class NavTabsComponent implements OnInit {
  @Input()
  tabs: TabsI[] = [];
  @ViewChild("navtoscroll") navtoscroll : ElementRef;
  isScroll = false;
  scrollRight = true;
  scrollLeft = false ;

  @Input()
  activeClass: string = 'active';

  @Output()
  onClick: EventEmitter<any> = new EventEmitter();

  activeIndex: number = 0;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    //console.log(['activeLink', activeLink]);
    // if (this.tabs.length > 0) {
    //   this.activeIndex = 0;
    // }
    const activeLink = this.router.url;
    this.activeIndex = this.tabs.findIndex((item) => item.link && item.link === activeLink);
    if (this.activeIndex == -1) {
      this.activeIndex = 0;
    }

    const scrollContainer = document.getElementById("navtoscroll");
    scrollContainer.addEventListener("wheel", (evt:any) => {
      scrollContainer.scrollLeft += evt.deltaY;
      evt.preventDefault();
      });
  
      // Whatch list Scroll Buttons 
      window.addEventListener("resize", (evt:any) => {
        this.watchScroll();
        evt.preventDefault();
      });
      this.watchScroll();
    
  }

  

  ngOnChanges(changes: SimpleChanges) {
    if(changes && changes.tabs && changes.tabs.currentValue && changes.tabs.previousValue){
      if (changes.tabs.previousValue.length == 0) {
        this.activeIndex = 0;
      }
      else if (changes.tabs.currentValue.length > changes.tabs.previousValue.length) {
        this.activeIndex = changes.tabs.currentValue.length-1;
      }else if(changes.tabs.currentValue.length < changes.tabs.previousValue.length){
        this.activeIndex = 0;
      }
    }
    this.watchScroll();
  }


  

  handleClick(e: any, item: TabsI, index: number) {
    e.preventDefault();
    this.watchScroll();
    //console.log(["item",item]);
    
    this.activeIndex = index;

    this.onClick.emit({item, index});

    if (item.preventDefault) {
      return;
    }
    
    if (item.link) {
    this.router.navigate([item.link], { replaceUrl: true });
    }
  }
  // TODO: logic to keep active tab within viewport on click

  watchScroll(pos = null){

    let time = (pos == null) ? 1000 : 0;
    setTimeout(()=>{
      let target = this.navtoscroll.nativeElement;
      let clientWidth = target.clientWidth;
      let scrollWidth = target.scrollWidth;
      let scrollCurr = target.scrollLeft;
      let scrollBy = 50;
     
      if(scrollWidth == clientWidth){
        this.isScroll = false; return;
      }else{
        this.isScroll = true;
      }

      if(pos == 'R'){
        scrollBy =  scrollCurr + scrollBy;
      }else if(pos == 'L'){
        scrollBy =  scrollCurr - scrollBy;
      }else{
        return;
      }

      if(scrollBy < 0 ){
        target.scrollLeft = 0;
        this.scrollLeft = false ;
      }else if(scrollCurr >= (scrollWidth - clientWidth) && pos == 'R'){
        target.scrollLeft = scrollWidth;
        this.scrollRight = false;
      }else if(scrollCurr <= (scrollWidth - clientWidth)){
        target.scrollLeft = scrollBy;
        this.scrollRight = true;
        this.scrollLeft = true ;
        
      }


    },time);

  } //watchScroll Ends

}

export interface TabsI {
  link?: string;
  label: string;
  icon?:string;
  hasInvested?:boolean;
  current_stage?:number;
  preventDefault?: boolean;
  id?:string;
}
