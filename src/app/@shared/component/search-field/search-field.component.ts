import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search-field',
  templateUrl: './search-field.component.html',
})
export class SearchFieldComponent implements OnInit {
  @Input()
  placeholder: string = 'Search here';

  @Input()
  searchInput: string = '';

  @Output()
  onBlur: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onSearchChange: EventEmitter<any> = new EventEmitter<any>();

  debounceTimer: any

  constructor() { }

  ngOnInit(): void { }

  handleSearchChange($event: any) {
    //CLEAR TIMEOUT
    if (this.debounceTimer) window.clearTimeout(this.debounceTimer);

    this.debounceTimer = window.setTimeout(() => {
      //EMIT EVENT
      this.onSearchChange.emit($event)
    }, 1000)
  }
}
