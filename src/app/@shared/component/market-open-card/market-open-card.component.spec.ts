import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketOpenCardComponent } from './market-open-card.component';

describe('MarketOpenCardComponent', () => {
  let component: MarketOpenCardComponent;
  let fixture: ComponentFixture<MarketOpenCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarketOpenCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketOpenCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
