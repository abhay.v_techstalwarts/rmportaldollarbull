import { Component, OnInit, ViewChild } from '@angular/core';
import { CountdownComponent, CountdownEvent } from 'ngx-countdown';
import {CommonService} from '../../../@core/service/common/common.service'

@Component({
  selector: 'app-market-open-card',
  templateUrl: './market-open-card.component.html',
})
export class MarketOpenCardComponent implements OnInit {
  @ViewChild('cd', { static: false }) private countdown: CountdownComponent;
  date_config:any;
  showTimer:boolean=false;
  txtMarketOpen:string="Market opens in";
  constructor(private commonService:CommonService) { }

  ngOnInit(): void {
    this.configureTradingTimer();
  }

  configureTradingTimer(){
    this.commonService.getMarketTime().subscribe((res)=>{
      let response = res.result;
      if (response.is_market_open) {  //response.is_market_open
        this.showTimer= false;
        this.txtMarketOpen = "Market is open"
        this.date_config= {leftTime: -1, demand: true};
      }else{
        this.showTimer= true;
        this.txtMarketOpen="Market opens in";
        let market_open_string = response.market_opens_in + " UTC";//response.market_opens_in + " UTC";
        let market_open_date : any = new Date(market_open_string);
        let current_date:any = new Date();
        let diff = (market_open_date - current_date)/1000;
        diff = Math.abs(diff);
        console.log(['Math.round(diff)', market_open_date, current_date, Math.round(diff)]);
        this.date_config= {leftTime: Math.round(diff), demand: true}
        setTimeout(() => {
          this.countdown.begin();
        });
      }
      
    });


    // let current_date:any = new Date();
    // let tmp_string = (current_date.getMonth() + 1) + "/" + current_date.getDate() + "/" + current_date.getFullYear() + " 14:30:00 UTC"
    // console.log(['tmp_string', tmp_string]);
    
    // let market_open_date : any = new Date(tmp_string);
    // let d = market_open_date - current_date;
    // if (d < 0) {
    //   tmp_string = (current_date.getMonth() + 1) + "/" + (current_date.getDate() + 1) + "/" + current_date.getFullYear() + " 14:30:00 UTC"
    //   market_open_date = new Date(tmp_string)
    // }
    // let diff = (market_open_date - current_date)/1000;
    // diff = Math.abs(diff);
    // console.log(['Math.round(diff)', market_open_date, current_date, Math.round(diff)]);
    
    // this.date_config= {leftTime: Math.round(diff), demand: true}
  }

  handleEvent(event: any) {
    if (event.action == 'done') {
      this.txtMarketOpen = "Market is open"
      this.showTimer= false;
    }
  }

}
