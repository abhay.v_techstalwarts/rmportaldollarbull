import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientListCardComponent } from './client-list-card.component';

describe('ClientListCardComponent', () => {
  let component: ClientListCardComponent;
  let fixture: ComponentFixture<ClientListCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientListCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientListCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
