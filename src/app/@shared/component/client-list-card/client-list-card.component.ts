import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-client-list-card',
  templateUrl: './client-list-card.component.html',
  styleUrls: ['./client-list-card.component.scss']
})
export class ClientListCardComponent implements OnInit {
  @Input()
  name :string = "Amazon com Inc" ;
  @Input()
  email_id :string = "Amazon com Inc" ;

  @Input() 
  viewPortfolioAllow :boolean;

  @Input()
   customerId :any;
   @Input()
   date :any;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  handleNavigatePortfolio(customerId , name, email_id){
    this.router.navigate(['portfolio-rm/'+customerId], {queryParams: {name : name , email: email_id}});
}

}
