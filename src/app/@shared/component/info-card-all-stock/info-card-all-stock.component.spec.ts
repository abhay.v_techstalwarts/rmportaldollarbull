import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoCardAllStockComponent } from './info-card-all-stock.component';

describe('InfoCardAllStockComponent', () => {
  let component: InfoCardAllStockComponent;
  let fixture: ComponentFixture<InfoCardAllStockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoCardAllStockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoCardAllStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
