import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-info-card-all-stock',
  templateUrl: './info-card-all-stock.component.html',
  styleUrls: ['./info-card-all-stock.component.scss']
})
export class InfoCardAllStockComponent implements OnInit {
  isPositive: number = 0
  @Input()
  name: string = "Amazon com Inc";
  @Input()
  logo: string = "assets/icons/amazon-65-675861.webp";
  @Input()
  capType: string = "Mid Cap"
  @Input()
  symbol: string = "APPL";
  @Input()
  price: any = "$214.25";
  @Input()
  dailyChange: any
  @Input()
  dailyChangeStatusImage: string = "assets/icons/triangle.svg";
  @Input()
  marketCap: string;
  @Input()
  ratio: string = "126.5T";
  @Input()
  companyId = 1;
  @Input()
  sector: string = "IT";
  @Input() previous_price: any = 0;

  @Input()
  stats: any;

  market_cap_value:number;

  @Input()
  isEtfs :boolean  = false;

  constructor(private router: Router,) { }

  ngOnInit(): void {
    this.market_cap_value = Number(this.marketCap);
    if (this.price) {
      // console.log(["this.price", this.price, "this.previous_price", this.previous_price]);

      let price_diff = this.price - this.previous_price;
      if (price_diff > 0) {
        this.isPositive = 1
      } else if (price_diff < 0) {
        this.isPositive = 2
      } else {
        this.isPositive = 0
      }

      let percentage_val = 0;
      if (this.previous_price > 0) {
        percentage_val = (this.price - this.previous_price) / this.previous_price * 100;
      }
      this.stats = "$" + price_diff.toFixed(2) + "(" + percentage_val.toFixed(2) + "%)";
    }
  }
  
  handleClick(){
    this.router.navigate([`explore/stocks/${this.companyId}`]);
  }
}



