import { Component, Input, OnInit, Output, EventEmitter, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartTooltipOptions } from "chart.js";
import { Color, Label } from "ng2-charts";
import { createChart, CrosshairMode } from 'lightweight-charts';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit, AfterViewInit {

  @Output() handleMonthPerformance: EventEmitter<any> = new EventEmitter();

  @Input()
  label: Array<any> = []
  private _data;

  // @Input() dataSets: any;
  // use getter setter to define the property
  get dataSets(): any { 
    return this._data;
  }
  
  @Input()
  set dataSets(val: any) {
    this._data = val;
    this.initializeGraph();
  }

  @Input()
  months: Array<any> = []



  dataLoaded: boolean = false
  monthsInCharts: any = []

  @ViewChild('mainScreen') elementView: ElementRef;
  chart: any;
  selectedMonth:string="1M";
  public isClicked = [];
  constructor() { }

  ngOnInit(): void {
    console.log(['dataSets',this.dataSets]);
    this.isClicked[1]=true;
    this.monthsInCharts= this.months;

    console.log(this.monthsInCharts);
  }

  initializeGraph(){
    var element = document.getElementById("chart") as HTMLInputElement;
    
    element.innerHTML='';
    let width = element.offsetWidth;
    let height = element.offsetHeight ==0 ? 270 : element.offsetHeight;
    this.chart = createChart(element, {
      width: width,
      height: height,
      rightPriceScale: {
        scaleMargins: {
          top: 0.3,
          bottom: 0.25,
        },
        borderVisible: false,
      },
      layout: {
        backgroundColor: '#fff',
        textColor: '#212529',
      },
      grid: {
        vertLines: {
          color: 'rgba(230,230,230,1)',
        },
        horzLines: {
          color: 'rgba(230,230,230,1)',
        },
      },
      
      localization: {
        dateFormat: 'yyyy/MM/dd',
    }
    });

    var areaSeries = this.chart.addAreaSeries({
      topColor: 'rgba(25, 107, 229, 0.3)',
      bottomColor: 'rgba(25, 107, 229, 0.04)',
      lineColor: 'rgba(25, 107, 229, 1)',
      lineWidth: 2
    });
    
    var volumeSeries = this.chart.addHistogramSeries({
      color: '#196be5',
      priceFormat: {
        type: 'volume',
      },
      priceScaleId: '',
      scaleMargins: {
        top: 0.8,
        bottom: 0,
      },
    });

    areaSeries.setData(this.dataSets.prices);
    volumeSeries.setData(this.dataSets.volumes);

    areaSeries.applyOptions({
      priceFormat: {
          type: 'custom',
          formatter: price => '$ ' + price.toFixed(2),
      },
  });

    if (this.selectedMonth.toLowerCase() == '1m' || this.selectedMonth.toLowerCase() == '1w' ) {
      this.chart.timeScale().fitContent();
    }
    
  }

  ngAfterViewInit() {
    this.chart.resize(this.elementView.nativeElement.offsetWidth, this.elementView.nativeElement.offsetHeight);
  }

  handleParentMonth(month:any, index:any){   
    this.isClicked =[]
    this.isClicked[index] = true;    
    this.selectedMonth = month;
    this.handleMonthPerformance.emit(month);
  }

}

