import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-stats',
  templateUrl: './card-stats.component.html',
  styleUrls: ['./card-stats.component.scss']
})
export class CardStatsComponent implements OnInit {
  @Input() stats: any = "0"
  @Input() is_positive: number = 0;


  img : string = "assets/icons/triangle.svg" 
  constructor() { }
  ngOnInit(): void {
    if (this.is_positive == 1) {
      this.img = "assets/icons/triangle.svg";
    }
    else if(this.is_positive == 2) {
      this.img = "assets/icons/triangle_down.svg"
    }
  }

}
