import { TabsI } from './../nav-tabs/nav-tabs.component';
import { Component, Input, OnInit, Output, EventEmitter, TemplateRef } from '@angular/core';
import { NavigationService } from 'src/app/@core/service/navigation.service';
import { CommonService } from 'src/app/@core/service/common/common.service';
import { AuthService } from 'src/app/@core/service/auth/auth.service';
import { NotificationService } from "src/app/@core/service/notification/notification.service";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-base-layout',
  templateUrl: './base-layout.component.html',
})
export class BaseLayoutComponent implements OnInit {
  hasInvested:boolean= false;
  current_stage:number=0;

  @Input()
  enableMobileMenu: boolean = false;

  @Input()
  mobileHeaderDashBoard :boolean=false;

  @Input()
  enableMobileHeader: boolean = false;
  
  @Input()
  enableDeskTopHeader: boolean = false;

  @Input()
  enableDeskTopHeaderAction: boolean = false;

  @Input()
  title: string = '';

  @Input()
  disableDefaultBack: boolean = false;

  @Output()
  onBackClick: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  // @ts-ignore
  headerActionTemplate: TemplateRef<any>;
  newNotification = false;


  desktopNavTabs: TabsI[] = [
    {
      link: '/dashboard',
      label: 'Dashboard',
      hasInvested:this.hasInvested,
    },
    {
      link: '/portfolio',
      label: 'Portfolio',
      hasInvested:this.hasInvested,

    },
    {
      link: '/kyc',
      label: 'KYC',
      hasInvested:this.hasInvested,

    },
    {
      link: '/funding',
      label: 'Funds',
      hasInvested:this.hasInvested,

    },
    {
      link: '/learn-to-trade',
      label: 'Invest',
      hasInvested:this.hasInvested,
    },
    {
      link: '/refer',
      label: 'Refer',

    },
    {
      link: '/explore/stocks',
      label: 'Browse',
      hasInvested:this.hasInvested,

    },
    {
      link: '/account',
      label: 'More',
    },
  ];

  loaderEnabled: any = false;

  constructor(private route: ActivatedRoute ,private navigation: NavigationService, private commonService: CommonService, private authService: AuthService,
    private notificationService:NotificationService,
    ) {}

  ngOnInit(): void {
    // let data = this.route.snapshot.paramMap;
    // letdata = this.route.url;
    // console.log(data);
    this.current_stage = this.commonService.getCurrentStage();
    this.commonService.isLoaderEnabled$.subscribe(loaderEnabled => {
      this.loaderEnabled = loaderEnabled
    });
    let tmp =this.authService.getHasInvested();
    if (tmp) {
      this.hasInvested = true;
    }
    else{
      this.hasInvested = false;
    }
    this.notificationService.getNewNotification().subscribe((isNewNoti)=>{ 
      this.newNotification =isNewNoti;
    });
  }

  handleLogout() {
    localStorage.clear();
    this.authService.logout().subscribe((res) => {
      this.navigation.stackFirst(['/splash']);
    });
  }

  getCustomerLatestStage() {
    this.commonService.getCustomerCurrentStage().subscribe((current_stage) => {
      this.current_stage = current_stage
    },
      (err) => {
        console.log(err)
      }
    );
  }

  handleBackClick(e: any) {
    if (this.onBackClick) {
      this.onBackClick.emit(e);
    }

    if (this.disableDefaultBack) {
      return;
    }
    this.navigation.back();
  }
}
