import { Component, OnInit, ViewChild , EventEmitter, Input , Output} from '@angular/core';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexStroke,
  ApexYAxis,
  ApexTitleSubtitle,
  ApexLegend,
  ApexFill
} from "ng-apexcharts";
export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  dataLabels: ApexDataLabels;
  yaxis: ApexYAxis;
  title: ApexTitleSubtitle;
  labels: string[];
  legend: ApexLegend;
  subtitle: ApexTitleSubtitle;
  fill: ApexFill;
};
@Component({
  selector: 'app-area-chart',
  templateUrl: './area-chart.component.html',
  styleUrls: ['./area-chart.component.scss']
})
export class AreaChartComponent implements OnInit {
  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  private _data;
  monthsInCharts: any = []
  @Input() months: Array<any> = [];
  @Output() handleMonthPerformance: EventEmitter<any> = new EventEmitter();
  public isClicked = [];
  selectedMonth:string="6M";
  @Input() title: string = "Apple Price Movement";
  @Input() pointerName: string = "Apple";
  get dataSets(): any { 
    return this._data;
  }
  @Input()
  set dataSets(val: any) {
    this._data = val;
    this.plotGraph();
  }
  constructor() { }

  ngOnInit(): void {
    this.isClicked[3]=true;
    this.monthsInCharts= this.months;
    this.plotGraph();
  }
  handleParentMonth(month:any, index:any){   
    this.isClicked =[]
    this.isClicked[index] = true;    
    this.selectedMonth = month;
    this.handleMonthPerformance.emit(month);
  }
  plotGraph(){
    this.chartOptions = {
      series: [
        {
          name: this.pointerName,
          data: this.dataSets.prices
        }
      ],
      chart: {
        toolbar: {
        show: false
        },
        type: "area",
        height: 350,
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: "straight"
      },

      title: {
        text: "",
        align: "left"
      },
      labels: this.dataSets.dates,
      xaxis: {
        type: "datetime"
      },
      yaxis: {
        show: true,
        labels: {
          formatter: function (value) {
            return "$" + value.toFixed(2);
          }
        },
      },
      legend: {
        horizontalAlign: "left"
      },
      fill: {
        colors: ["#3B64BE"],
        type: "gradient",
        gradient: {
          shadeIntensity: 1,
          opacityFrom: 0.7,
          opacityTo: 0.9,
          stops: [0, 90, 100]
        }
      }
    };
  }

}
