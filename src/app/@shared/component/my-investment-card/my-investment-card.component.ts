import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-my-investment-card',
  templateUrl: './my-investment-card.component.html',
})
export class MyInvestmentCardComponent implements OnInit {
  @Input()
  customer_invested_value :string = "200" ;
  @Input()
  customer_current_value :string = "200" ;
  @Input()
  profit_loss_value :string = "200" ;
  @Input()
  profit_loss_per :string = "200" ;
  @Input()
  unrealised_pl :string = "200" ;

  @Input()
  account_balance :string = "" ;
  
  constructor() { }

  ngOnInit(): void {
  }

}
