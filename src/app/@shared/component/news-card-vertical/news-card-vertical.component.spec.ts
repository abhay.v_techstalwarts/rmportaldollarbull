import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsCardVerticalComponent } from './news-card-vertical.component';

describe('NewsCardVerticalComponent', () => {
  let component: NewsCardVerticalComponent;
  let fixture: ComponentFixture<NewsCardVerticalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsCardVerticalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsCardVerticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
