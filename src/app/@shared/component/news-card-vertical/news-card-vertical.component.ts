import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-news-card-vertical',
  templateUrl: './news-card-vertical.component.html'
})
export class NewsCardVerticalComponent implements OnInit {
  @Input()
  newsTitle: string ="Amazon’s latest grocery store concept opens, with high-tech carts";
  @Input()
  newsDate : string = "2 hours ago";

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  handleClick(){
    this.router.navigate(['account/article'])
  }
}
