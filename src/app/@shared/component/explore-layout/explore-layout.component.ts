import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { StocksService } from '../../../@core/service/stocks/stocks.service'
import { Router } from '@angular/router';
@Component({
  selector: 'app-explore-layout',
  templateUrl: './explore-layout.component.html',
})
export class ExploreLayoutComponent implements OnInit {
  @Input()
  type: 'stocks' | 'indices' | 'etfs' | '' = '';

  @Output()
  onSearchChange: EventEmitter<string> = new EventEmitter<string>();
  isMobileResolution: boolean;
  searchInput: string = '';
  searchBlock = false;
  searchedResults = []
  strings: any = {
    searchPlaceholder: 'Search Stocks',
  };

  stringsData: any = {
    stocks: {
      searchPlaceholder: 'Search Stocks',
    },
    indices: {
      searchPlaceholder: 'Search Indices',
    },
    etfs: {
      searchPlaceholder: 'Search ETFs',
    },
  };

  exploreTabs: any[] = [
    {
      link: "/explore/stocks",
      label: "Stocks",      
    },
    {
      link: "/explore/etfs",
      label: "ETFs",
    },
  ]

  searchCategoryTabs: any[] = [
    {
      link: "/explore/stocks",
      label: "Stocks"
    },
    {
      link: "/explore/stocks",
      label: "ETFs"
    },
  ]

  constructor(private stocksService: StocksService, private router: Router) { }

  ngOnInit(): void {
    this.strings = this.stringsData[this.type];
  }

  handleSearchChange(e: any) {
    this.searchInput = e.target.value;
    this.searchInput = this.searchInput.trim()

    if (this.searchInput && this.searchInput.length > 1) {
      this.searchBlock = true;
      let type= "";
      console.log(["this.type",this.type]);
      
      if (this.type == 'etfs') {
        type = "et";
      }
      else{
        type=""
      }
      this.stocksService.requestStockSearch(this.searchInput, type)
        .subscribe(res => {
          this.searchedResults = res.result
        })
    }
  }

  handleSearchClick(e:any) {
    if (window.innerWidth < 768) {
      this.isMobileResolution = true;
    } else {
      this.isMobileResolution = false;
    }
  }

  handleOnBlurSearch(event) {
    this.searchBlock = false;
    this.isMobileResolution = false;
  }

  handleStockNavigate(companyId: number){
    console.log(companyId)
    if(!companyId){ return; }

    this.router.navigate(['explore/stocks/'+companyId])
  }
  navigateToAllStocks() {
    
    if (this.type == "etfs") {
      this.router.navigate(['explore/stocks/all-etfs']);
      
    }else{
      this.router.navigate(['explore/stocks/all-stocks']);
    }
  }
}
