import { Component, OnInit, Input } from '@angular/core';
import { AccountService } from 'src/app/@core/service/account/account.service';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-user-info-dropdown',
  templateUrl: './user-info-dropdown.component.html',
  providers: [NgbDropdownConfig]
})
export class UserInfoDropdownComponent implements OnInit {
  @Input() customer_id :string = "" ;
  
  accounts=[];
  selectedAccount:any;
  constructor( 
    dropdownConfig:NgbDropdownConfig,
    private accountService:AccountService,
    ) {
    dropdownConfig.placement = 'bottom-right';
    dropdownConfig.autoClose = true;
  }
  ngOnInit(): void {
    console.log(['customer_id', this.customer_id]);
    this.accountService.getCustomerAccount(this.customer_id).subscribe((accounts)=>{
      this.accounts = accounts;
      console.log(this.accounts);
      this.accountService.SelectedAccount().subscribe((selectedAccount)=>{
        this.selectedAccount = accounts.find( account => account.customer_tradestation_account_id == selectedAccount)
      });
      if (this.selectedAccount == undefined || this.accounts.length>0) {
        this.accountService.setSelectedAccount(accounts[0].customer_tradestation_account_id);
      }
    })
  }

  onAccountClick(account){
    this.accountService.setSelectedAccount(account.customer_tradestation_account_id);
  }

}
