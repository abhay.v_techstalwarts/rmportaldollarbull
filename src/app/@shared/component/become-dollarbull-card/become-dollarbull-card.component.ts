import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { CommonService } from '../../../@core/service/common/common.service'
import { NgbModalConfig, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from "../../../@core/service/auth/auth.service";
import { environment } from 'src/environments/environment';
import { PlatformService } from 'src/app/@core/service/platform.service';


@Component({
  selector: 'app-become-dollarbull-card',
  templateUrl: './become-dollarbull-card.component.html',
  providers: [NgbModalConfig, NgbModal],

})
export class BecomeDollarbullCardComponent implements OnInit {
  @ViewChild("fundTransferModal") modalContent: TemplateRef<any>;
  @ViewChild("ComplteKycOptionsModal") kycOprionContent: TemplateRef<any>;
  @ViewChild("noteModal") noteContent: TemplateRef<any>;


  displayFundTransferCards: any[] = [
    {
      title: 'Remit funds online',
      info: 'You must fund your account from a bank account you own. You may not transfer funds from third party',
      iconPath: 'assets/icons/netbanking.svg',
      navigateTo: `netbanckingstep`,
    },
    {
      title: 'Remit funds through bank',
      info: 'You must fund your account from a bank account you own. You may not transfer funds from third party',
      iconPath: 'assets/icons/a2process.svg',
      navigateTo: `fundtransferbank`,
    },

  ];
  displayKycOptionCards: any[] = [
    {
      title: 'Go to KYC',
      info: 'Start KYC / complete pending KYC',
      iconPath: 'assets/icons/kyc-icon-modal.svg',
      navigateTo: `netbanckingstep`,
    },
    {
      title: 'Login to Tradestation',
      info: 'Logi here if your KYC is verified',
      iconPath: 'assets/icons/trade-station-icon-modal.png',
      navigateTo: `fundtransferbank`,
    },
  ]
  openKYCStage: string = "pending";
  addFunds: string = "pending";
  investMore: string = "pending";
  actionBtnLabel: string = "";
  // content = document.getElementById('content');
  actionStage: number = 0;
  //0-KYC, 1-Funding, 2-Invest
  platform = "";
  private _videoPlayer: any;
  private _url: string;
  private redirect_url: string = "";

  constructor(config: NgbModalConfig,
    private commonService: CommonService,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private router: Router, private modalService: NgbModal,
    private toastr: ToastrService,
    private platformService: PlatformService,
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    // console.log(['window.location', window.location.origin]);
    

  }

  ngOnInit(): void {
    this.platform = this.platformService.getPlatform();
    this.getCustomerLatestStage(false);
    // this.selectStage(0);
    this.selectStage(Number(this.commonService.getCurrentStage()));
  }

  
  getCustomerLatestStage(isForceCall =true) {
    this.commonService.getCustomerCurrentStage(isForceCall).subscribe((current_stage) => {
      this.selectStage(current_stage);
    },
      (err) => {
        console.log(err)
      }
    );
  }

  selectStage(stage: number) {
    this.actionStage = stage;
    switch (stage) {
      case 0:
        //submitKYC
        this.openKYCStage = "current";
        this.addFunds = "pending";
        this.investMore = "pending";
        if (this.router.url.includes('dashboard')) {
          this.actionBtnLabel = "Go to KYC";
        } else {
          this.actionBtnLabel = "Open KYC";
        }
        break;
      case 1:
        this.openKYCStage = "done";
        this.addFunds = "pending";
        this.investMore = "pending";
        if (this.router.url.includes('dashboard')) {
          this.actionBtnLabel = "Go to Funds";
        } else {
          this.actionBtnLabel = "Add Funds";
        }
        break;
      case 2:
      case 3:
        // addFunds
        this.openKYCStage = "done";
        this.addFunds = "done";
        this.investMore = "pending";
        if (this.router.url.includes('dashboard')) {
          this.actionBtnLabel = "Learn to Trade";
        } else {
          this.actionBtnLabel = "Invest";
        }
        break;
      default:
        break;
    }
  }

  actionBtnClick(currentStage: number) {
    this.actionOnClick(currentStage);
  }

  actionOnClick(stageNo: number) {
    if (this.router.url.includes('dashboard')) {
      switch (stageNo) {
        case 0:
          this.router.navigate(['kyc']);
          break;
        case 1:
          this.router.navigate(['funding']);
          break;
        case 2:
        case 3:
          this.router.navigate(['/learn-to-trade']);
          break;
      }
    }
    else {
      switch (stageNo) {
        case 0:
          this.modalService.open(this.kycOprionContent);
          break;
        case 1:
          this.modalService.open(this.noteContent, { centered: true });
          break;
        case 2:
        case 3:
          this.router.navigate(['/explore/stocks']);
          // this.playVideo();
          break;
      }
    }

  }

  navigateToFundus(val: any) {
    this.modalService.dismissAll();
    var urlsh = val;
    if (val != '') {
      this.router.navigate([`/dashboard/fundtransfer/${urlsh}`], { replaceUrl: true });
    }
  }

  openLRSPopup() {
    this.modalService.dismissAll();
    this.router.navigate(['/dashboard/fundtransfer']);
    // this.modalService.open(this.modalContent);
  }

  getParameterByName(name, url) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }


}
