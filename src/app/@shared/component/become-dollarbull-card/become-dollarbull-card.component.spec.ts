import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BecomeDollarbullCardComponent } from './become-dollarbull-card.component';

describe('BecomeDollarbullCardComponent', () => {
  let component: BecomeDollarbullCardComponent;
  let fixture: ComponentFixture<BecomeDollarbullCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BecomeDollarbullCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BecomeDollarbullCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
