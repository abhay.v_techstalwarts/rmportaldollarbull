import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html'
})
export class NewsCardComponent implements OnInit {
  @Input()
  newsTitle: string ="Amazon’s latest grocery store concept opens, with high-tech carts";
  @Input()
  newsDate : string = "2 hours ago";
  @Input()
  newsUrl:string="";
  Browser:any; 
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  handleClick(url){
    window.open(url, '_blank');
    // this.Browser.open({ url: });
    // this.router.navigate(['account/news-articles/1'])
  }
}
