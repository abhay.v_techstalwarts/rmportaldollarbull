import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
})
export class InfoCardComponent implements OnInit {
  isPositive: number = 0
  @Input()
  name :string = "Amazon com Inc" ;

  @Input() symbol :string = "TSLA" ;

  @Input()
  img :string = "https://storage.googleapis.com/iexcloud-hl37opg/api/logos/IEI.png";

  @Input()
  price :any;

  @Input() companyId :any = 1;

  @Input()
  company :string = "" ;

  @Input()
  stats :any;

  @Input()
  logo :any;

  @Input() sector :string = "" ;
  @Input() market_cap_type :string = "" ;

  @Input() previous_price : any = 0;

  @Input()
  longPressState :boolean ;

  @Input()
  isSelected :boolean ;

  @Input()
  isEtfs :boolean  = false;

  @Input() attachedSymbolWithName :boolean = false;

  activeCard:boolean;
  componeysIds = [];
  componeysIdArray=[];


  constructor(private router: Router) { }

  ngOnInit(): void {
    this.name = (this.name) ? this.name : "";
    this.img = (this.img) ? this.img : "https://storage.googleapis.com/iexcloud-hl37opg/api/logos/IEI.png";
    this.company = (this.company) ? this.company : "Sector";
    if(this.price){
      // console.log(["this.price", this.price, "this.previous_price", this.previous_price]);
      
      let price_diff = this.price - this.previous_price;
      if (price_diff > 0) {
        this.isPositive = 1
      }else if (price_diff < 0) {
        this.isPositive = 2
      } else {
        this.isPositive = 0        
      }
    
      let percentage_val = 0;
      if (this.previous_price > 0) {
        percentage_val = (this.price - this.previous_price)/ this.previous_price*100;
      }
      this.stats = "$" + price_diff.toFixed(2) + "(" + percentage_val.toFixed(2) + "%)";
    }
   
  }

//   getAnswers(event) {
   
//  }

  handleClick(event ,cid){
    if(this.longPressState){
      

    //  else {
    //     console.log('unchecked: ' + event.target.id);
    //  }
    }
    // if(!this.companyId){ return; }
else{
    this.router.navigate(['explore/stocks/'+this.companyId])
}
  }

}
