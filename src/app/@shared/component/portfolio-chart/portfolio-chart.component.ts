import { Component, Input, OnInit, Output, EventEmitter,  } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartTooltipOptions } from "chart.js";
import { Color, Label } from "ng2-charts";

@Component({
  selector: 'app-portfolio-chart',
  templateUrl: './portfolio-chart.component.html',
  styleUrls: ['./portfolio-chart.component.scss']
})
export class PortfolioChartComponent implements OnInit {

  @Output() handleMonthPerformance: EventEmitter<any> = new EventEmitter();

  private _label;
  get label(): any { 
    return this._label;
  }

  @Input()
  set label(val: any) {
    this._label = val;
    if (val.length > 0) {
      this.refreshGraph();
    }
  }

  @Input()
  dataSets: Array<{
    data: number[],
    label: string,
  }> = [
      {
        data: [],
        label: ""
      }
    ]

  @Input() months: Array<any> = [];

  

  dataLoaded: boolean = false;
  monthsInCharts: any = [];
  
  public lineChartLegend = true;
  public lineChartType = "line" as const;
  public lineChartPlugins = [];

  constructor() { }

  ngOnInit(): void {
    console.log(['dataSets', this.dataSets, "label", this.label, "months", this.months]);
    // this.refreshGraph();    
  }

  refreshGraph(){
    setTimeout(() => {
      this.lineChartLabels = this.label
      this.lineChartData = this.dataSets
      this.monthsInCharts = this.months
      this.dataLoaded = true
    }, 1000)
  }

  public lineChartData: ChartDataSets[] = this.dataSets;

  public lineChartLabels: Label[] = this.label;

  public lineChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false,
      position: 'bottom',
      labels: {
        usePointStyle: true,
        fontColor: 'black',
        boxWidth: 8,
        padding: 20,
      },
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            display: false,
          },
          ticks: {
            autoSkip: true,
            maxTicksLimit: 5,
          },
        },
      ],
      yAxes: [
        {
          display: true,
          gridLines: {
            display: true,
            drawBorder: false,
          },
        },
      ],
    },
  };

  public lineChartColors: Color[] = [
    {
      backgroundColor: '#f0f8ff00',
      pointHoverBackgroundColor: '#fff',
      pointBorderColor: '#fff',
      borderColor: '#196be5BA',
     
    }
  ];


  handleParentMonth(month:any){
    this.handleMonthPerformance.emit(month)
  }

}
