import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NeedHelpCardComponent } from './need-help-card.component';

describe('NeedHelpCardComponent', () => {
  let component: NeedHelpCardComponent;
  let fixture: ComponentFixture<NeedHelpCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NeedHelpCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NeedHelpCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
