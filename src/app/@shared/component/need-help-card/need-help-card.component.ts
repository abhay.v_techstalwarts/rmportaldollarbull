import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-need-help-card',
  templateUrl: './need-help-card.component.html',
})
export class NeedHelpCardComponent implements OnInit {

  constructor(private router: Router,) { }

  ngOnInit(): void {
  }
  navigateToAppoitment(){
    this.router.navigate(['funding/appointment']);
  }

}
