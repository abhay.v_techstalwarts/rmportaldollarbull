import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cards-list',
  templateUrl: './cards-list.component.html',
  styleUrls: ['./cards-list.component.scss']
})
export class CardsListComponent implements OnInit {

  @Input() title: any = "";
  @Input() type: any = "";
  
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  handleViewAll(){
    switch (this.type) {
      case "top_etfs":
        this.router.navigate(['explore/stocks/all-etfs']);
        break;
      case "top_gainers":
        this.router.navigate(['explore/stocks/top-gainers']);
        break;
      case "top_losers":
        this.router.navigate(['explore/stocks/top-losers']);
        break;
      case "top_trader":
        this.router.navigate(['explore/stocks/most-traded']);
        break;
      default:
        this.router.navigate(['explore/stocks/all-stocks']);
        break;
    }
  }
}
