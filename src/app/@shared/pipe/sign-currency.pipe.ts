import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'signCurrency' })
export class SignCurrencyPipe implements PipeTransform {

    transform(value: any,...args: unknown[]): string {
        console.log(value);
        
        if (value != null){
            let val = value.replace(",","")
            let sign =( val > 0) ? "+": (val < 0) ?  "-": "";
            return sign + "$" + value.replace("-","");
        }
        return "";
    }
}