import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'baseTitle'
})
export class BaseTitlePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    return value.split('-').join(' ');
  }

}
