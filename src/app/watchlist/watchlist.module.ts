import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../@shared/shared.module';
import { WatchlistRoutingModule } from './watchlist-routing.module';
import { WatchlistComponent } from './watchlist.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'


@NgModule({
  declarations: [WatchlistComponent],
  imports: [
    CommonModule,
    WatchlistRoutingModule,
    SharedModule,
    FormsModule, 
    ReactiveFormsModule,
  ]
})
export class WatchlistModule { }
