import { SharedModule } from 'src/app/@shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqSupportRoutingModule } from './faq-support-routing.module';
import { FaqComponent } from './faq/faq.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [FaqComponent],
  imports: [
    CommonModule,
    FaqSupportRoutingModule,
    SharedModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [FaqComponent]
})
export class FaqSupportModule { }
