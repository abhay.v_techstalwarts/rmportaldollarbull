import { CommonService } from 'src/app/@core/service/common/common.service';
import { JwtService } from './../../@core/service/jwt.service';
import { Component, OnInit } from '@angular/core';
import {NgbAccordionConfig} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
  providers: [NgbAccordionConfig] 
})
export class FaqComponent implements OnInit {
  queryForm:any;
  gettingToKnowDollarBull:any;
  howInvestingOnDollarbull:any;
  addingFunds:any;
  withdrawal:any;
  taxation:any;
  saftyAndSecurty:any;
  investingAndTrading:any;
  activeIds = 'gkd-45';
  faqList:any = [];
  constructor(config: NgbAccordionConfig, private fb: FormBuilder, private jwtService: JwtService, private CommonService: CommonService) { 
    config.closeOthers = true;
    config.type = 'white';
    this.queryForm = this.fb.group({
      query: ['', [Validators.required,]],

    });
  }

  ngOnInit(): void {
    this.CommonService.get_faq().subscribe(
      success => {  
      if(success.status == 1){
        // this.partnerForm.reset();
        console.log(success.result.records);
        this.faqList = [...success.result.records];

       this.addingFunds =  this.faqList.filter((el:any) => {
         return el.section == 'Adding Funds';
        })


        this.withdrawal =  this.faqList.filter((el:any) => {
          return el.section == 'Withdrawal';
         })

         
        this.taxation =  this.faqList.filter((el:any) => {
          return el.section == 'Taxation';
         })
        
         this.saftyAndSecurty =  this.faqList.filter((el:any) => {
          return el.section == 'Safety and Security';
         })

         this.investingAndTrading =  this.faqList.filter((el:any) => {
          return el.section == 'Investing and Trading';
         })

         this.howInvestingOnDollarbull =  this.faqList.filter((el:any) => {
          return el.section == 'How Investing on DollarBull Works?';
         })

         console.log("how invest" +  this.howInvestingOnDollarbull );

         this.gettingToKnowDollarBull =  this.faqList.filter((el:any) => {
          return el.section == 'Getting to know DollarBull';
         })
        

      }
      },
      error=>{
        console.log("message");
        console.log(error);

      }
    
    );
  }
  submitQuery(){
    let data = {
      query: this.queryForm.get('query').value,
    }

    this.CommonService.rm_Query(data).subscribe((res) => {
      let response: any = res;

      if(response.status == 1){
        alert(response.message);
        this.queryForm.reset();
      }

    });
  }

}
