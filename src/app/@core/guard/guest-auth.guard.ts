import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth/auth.service';
import { NavigationService } from '../service/navigation.service';

/**
 * NoGuestAuthGuard helps guarding no-guest routes like, Orders, Profile Information, etc.
 * Routes which shall be accessible by non-guest users only
 */
@Injectable({
  providedIn: 'root',
})
export class NoGuestAuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private navigationService: NavigationService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.authService.isGuestUser()) {
      if (route.data && route.data.noGuestGuardRedirectUrl) {
        this.navigationService.navigate([route.data.noGuestGuardRedirectUrl]);
      } else {
        this.navigationService.stackFirst(['/dashboard']);
      }
      return false;
    }
    return true;
  }
}
