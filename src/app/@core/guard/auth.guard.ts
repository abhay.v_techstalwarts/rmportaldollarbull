import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth/auth.service';
import { NavigationService } from '../service/navigation.service';
import { Router } from '@angular/router';

/**
 * AuthGuard helps guarding authenticated routes like dashboard, profile, etc.
 * Routes which shall be accessible by auth users
 */
@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private navigationService: NavigationService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (!this.authService.isAuthenticated()) {
      if (route.data && route.data.authGuardRedirectUrl) {
        this.navigationService.navigate([route.data.authGuardRedirectUrl]);
      } else {
        this.navigationService.stackFirst(['/splash']);
      }
      return false;
    } else {
      if(state.url != "/login") {
        return true;
      } else {
        this.router.navigate(['dashboard/categorywatchlist']);
        return true;
      }
    }
  }
}
