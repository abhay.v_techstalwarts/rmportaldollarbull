import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, shareReplay } from 'rxjs/operators';
import {BehaviorSubject, Observable, of } from 'rxjs';
import { NotificationPayloadI} from './notification.interface';
import { NotificationParser } from './notification.parser';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  private urls = {
    notifications: '@UserAPI/notifications',
    readNotification:'@UserAPI/readnotification'
  };
  newNotification:BehaviorSubject<Boolean> = new BehaviorSubject(false);
  
  constructor(private httpClient: HttpClient) {}


  public getNotifications(payload) {
    let url = this.urls.notifications+`?page=${payload.page}&perPage=${payload.perPage}`;

    return this.httpClient.get(url).pipe(map(res => NotificationParser.notificationList(res)));;
  }

  public readNotification(notificationId: string) {
    let payload ={
      notification_id:`${notificationId}`
    }
    return this.httpClient.post(this.urls.readNotification, payload);
  }
  
  public getNewNotification():any | Observable<any> {
    return this.newNotification.asObservable();
}

  public setNewNotification(newNotification:boolean){
    this.newNotification.next(newNotification);
  }


  
}
