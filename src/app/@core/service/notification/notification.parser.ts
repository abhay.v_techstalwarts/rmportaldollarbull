
const notificationList = (res: any) => {
  let result: any = {
    page: 0,
    perPage: 0,
    totalCount: 0,
    records: [
    ]
  }

  if (res && res.result) {
    res = res.result;
  }

  if (!res || !res.records ||!res.records.length) {
    return result;
  }

  result.page = res.page ? res.page : 0,
  result.perPage = res.perPage ? res.perPage : 0,
  result.totalCount = res.totalCount ? res.totalCount : 0,
  result.records = res.records;

  return result;
};

export const NotificationParser = {
  notificationList,
};
