export interface NotificationPayloadI {
    notification_id: number;
    title: string;
    body: string;
    reference_id: string;
    type: string;
    status: string;
}

