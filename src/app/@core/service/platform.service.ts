import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { BehaviorSubject, Observable } from 'rxjs';
const { Device } = Plugins;

@Injectable({
  providedIn: 'root',
})
export class PlatformService {
  private deviceInfo: any = null;
  private platform: 'ios' | 'android' | 'web' | 'electron'= 'web';
  private platformDetector : BehaviorSubject<String> = new BehaviorSubject('web');;
  

  constructor() {
    // Device.getInfo().then(info =>{
    //   this.deviceInfo = info;
    //   this.platform = info.platform;
    // })
  }

  isDesktop(){
    const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
    return vw > 991;
  }

  async getDeviceDetails() {
    // const info = await Device.getInfo();
    // this.deviceInfo = info;
    // this.platform = info.platform;
    
  };

  //   get platform info for both web and app
  getPlatform() : string {
    return this.platform;
  }

  setPlatform(platform){
    this.platformDetector.next(platform);
  }

  get _getPlatform():Observable<any> {
    return this.platformDetector.asObservable();
  }

  //   get device info spcifically for apps
  getDeviceInfo() {
    return new Observable<any>((observer) => {
      observer.next(this.deviceInfo);
    });
  }

  //   check current platform is matching
  isPlatform(name: 'web' | 'android' | 'web'):boolean {
    return this.platform === name ? true : false;
  }
}
