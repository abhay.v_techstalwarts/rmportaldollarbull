const list = (res: any) => {
  let result: any[] = [];

  if (res && res.result) {
    res = res.result;
  }

  if (!res || !res.length) {
    return result;
  }

   return result;
};

export const SignupParser = {
  list,
};
