import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, shareReplay } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { SignupPayloadI, onEmailOTPSubmitI, updateMobileNoI, onMobileOTPSubmitI, submitRMCodeI, app_loginI, onEmailOTPSubmitLoginI } from './signup.interface';
import { SignupParser } from './signup.parser';

@Injectable({
  providedIn: 'root',
})
export class SignUpService {
  private urls = {
    SignUp: '@UserAPI/signup',
    EmailOTPVerify: '@UserAPI/verifyemail',
    UpdateMobileNo: '@UserAPI/updatemobileno',
    MobileOTPVerify: '@UserAPI/verifyotp',
    SubmitRMCode: '@UserAPI/updateprofile',
    ResendEmailOTP: '@UserAPI/resendemailotp',
    ResendMobileOTP: '@UserAPI/resendotp',
    App_login: '@UserAPI/applogin',
    RM_Login: '@RmAPI/login',
    OnEmailOTPSubmitLogin: '@UserAPI/login/verifyemail',
  };
  
  constructor(private httpClient: HttpClient) {}


  public callSignupAPI(payload: SignupPayloadI) {
    return this.httpClient.post(this.urls.SignUp, payload);
  }

  public onEmailOTPSubmit(payload: onEmailOTPSubmitI) {
    return this.httpClient.post(this.urls.EmailOTPVerify, payload);
  }

  public updateMobileNo(payload: updateMobileNoI) {
    return this.httpClient.post(this.urls.UpdateMobileNo, payload);
  }

  public onMobileOTPSubmit(payload: onMobileOTPSubmitI) {
    return this.httpClient.post(this.urls.MobileOTPVerify, payload);
  }

  public submitRMCode(payload: submitRMCodeI) {
    return this.httpClient.post(this.urls.SubmitRMCode, payload);
  }

  public resendEmailOTP() {
    return this.httpClient.post(this.urls.ResendEmailOTP,{});
  }

  public resendMobileOTP() {
    return this.httpClient.post(this.urls.ResendMobileOTP,{});
  }

  public app_login(payload: app_loginI) {
    console.log("app_login",JSON.stringify({
      url:this.urls.App_login,
      payload
    }));
    
    return this.httpClient.post(this.urls.App_login, payload);
  }
  //@ts-ignore
  public rm_login(payload: rm_loginI) {
    console.log("rm_login",JSON.stringify({
      url:this.urls.RM_Login,
      payload
    }));
    
    return this.httpClient.post(this.urls.RM_Login, payload);
  }

  public onEmailOTPSubmitLogin(payload: onEmailOTPSubmitLoginI) {
    return this.httpClient.post(this.urls.OnEmailOTPSubmitLogin, payload);
  }
}
