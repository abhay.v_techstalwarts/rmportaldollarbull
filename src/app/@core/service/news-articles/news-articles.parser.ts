const list = (res: any) => {
  let result: any[] = [];

  if (res && res.result) {
    res = res.result;
  }

  if (!res || !res.length) {
    return result;
  }

  res.forEach((r: any) => {
    const item = {
      company_news_id: r['company_news_id'] ? r['company_news_id'] : '',
      company_id: r['company_id'] ? r['company_id'] : '',
      headline: r['headline'] ? r['headline'] : '',
      source: r['source'] ? r['source'] : '',
      url: r['url'] ? r['url'] : '',
      summary: r['summary'] ? r['summary'] : '',
      image: r['image'] ? r['image'] : '',
      createdAt: r['createdAt'] ? r['createdAt'] : '',
      updatedAt: r['updatedAt'] ? r['updatedAt'] : '',
    };
    result.push(item);
  });

  return result;
};

const newsList = (res: any) => {
  let result: any = {
    page: 0,
    perPage: 0,
    totalCount: 0,
    records: [
    ]
  }

  if (res && res.result) {
    res = res.result;
  }

  if (!res || !res.records ||!res.records.length) {
    return result;
  }

  result.page = res.page ? res.page : 0,
  result.perPage = res.perPage ? res.perPage : 0,
  result.totalCount = res.totalCount ? res.totalCount : 0,
  result.records = res.records.map((r: any) => {
    const item = {
      company_news_id: r['company_news_id'] ? r['company_news_id'] : '',
      company_id: r['company_id'] ? r['company_id'] : '',
      headline: r['headline'] ? r['headline'] : '',
      source: r['source'] ? r['source'] : '',
      url: r['url'] ? r['url'] : '',
      summary: r['summary'] ? r['summary'] : '',
      image: r['image'] ? r['image'] : '',
      createdAt: r['createdAt'] ? r['createdAt'] : '',
      updatedAt: r['updatedAt'] ? r['updatedAt'] : '',
    };
    return item;
  });

  return result;
};

export const newsArticlesParser = {
  list,
  newsList,
};
