export interface NewsArticlesPayloadI {
    limit: number;
    offset: number;
    company_id?: string;
}

export interface NewsPayloadI {
    page: number;
    perPage: number;
    company_id?: string;
}