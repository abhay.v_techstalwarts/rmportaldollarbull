// get all stocks payload
export interface StocksListPayloadI {
    limit: number;
    offset: number;
    search?: string;
    sort?: string;
    category?: string;
    filter?: any;
}