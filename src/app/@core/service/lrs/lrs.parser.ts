
const banksListParser = (response: any) => {
    let result = [];
    
    if(!response || !response.length){
        return result
    }

    for(const res of response){
        result.push({id:res['bank_master_id'], bank:res['bank_name']});
    }
    return result;
}

const remitOnlineParser = (response: any) => {
    let result = [];
    
    if(!response.data || !response.data.length){
        return result
    }
    result = response.data.slice(0);
    result.sort(function(a,b) {
        return a.order - b.order;
    });
    return {
        data : result,
        net_banking_url : response.net_banking_url,
        reference_link : response.reference_link
    };
}

const remitOfflineParser = (response: any) => {
    let result = {
        base64:null,
        pdf:null,
    };

    if(!response || !response.result || !response.result.data){
        return result
    }
    result.base64 = response.result.data.base64 ?response.result.data.base64 :null;
    result.pdf = response.result.data[0].pdf_path ? response.result.data[0].pdf_path :null;
    return result;
}

const mailLrsProcessParser = (response: any) => {

    let result = "";
    if(!response || !response.result || !response.result.data){
        return result
    }
    result = response.result.data
    return result;
}

const downloadLrsPdfParser = (response: any) => {
    const pdfFile = new Blob(response, { type: "application/pdf" });
    return pdfFile;
}


export const lrsParser = {
    banksListParser,
    remitOnlineParser,
    remitOfflineParser,
    mailLrsProcessParser,
    downloadLrsPdfParser
};