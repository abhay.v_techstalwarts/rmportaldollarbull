import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { filter, map, shareReplay, tap } from 'rxjs/operators';
import { lrsParser } from './lrs.parser'
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LrsService {
  private urls = {
    banksMaster: '@UserAPI/banks',
    remitOnline: '@UserAPI/onlinelrsprocess',
    remitOffline: '@UserAPI/offlinelrsprocess',
    mailLrsProcess: '@UserAPI/maillrsprocess'
  };

  constructor(private httpClient: HttpClient) { }

  public getBanks(is_lrs_active:number=1){
    //@ts-ignore
    let payload={
      is_lrs_active : is_lrs_active
    }
    return this.httpClient.post(this.urls.banksMaster, payload).pipe(map((res:any) => lrsParser.banksListParser(res.result)));
  }

  public RemitOnline(payload){
    //@ts-ignore
    return this.httpClient.post(this.urls.remitOnline, payload).pipe(map(res => lrsParser.remitOnlineParser(res.result)));
  }

  public RemitOffline(payload){
    //@ts-ignore
    return this.httpClient.post(this.urls.remitOffline, payload).pipe(map(res => lrsParser.remitOfflineParser(res)));
  }


  
  public mailLrsProcess(bank_master_id){
    let payload ={
      bank_master_id
    }
    return this.httpClient.post(this.urls.mailLrsProcess, payload).pipe(map(res => lrsParser.mailLrsProcessParser(res)));
  }

  public downloadLrsPdf(pdfLink){
    return this.httpClient.get(pdfLink,{headers:null} ).pipe(map(res => lrsParser.downloadLrsPdfParser(res)));
  }


  

  
}
