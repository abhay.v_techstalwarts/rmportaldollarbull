import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { filter, map, shareReplay, tap } from 'rxjs/operators';
import { customerParser } from './customer.parser'
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  private urls = {
    submitQuery: '@UserAPI/submitquery',
    bookAppointment: '@UserAPI/sendsupportmail',
    updateDeviceToken: '@UserAPI/updatedevicetoken',
    updateMobileNo: '@UserAPI/updatemobileno',
    getUserProfile: '@UserAPI/myprofile',
  };

  constructor(private httpClient: HttpClient) { }
    // Customer Query
    public submitQuery(payload: any): Observable<any> {
      return this.requestsubmitQuery(payload).pipe(shareReplay(1));
    }
  
    private requestsubmitQuery({ query, description }: { query: string , description: string }) {
      const data = {
        query,
        description
      }
      return this.httpClient.post(this.urls.submitQuery, data).pipe(map(customerParser.submitQuery));
    }


    public requestAppointment(payload: any): Observable<any> {
      return this.httpClient.post(this.urls.bookAppointment, payload).pipe(map(customerParser.bookAppointment));
    }



      // Add Company to Watchlist
  public updateDeviceToken(deviceType: string, deviceToken: string): Observable<any> {
    return this.requestUpdateDeviceToken(deviceType, deviceToken).pipe(shareReplay(1));
  }

  private requestUpdateDeviceToken(deviceType: string, deviceToken: string) {
    const data = {
      device_type: deviceType,
      device_token: deviceToken,
    }
    return this.httpClient.post(this.urls.updateDeviceToken, data).pipe(map(res => res));
  }

  public updateMobileNo(data: any): Observable<any> {
    return this.httpClient.post(this.urls.updateMobileNo, data).pipe(map(res => res));
  }

  public getProfile(): Observable<any> {
    return this.httpClient.post(this.urls.getUserProfile, {}).pipe(map(res => res));
  }


  getCustomerProfile(): any {
    let CustomerProfile = window.localStorage['CustomerProfile'];
    if (CustomerProfile) {
      return JSON.parse(CustomerProfile)
    }
    return "";
  }

  saveCustomerProfile(data: any): void {
    window.localStorage['CustomerProfile'] = JSON.stringify(data);
  }

  destroyCustomerProfile(): void {
    window.localStorage.removeItem('CustomerProfile');
  }

  
}
