import { Injectable } from '@angular/core';
import jwtDecode from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class JwtService {
  getToken(): string {
    return window.localStorage['jwtToken'];
  }

  saveToken(token: string): void {
    window.localStorage['jwtToken'] = token;
  }

  saveRefCode(refCode: string): void {
    window.localStorage['refCode'] = refCode;
  }

  getRefCode(): string {
    return window.localStorage['refCode'];
  }

  destroyRefCode(): void {
    window.localStorage.removeItem('refCode');
  }

  destroyToken(): void {
    window.localStorage.removeItem('jwtToken');
  }

  // rm portal
  
  savePartnercode(partner_code:string):void{
    window.localStorage['partner_code'] = partner_code;
  }

  getPartnercode():void{
    return window.localStorage['partner_code'];
  }

  saveRM_AdvisorCode(rm_advisor_code:string):void{
    window.localStorage['rm_advisor_code'] = rm_advisor_code;
  }

  getRM_AdvisorCode():void{
    return window.localStorage['rm_advisor_code'];
  }

  saveRM_Email_ID(rm_email_id:string):void{
    window.localStorage['rm_email_id'] = rm_email_id;
  }

  getRM_Email_ID():void{
    return window.localStorage['rm_email_id'];
  }

  saveRM_Master_Id(rm_master_id:string):void{
    window.localStorage['rm_master_id'] = rm_master_id;
  }

  getRM_Master_Id():void{
    return window.localStorage['rm_master_id'];
  }

  saveRM_Mobile_number(rm_mobile_no:string):void{
    window.localStorage['rm_mobile_no'] = rm_mobile_no;
  }

  getRM_Mobile_number():void{
    return window.localStorage['rm_mobile_no'];
  }

  saveRM_Referal_Code(rm_referal_code:string):void{
    window.localStorage['rm_referal_code'] = rm_referal_code;
  }

  getRM_Referal_Code():void{
    return window.localStorage['rm_referal_code'];
  }

  destroyPartnercode():void{
    window.localStorage.removeItem('partner_code');
  }

  // end rm portal

  readTokenData(): any {
    const token = this.getToken();
    const payload = jwtDecode(token);
    return payload;
  }
}
