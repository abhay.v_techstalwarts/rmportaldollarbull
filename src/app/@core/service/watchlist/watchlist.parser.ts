const list = (res: any) => {
  let result: any[] = [];
  
  if (res && res.result) {
    res = res.result;
  }

  if (!res || !res.length) {
    return result;
  }

  result = res.map((r: any) => {
    const item = {
      watchlistId: r.watchlist_id ? r.watchlist_id : '',
      watchlistName: r.watchlist_name ? r.watchlist_name.toUpperCase() : null,
    };
    return item;
  });

  return result;
};

const watchlistByCompany = (res: any) => {
  let result: any[] = [];
  
  if (res && res.result) {
    res = res.result;
  }
  
  if (!res || !res.length) {
    return result;
  }
  
  result = res.map((r: any) => {
    const item = {
      watchlistId: r.watchlist_id ? r.watchlist_id : '',
      watchlistName: r.watchlist_name ? r.watchlist_name : null,
      isAdded: r.isAdded ? r.isAdded : false,
    };
    return item;
  });

  return result;
};

const watchlist = (res: any) => {
  let result: any = {
    watchlistName: "",
    watchlistId: null
  }

  if (!res || !res.result) {
    return result;
  }
  
  if (res && res.result) {
    res = res.result;
  }

  result={
    watchlistId: res.watchlist_id ? res.watchlist_id : '',
    watchlistName: res.watchlist_name ? res.watchlist_name : null,
  };
  return result;
};

const common = (res: any) => {
  let result: any ="";
  if (!res || !res.result) {
    return result;
  }
  if (res && res.result) {
    res = res.result;
  }

  result = res;
  return result;
};

const companyList = (res: any) => {
  let result: any = {
    page: 0,
    perPage: 0,
    totalCount: 0,
    records: [
    ]
  }

  if (res && res.result) {
    res = res.result;
  }

  if (!res || !res.records ||!res.records.length) {
    return result;
  }

  result.page = res.page ? res.page : 0,
  result.perPage = res.perPage ? res.perPage : 0,
  result.totalCount = res.totalCount ? res.totalCount : 0,
  result.records = res.records.map((r: any) => {
    const item = {
      name: r.name ? r.name : '',
      symbol: r.symbol ? r.symbol : '',
      img: r.logo ? r.logo : null,
      price: r.current_price ? r.current_price : null,
      company: r.sector ? r.sector : null,
      stats: r.price_diff ? r.price_diff : null,
      companyId: r.company_id ? r.company_id : null,
      previous_price: r.yesterday_price ? r.yesterday_price : null,
      percentage_change: r.yesterday_price > 0 ? (r.current_price - r.yesterday_price) / r.yesterday_price * 100 : null,
    };
    return item;
  });

  return result;
};

const addCompany = (res: any) => {
  let result: any = {
    companyId: 0,
    symbol: 0,
    name: "",
    logo: null,
    exchange: null,
    sector: "",
    currentPrice: 0,
    yesterdayPrice: 0,
    priceDiff: 0
  }

  if (res && res.result) {
    res = res.result;
  }

  if (!res || !res.result) {
    return result;
  }

  result.companyId = res.company_id ? res.company_id : 0;
  result.symbol = res.symbol ? res.symbol : 0;
  result.name = res.name ? res.name : "";
  result.logo = res.logo ? res.logo : null;
  result.exchange = res.exchange ? res.exchange : null;
  result.sector = res.sector ? res.sector : "";
  result.currentPrice = res.current_price ? res.current_price : 0;
  result.yesterdayPrice = res.yesterday_price ? res.yesterday_price : 0;
  result.priceDiff = res.price_diff ? res.price_diff : 0;
  return result;
};

export const watchlistParser = {
  list,
  companyList,
  watchlist,
  addCompany,
  common,
  watchlistByCompany,
};
