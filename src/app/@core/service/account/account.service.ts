import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { filter, map, shareReplay, tap } from 'rxjs/operators';
import { accountParser } from './account.parser'
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  private urls = {
    accounts: '@UserAPI/rm/getCustomerAccounts',
    syncAccount: '@UserAPI/syncCustomerAccount',
    updateTagName:'@UserAPI/updateAccountTagName',
  };
  
  accounts: BehaviorSubject<any[]> = new BehaviorSubject([]);
  selectedAccount: BehaviorSubject<Number> = new BehaviorSubject(window.localStorage['selectedAccount']);

  constructor(private httpClient: HttpClient) { }


  //Accounts

  private requestAccounts(customer_id:string) {
    let data = {
      customer_id : customer_id
    }
    return this.httpClient.post<any>(`${this.urls.accounts}`, data).pipe(map(res => accountParser.accounts(res.result)));
  }

  public getCustomerAccount(customer_id, isForceCall = true): any | Observable<any> {
    if (isForceCall) {
      this.requestAccounts(customer_id).subscribe((success: any) => {
        let accounts = success && success.length ? success : [];
        this.accounts.next(accounts);
      })
    }
    return this.accounts.asObservable();
  }

  public setSelectedAccount(selectedAccount: number) {
    this.saveSelectedAccount(selectedAccount);
    this.selectedAccount.next(selectedAccount);
  }

  SelectedAccount():any | Observable<any> {
    return this.selectedAccount.asObservable();
  }

  getSelectedAccount(): number {
    const tmp = window.localStorage['selectedAccount'];
    return tmp ? tmp : null;
  }

  saveSelectedAccount(selectedAccount: number): void {
    window.localStorage['selectedAccount'] = selectedAccount;
  }

  destroySelectedAccount(): void {
    window.localStorage.removeItem('selectedAccount');
  }

  //Sync Account
  public syncAccounts() {
    return this.httpClient.post<any>(`${this.urls.syncAccount}`,{})
  }

  
  //Upade Account Tag
  public updateAccountTag(payload) {
    return this.httpClient.post<any>(`${this.urls.updateTagName}`,payload)
  }

}
