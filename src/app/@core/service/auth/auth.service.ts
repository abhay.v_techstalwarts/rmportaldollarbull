import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map, shareReplay } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { JwtService } from '../jwt.service';
import {CommonService} from '../common/common.service'
import {CustomerService} from '../customer/customer.service'


@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private urls = {
    guestLogin: '@UserAPI/guestLogin',
    tradeStationLogin: '@UserAPI/tradeStationlogin',
    customerLogin: '@UserAPI/loginCustomer',
  };

  constructor(private httpClient: HttpClient, private jwtService: JwtService, private commonService:CommonService, private customerService:CustomerService) {}

  // validate authentication
  public isAuthenticated(): boolean {
    const token = this.jwtService.getToken();
    return token ? true : false;
  }

  //   validate if logged in user is a guest user
  public isGuestUser(): boolean {
    const payload = this.jwtService.readTokenData();
    return payload && payload.isTmpCustomer ? true : false;
  }

  //   validate if logged in user is a guest user
  public isNotGuestUser(): boolean {
    const payload = this.jwtService.readTokenData();
    return payload && payload.isTmpCustomer ? false : true;
  }

  // authenticate user for the platform
  public authenticate(customer_id?: string): Observable<any> {
    let response: Observable<any>;

    if (customer_id) {
      // response = this.httpClient.post(this.urls.tradeStationLogin, { code });
      response = this.httpClient.post(this.urls.customerLogin, { customer_id });
    } else {
      response = this.httpClient.post(this.urls.guestLogin, {});
    }

    response = response.pipe(shareReplay(1)).pipe(
      map((res: any) => {
        if (res.status === 1) {
          this.jwtService.saveToken(res.result.token);
        } else {
          this.jwtService.destroyToken();
        }
        return res;
      })
    );
    return response;
  }

  //   logout user
  public logout(): Observable<any> {
    this.jwtService.destroyToken();
    this.destroyHasInvested();
    this.commonService.destroyCurrentStage();
    this.jwtService.destroyRefCode();
    this.customerService.destroyCustomerProfile();
    return of(
      new HttpResponse({
        status: 1,
      })
    );
  }

  public tradeStationLogin(code?: string, redirect_url?:string): Observable<any> {
    let payload={
      code,
      redirect_url
    }
      return this.httpClient.post<any>(`${this.urls.tradeStationLogin}`, payload).pipe(map(res => res))
  }

  getHasInvested(): boolean {
    const tmp = window.localStorage['Invested'];
    return tmp == 'true' ? true : false;
  }

  saveHasInvested(hasInvested: boolean): void {
    window.localStorage['Invested'] = hasInvested;
  }

  destroyHasInvested(): void {
    window.localStorage.removeItem('Invested');
  }
}
