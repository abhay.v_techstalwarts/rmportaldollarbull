import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { filter, map, shareReplay, tap } from 'rxjs/operators';
import { commonParser, masterFilterParser } from './common.parser'
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { AccountService } from "../account/account.service";
@Injectable({
  providedIn: 'root',
})
export class CommonService {
  private urls = {
    filterMaster: '@DataAPI/filter',
    stats: '@DataAPI/stats',
    overview: '@RmAPI/portfolio_overview',
    performance: '@RmAPI/performance_chart',
    investmentChart: '@RmAPI/getCustomerInvestmentsPieChart',
    stockPriceChart: '@DataAPI/stock_price_chart',
    topSectorMaster: '@RmAPI/sectors',
    getCustomerLatestStage: '@RmAPI/currentstage',
    updateCustomerStage: '@RmAPI/updatestage',
    getCustomerTradestaionBalanace: '@UserAPI/getAccountBalance',
    getMarketTime: '@UserAPI/marketTime',
    refer_Client: '@RmAPI/referclient',
    rm_Client_List:'@RmAPI/clients',
    submitQuery:'@RmAPI/submitquery',
    forgotPassword:'@RmAPI/forgotpassword',
    portfolio_my_investment:'@RmAPI/my_investment',
    portfolio_rm_overview:'@RmAPI/portfolio_overview',
    partners_list:'@RmAPI/partners',
    partners_signup:'@RmAPI​/signup',
    reset_password:'@RmAPI/resetpassword',
    changepassword:'@RmAPI/changepassword',
    faqs: '@FaqApi/'
  };

  // @ts-ignore
  private statsCache: Observable<any>;

  // @ts-ignore
  private filterDataCache$: Observable<any>;

  // @ts-ignore
  private overViewCache: Observable<any>;
  //@ts-ignore
  public selectedFilter$ = {};

  //@ts-ignore
  private performanceCache$: Observable<any>;
  private customerInvestmentCache$: Observable<any>;

  //@ts-ignore
  private stockPriceChartCache$: Observable<any>

  //@ts-ignore
  private topSectorCache$: Observable<any>;


  accountBalance: BehaviorSubject<Number> = new BehaviorSubject(0);
  currentStage: BehaviorSubject<Number> = new BehaviorSubject(0);

  public isLoaderEnabled$ = new Subject()

  constructor(private httpClient: HttpClient, private accountService: AccountService) { }

  private filterData = [
    {
      name: "Sort By",
      type: "single",
      value: [
        "Market Cap",
        "Price- low"
      ]
    },
    {
      name: "Sector",
      type: "multi",
      value: [
        "Basic Material",
        "Consumer Defensive"
      ]
    }
  ]

  get_faq(): Observable<any> {
    return this.httpClient.get(this.urls.faqs+'faq');
  }

  //filterInitialCache
  public getFilterMaster(): Observable<any> {
    if (!this.filterDataCache$) {
      this.filterDataCache$ = this.requestFilterMaster()
    }
    return this.filterDataCache$
  }

  public requestFilterMaster() {
    return this.httpClient.get<any>(`${this.urls.filterMaster}`).pipe(map(res => masterFilterParser(res)))
  }

  public updateSelectedFilter(filterKey: any, filterValue: any) {
    this.selectedFilter$[filterKey] = filterValue
    return this.selectedFilter$
  }

  public getOverview(forceCall = false): Observable<any> {
    let customer_tradestation_account_id = this.accountService.getSelectedAccount();
    if (customer_tradestation_account_id > 0) {
      if (!this.overViewCache || forceCall) {
        this.overViewCache = this.requestOverviewStat(customer_tradestation_account_id).pipe(shareReplay(1));
      }
    }
    return this.overViewCache;
  }

  //Performance
  public getPerformance(payload: any, forceCall: boolean = false): Observable<any> {
    let customer_tradestation_account_id = this.accountService.getSelectedAccount();
    if (customer_tradestation_account_id > 0) {
      payload.customer_tradestation_account_id = customer_tradestation_account_id;
      if (!this.performanceCache$ || forceCall) {        
        this.performanceCache$ = this.requestPerformance(payload)
      }
    }
    return this.performanceCache$
  }

  private requestPerformance(payload: any) {
    return this.httpClient.post<any>(`${this.urls.performance}`, payload).pipe(map(res => commonParser.performance(res.result)))
  }

  public getMarketTime() {
    return this.httpClient.post<any>(`${this.urls.getMarketTime}`, []);
  }

  //Investment Chart
  public getCustomerInvestmentChart(payload: any, forceCall: boolean = false): Observable<any> {
    let customer_tradestation_account_id = this.accountService.getSelectedAccount();
    if (customer_tradestation_account_id > 0) {
      payload.customer_tradestation_account_id = customer_tradestation_account_id;
      if (!this.customerInvestmentCache$ || forceCall) {
        this.customerInvestmentCache$ = this.requestCustomerInvestmentChart(payload)
      }
    }
    return this.customerInvestmentCache$
  }

  private requestCustomerInvestmentChart(payload: any) {
    return this.httpClient.post<any>(`${this.urls.investmentChart}`, payload).pipe(map(res => commonParser.investmentParser(res.result)))
  }

  public getStockPriceChart(payload: any, forceCall: boolean = false) {
    // if (!this.stockPriceChartCache$) {

    // }
    this.stockPriceChartCache$ = this.requestStockPriceChart(payload)
    return this.stockPriceChartCache$
  }

  private requestStockPriceChart(payload) {
    return this.httpClient.post<any>(`${this.urls.stockPriceChart}`, payload).pipe(map(res => commonParser.stockPriceChartParser(res.result)));
  }

  public getMasterFilters() {
    return masterFilterParser(this.filterData)
  }

  // get dollarbull stats
  public getDollarbullStats(forceCall = false): Observable<any> {
    if (!this.statsCache || forceCall) {
      this.statsCache = this.requestDollarbullStats().pipe(shareReplay(1));
    }
    return this.statsCache;
  }

  private requestDollarbullStats() {
    return this.httpClient.get(this.urls.stats).pipe(map(commonParser.stats));
  }

  private requestOverviewStat(customer_tradestation_account_id) {
    let data = {
      customer_tradestation_account_id: customer_tradestation_account_id
    }
    return this.httpClient.post(this.urls.overview, data).pipe(map(commonParser.overviewStat));
  }

  public setLoader(isEnable: boolean) {
    console.log("isEnabled", isEnable);
    this.isLoaderEnabled$.next(isEnable)
  }

  public getTopSectorsMaster() {
    // if (!this.topSectorCache$) {
    // }
    this.topSectorCache$ = this.requestTopSector(); //.pipe(shareReplay(1));
    return this.topSectorCache$;
  }

  private requestTopSector() {
    //@ts-ignore
    return this.httpClient.post(this.urls.topSectorMaster, {}).pipe(map(commonParser.topSectorParser));
  }

  public getCustomerLatestStage() {
    return this.httpClient.post(this.urls.getCustomerLatestStage, {});
  }
  

  // RM portal

  public refer_Client(payload: any) {
    return this.httpClient.post(this.urls.refer_Client, payload);
  }

  public forgotPasswordLink(data:any){
    return this.httpClient.post(this.urls.forgotPassword, data);
  }
  
  public resetPassword(data:any){
    return this.httpClient.post(this.urls.reset_password, data);
  }

  public changePassword(data:any){
    return this.httpClient.post(this.urls.changepassword, data);
  }

  public get_RM_Client_List() {
    return this.httpClient.post(this.urls.rm_Client_List, {});
  }
  public rm_Query(payload:any) {
    return this.httpClient.post(this.urls.submitQuery, payload);
  }
  public getPartnersList() {
    return this.httpClient.get(this.urls.partners_list);
  }
  public signupAPI(payload: any) {
    return this.httpClient.post(this.urls.partners_signup, payload);
  }

  

  public get_Investment_List_By_Customer_ID(payload:any) {
    return this.httpClient.post(this.urls.portfolio_my_investment, payload);
  }

  
  public get_Portfolio_Overview(data:any) {
    return this.httpClient.post(this.urls.portfolio_rm_overview, data);
  }



  // end rm portal

  public updateCustomerStage(payload: any) {
    return this.httpClient.post(this.urls.updateCustomerStage, payload);
  }

  public getCustomerTradestaionBalanace() {
    let url =this.urls.getCustomerTradestaionBalanace;
    url = `${url}?customer_tradestation_account_id=${this.accountService.getSelectedAccount()}`;
    return this.httpClient.get(url);

  }
  public getaccountBalance(isForceCall = true): any | Observable<any> {
    if (isForceCall) {
      this.getCustomerTradestaionBalanace().subscribe((success: any) => {
        let accountBalance = success.result && success.result.account_balance ? success.result.account_balance : 0;
        this.setaccountBalance(accountBalance);
      })
    }
    return this.accountBalance.asObservable();
  }

  public setaccountBalance(accountBalance: number) {
    this.accountBalance.next(accountBalance);
  }

  public getCustomerCurrentStage(isForceCall = true): any | Observable<any> {
    if (isForceCall) {
      this.getCustomerLatestStage().subscribe((success: any) => {
        let currentStage = success.result && success.result.current_stage ? success.result.current_stage : 0;
        this.setCurrentStage(currentStage);
      })
    }
    return this.currentStage.asObservable();
  }

  public setCurrentStage(currentStage: number) {
    this.saveCurrentStage(currentStage);
    this.currentStage.next(currentStage);
  }


  getCurrentStage(): number {
    const tmp = window.localStorage['current_stage'];
    return tmp ? tmp : 0;
  }

  saveCurrentStage(current_stage: number): void {
    window.localStorage['current_stage'] = current_stage;
  }

  destroyCurrentStage(): void {
    window.localStorage.removeItem('current_stage');
  }



}
