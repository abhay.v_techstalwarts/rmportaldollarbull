import { createAction, props } from '@ngrx/store';

export enum StocksActionTypes {
    GetTopStocks = '[Stocks] Top Stocks',
    GetTopStocksSuccess = '[Stocks] Top Stocks Success',

    GetTopEtfs = '[Stocks] Top ETFs',
    GetTopEtfsSuccess = '[Stocks] Top ETFs Success',

    GetTopIndices = '[Stocks] Top Indices',
    GetTopIndicesSuccess = '[Stocks] Top Indices Success',

    GetAllStocks = '[Stocks] All Stocks',
    GetAllStocksSuccess = '[Stocks] All Stocks Success',

    // GetStocksById = '[Stocks] All Stocks',
    // GetStocksByIdSuccess = '[Stocks] All Stocks Success'
}

//TOP StockS ACTIONS
export const getTopStocks = createAction(
    StocksActionTypes.GetTopStocks
);

export const getTopStocksSuccess = createAction(
    StocksActionTypes.GetTopStocksSuccess,
    props
);

//Top etf actions
export const getTopEtfs = createAction(
    StocksActionTypes.GetTopEtfs
);

export const getTopEtfsSuccess = createAction(
    StocksActionTypes.GetTopEtfsSuccess,
    props
);

//Top Indices Actions
export const getTopIndices = createAction(
    StocksActionTypes.GetTopIndices
);

export const getTopIndicesSuccess = createAction(
    StocksActionTypes.GetTopIndicesSuccess,
    props
);

//All Stocks Actions
export const getAllStocks = createAction(
    StocksActionTypes.GetAllStocks,
    props<any>()
);

export const getAllStocksSuccess = createAction(
    StocksActionTypes.GetAllStocksSuccess,
    props
);

// //Stocks By id
// export const getStocksById = createAction(
//     StocksActionTypes.GetStocksById,
//     props<Number>()
// );

// export const getStocksByIdSuccess = createAction(
//     StocksActionTypes.GetStocksByIdSuccess,
//     props
// );