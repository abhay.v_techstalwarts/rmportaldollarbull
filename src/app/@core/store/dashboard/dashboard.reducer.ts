import { createReducer, on } from '@ngrx/store';

interface dashboardState {
    invested: Number;
    current: Number;
    p_l : Number;
}

export const initialState: dashboardState = {
    invested: 0,
    current: 0,
    p_l: 0
};


export const dashboardReducer = createReducer(
    initialState,
    
);
