import { createReducer, on } from '@ngrx/store'
import { getMasterFiltersSuccess,updateFilters } from './filter.action'
export const initialState: any = []

export const filterReducer = createReducer(
    initialState,
    on(getMasterFiltersSuccess, (state, props: any) => (props.payload)),
    on(updateFilters, (state, props:any) => (props.payload))
)