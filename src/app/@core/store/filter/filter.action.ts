import { createAction, props } from '@ngrx/store';

export enum FilterActionTypes {
    //Update filter
    UpdateFilters = '[Filter] Update Filter',

    //Master filter
    GetMasterFilters = '[Filter] Get Master Filter',
    GetMasterFiltersSuccess = '[Filter] Get Master Filter Success',
    
}

//update filter action
export const updateFilters = createAction(
    FilterActionTypes.UpdateFilters,
    props<any>()
);

// master filter action
export const getMasterFilters = createAction(
    FilterActionTypes.GetMasterFilters
);

// master filter action success
export const getMasterFiltersSuccess = createAction(
    FilterActionTypes.GetMasterFiltersSuccess,
    props
);

