import { createSelector } from "@ngrx/store";

export const filter = createSelector(
    (state: any) => state.filter,
    (filter: any) => filter
);
