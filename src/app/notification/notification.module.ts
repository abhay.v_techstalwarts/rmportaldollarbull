import { SharedModule } from 'src/app/@shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationRoutingModule } from './notification-routing.module';
import { NotificationHomeComponent } from './notification-home/notification-home.component';


@NgModule({
  declarations: [NotificationHomeComponent],
  imports: [
    CommonModule,
    NotificationRoutingModule,
    SharedModule
  ]
})
export class NotificationModule { }
