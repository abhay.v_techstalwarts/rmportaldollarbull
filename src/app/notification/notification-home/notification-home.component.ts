import { Component, OnInit } from '@angular/core';
import { NotificationService } from "src/app/@core/service/notification/notification.service";
import { Router } from '@angular/router';
@Component({
  selector: 'app-notification-home',
  templateUrl: './notification-home.component.html',
})
export class NotificationHomeComponent implements OnInit {
  notifications=[]
  perPage= 5;
  page= 1;
  loading=false;
  totalCount=0;
  constructor(
    private router: Router,
    private notificationService: NotificationService,
  ) { }

  ngOnInit(): void {
    this.notificationService.setNewNotification(false);
    this.getNotification();

  }

  getNotification(page = this.page,perPage = this.perPage){
  this.loading=true;
  this.notificationService.getNotifications({page,perPage}).subscribe(
      (success:any) =>{
        if (page > 1) {
          this.notifications =[...this.notifications, ...success.records];
        }else{
          this.notifications = success.records;
        }
        this.totalCount = success.totalCount;
        console.log(this.notifications);
        this.loading=false;
        
      },
      fail=>{
        console.log(fail);
        this.loading=false;
      }
    )
  }

  loadMore(){
    this.page = this.page +1;
    this.getNotification();
  }

  handleReadNotification(selectNotification){
    let type = selectNotification.type;
    this.notificationService.readNotification(selectNotification.notification_id).subscribe(
      (success:any) =>{
        this.notifications = this.notifications.map( notification =>{
          if (notification.notification_id == selectNotification.notification_id) {
            notification.status = 1;
          }
          return notification;
        })
        
      },
      fail=>{
        console.log(fail);
        
      }
    )

    switch (type) {
      case 'pending':
          this.router.navigate(['/account/order/pending'])
          break;
      case 'executed':
          this.router.navigate(['/account/order/executed'])
          break;
      default:
          break;
}
  }

}
