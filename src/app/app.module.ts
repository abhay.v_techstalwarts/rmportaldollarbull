import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './@shared/shared.module';
import { CoreModule } from './@core/core.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ChartsModule } from 'ng2-charts';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { combineReducers, combineEffects } from './@core/store/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { SearchMobileComponent } from './search-mobile/search-mobile.component';
import { FundingModule } from './funding/funding.module';
import { HttpClientModule } from '@angular/common/http';
import { NotificationModule } from './notification/notification.module';
import { CountdownModule } from 'ngx-countdown';
import { FaqSupportModule } from './faq-support/faq-support.module';
import { PortfolioRmModule } from './portfolio-rm/portfolio-rm.module';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';

@NgModule({
  declarations: [AppComponent, SearchMobileComponent, ResetpasswordComponent, ChangepasswordComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    CoreModule,
    ChartsModule,
    NgbModule,
    FormsModule,
    NgApexchartsModule,
    ReactiveFormsModule,
    StoreModule.forRoot(combineReducers()),
    StoreDevtoolsModule.instrument({
      maxAge: 20,
      logOnly: environment.production,
    }),
    EffectsModule.forRoot(combineEffects()),
    // SocialLoginModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      closeButton:true,
      progressBar:false,
      disableTimeOut:true,
    }),
    FundingModule,
    NotificationModule, // ToastrModule added
    CountdownModule, FaqSupportModule, PortfolioRmModule,
  ],
  providers: [],
  bootstrap: [AppComponent,],
  exports: [SearchMobileComponent, ResetpasswordComponent, ChangepasswordComponent],
})
export class AppModule {}
