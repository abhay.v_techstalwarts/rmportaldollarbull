import { CommonService } from './../../@core/service/common/common.service';
import { Component, EventEmitter, Input, OnInit, Output,ViewChild } from '@angular/core';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { SignUpService } from '../../@core/service/signup/signup.service';
import { JwtService } from '../../@core/service/jwt.service';
import { Router } from '@angular/router';
import { PlatformService } from 'src/app/@core/service/platform.service';
import { HttpClient } from '@angular/common/http';
import { CustomerService } from 'src/app/@core/service/customer/customer.service';
import { CountdownComponent } from 'ngx-countdown';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';




@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  providers: [NgbDropdownConfig]

})
export class SignupComponent implements OnInit {

  @ViewChild('cd', { static: false }) private countdown: CountdownComponent;
  fbLogin: any;
  user = null;
  token = null;
  partitioned: number;
  phoneNumberSection: boolean = false;
  enterEmailOTPSection: boolean = false;
  mobileOTPSection: boolean = false;
  personalDetailSection: boolean = true;
  enterRmCode: boolean = false;
  phoneNumberForm: any;
  emailOTPForm: any;
  mobileOTPForm: any;
  personalDetailForm: any;
  rmCodeForm: any;
  sign_up_msg: string = "";
  email_otp_msg: string = "";
  mobile_otp_msg: string = "";
  rmcode_msg: string = "";
  enteredMobileNo: string = "";
  platform = "";
  settings = {
    length: 6,
    numbersOnly: true,
    // timer: 60
  }

  requestData:any={}

  partners= [];
  selectPartner:any = "Select Partner";
  selectPartnerId:any =0;


  constructor(private fb: FormBuilder,
    dropdownConfig:NgbDropdownConfig,
    private signUpService: SignUpService,
    private router: Router,
    private customerService:CustomerService,
    private CommonService:CommonService
  ) {
    // this.countdown.begin();
    dropdownConfig.placement = 'bottom-right';
    dropdownConfig.autoClose = false;
  }

  handleEvent(event:any, btn){
    console.log(['Countdown handleEvent', event]);
    if (event.action == 'done') {
      btn.disabled = null;
      btn.innerText="Resend";
    }
  }

  ngOnInit(): void {
    this.personalDetailForm = this.fb.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required, Validators.maxLength(15), Validators.pattern('[1-9]{1}[0-9]{9}')]],
    })

    // this.showScreen("phoneNumberSection");
    this.getPartnerList();

  }
  getPartnerList(){
      this.CommonService.getPartnersList().subscribe((res)=>{
        let data : any = res;
        this.partners = data.result.partners;
      });
  }

  partnerval(val){
   this.selectPartner = val.partner_name;
   this.selectPartnerId = val.partner_id;
  }


  signUp() {
    console.log(this.personalDetailForm);
    if (this.personalDetailForm.valid) {
      this.requestData = {
        rm_email_id: this.personalDetailForm.get('email').value,
        rm_mobile_no: this.personalDetailForm.get('phoneNumber').value,
        rm_name: this.personalDetailForm.get('fullName').value,
        partner_id: this.selectPartnerId,
      }
      this.CommonService.signupAPI(this.requestData).subscribe((res) => {
        console.log(["res", res]);
        this.sign_up_msg = "";
      },
        (err) => {
          console.log(err)
          this.sign_up_msg = err.error.message;
        }
      );
    }
  }

  getCustomerProfile(){
    this.customerService.getProfile().subscribe((res)=>{
      let data : any = res;
      this.customerService.saveCustomerProfile(data.result);
    });
  }

  async getCurrentToken() {    
    const result = await this.fbLogin.getCurrentAccessToken();
 
    if (result.accessToken) {
      this.token = result.accessToken;
    } else {
      // Not logged in.
    }
  }

  signOut(): void {
    // this.socailAuthService.signOut();
  }


  // open_terms_condition() {
  //   this.webviewService.open_terms_n_condition();
  // }

  // open_privacy_policy() {
  //   this.webviewService.open_privacy_policy();
  // }



}
