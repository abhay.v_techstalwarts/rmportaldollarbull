import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { AngularOtpLibModule } from 'angular-otp-box';
import { CountdownModule } from 'ngx-countdown';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [SignupComponent],
  imports: [
    CommonModule,
    SignupRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularOtpLibModule,
    CountdownModule,
    NgbModule
  ],
  exports: [SignupComponent]
})
export class SignupModule { }
